
Sigschege, the SIGnal SCHEdule GEnerator
========================================

A software for creating timing diagrams for digital electrical circuits  

* Copyright

Sigschege is copyright 2004-2022 by Ingo Hinrichs and Ulf Klaperski.

Sigschege is distributed under the terms of the GNU General Public
License, version 3. See the file LICENSE and COPYING for further information.

* Requirements

C++20 compliant compiler

cmake >= 3.16
make
Qt >= 5.14.2
Doxygen

* Building from source (distribution tarball)

This project uses the standard cmake build flow. See the file
INSTALL for generic install instructions.

* Building from source (git snapshot)

mkdir build
cd build
cmake ../
make

* Debugging

Disable optimization:

mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE='Debug' ..
make

