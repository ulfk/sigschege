

# 2021-05-04 Ulf Klaperski
Restarted Sigschege project to celebrate this year's Star Wars day.
Migrated Sigschege to Qt 5.14.2.
Changed License to GNU GPL v3.
