// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLADDEVENT_H_
#define TIMTOOLADDEVENT_H_

#include "TimTool.h"
class TimEvent;

class TimToolAddEvent: public TimTool
{
public:
  TimToolAddEvent();
  virtual ~TimToolAddEvent();
  virtual TimToolAction* createAction(QObject *parent);
  virtual void exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event);
};

#endif /* TIMTOOLADDEVENT_H_ */
