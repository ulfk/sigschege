// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolAddEvent.h"

#include <QObject>
#include <QIcon>

TimToolAddEvent::TimToolAddEvent() {}

TimToolAddEvent::~TimToolAddEvent() {}

TimToolAction *TimToolAddEvent::createAction(QObject *parent) {
  return new TimToolAction(this, QIcon(":/images/SigLH.png"), QObject::tr("Add event to signal"), QObject::tr("Add event to signal"), parent, false);
}

void TimToolAddEvent::exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event) {
  caller->actionAddEventTool(event);
}
