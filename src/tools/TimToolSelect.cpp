// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolSelect.h"
#include <QObject>
#include <QIcon>

TimToolSelect::TimToolSelect() {}

TimToolSelect::~TimToolSelect() {}

TimToolAction* TimToolSelect::createAction(QObject *parent) {
  return new TimToolAction(this, QIcon(":/images/arrow.png"), QObject::tr("Select mode"), QObject::tr("Select mode"), parent, true);
}

void TimToolSelect::exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event) {
  caller->actionSelectTool(event);
}
