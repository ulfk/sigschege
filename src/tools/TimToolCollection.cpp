// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolCollection.h"

TimToolCollection::TimToolCollection(QObject *parent)
  : QObject(parent), m_current(0) {}

TimToolCollection::~TimToolCollection() {}

void TimToolCollection::registerTool(TimTool *tool) {
  // select the first tool as current tool
  if (m_tools.empty())
    m_current = tool;
  m_tools.push_back(QSharedPointer<TimTool>(tool));
}

TimToolActions_t TimToolCollection::createActions(QObject *parent) {
  TimToolActions_t actions;
  for (TimTools_t::iterator it = m_tools.begin(); it != m_tools.end(); ++it) {
    TimToolAction *action = (*it)->createAction(parent);
    connect(action, SIGNAL(selected(TimTool*)), this, SLOT(selectionChanged(TimTool*)));
    actions.push_back(action);
  }
  return actions;
}

void TimToolCollection::selectionChanged(TimTool *tool) {
  m_current = tool;
}

TimTool* TimToolCollection::getCurrent() {
  return m_current;
}
