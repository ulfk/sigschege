// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolRmEvent.h"

TimToolRmEvent::TimToolRmEvent() {}

TimToolRmEvent::~TimToolRmEvent() {}

TimToolAction* TimToolRmEvent::createAction(QObject *parent) {
  return new TimToolAction(this, QIcon(":/images/rmEvent.png"), QObject::tr("Remove event from signal"), QObject::tr("Remove event from signal"), parent, false);
}

void TimToolRmEvent::exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event) {
  caller->actionRmEventTool(event);
}
