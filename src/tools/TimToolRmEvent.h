// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLRMEVENT_H
#define TIMTOOLRMEVENT_H

#include "TimTool.h"
class TimEvent;

class TimToolRmEvent : public TimTool
{
public:
  TimToolRmEvent();
  virtual ~TimToolRmEvent();
  virtual TimToolAction * createAction(QObject * parent);
  virtual void exec(TimToolInterface * caller, QGraphicsSceneMouseEvent * event);
};

#endif // TIMTOOLRMEVENT_H
