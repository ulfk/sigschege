// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLCOLLECTION_H_
#define TIMTOOLCOLLECTION_H_

#include "TimTool.h"
#include "TimToolAction.h"
#include <QObject>

class TimToolCollection : public QObject
{
  Q_OBJECT

public:
  TimToolCollection(QObject * parent);
  virtual ~TimToolCollection();
  void registerTool(TimTool * tool);
  TimToolActions_t createActions(QObject * parent);
  TimTool* getCurrent();

public slots:
  void selectionChanged(TimTool * tool);

private:
  TimTool    *m_current;
  TimTools_t  m_tools;
};

#endif /* TIMTOOLCOLLECTION_H_ */
