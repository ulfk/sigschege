// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLSELECT_H_
#define TIMTOOLSELECT_H_

#include "TimTool.h"
class TimToolAction;

class TimToolSelect  : public TimTool
{
public:
  TimToolSelect();
  virtual ~TimToolSelect();
  virtual TimToolAction * createAction(QObject * parent);
  virtual void exec(TimToolInterface * caller, QGraphicsSceneMouseEvent * event);
};

#endif /* TIMTOOLSELECT_H_ */
