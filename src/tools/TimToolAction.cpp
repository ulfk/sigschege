// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolAction.h"

TimToolAction::TimToolAction(TimTool *tool, const QIcon &icon, const QString &text, const QString &status_tip, QObject *parent, bool checked)
  : QAction(icon, text, parent), m_tool(tool)
{
  setCheckable(true);
  setChecked(checked);
  setStatusTip(status_tip);
  connect(this, SIGNAL(triggered(bool)), this, SLOT(select(bool)));
}

TimToolAction::~TimToolAction() {}

void TimToolAction::select(bool checked) {
  if (checked)
    emit selected(m_tool);
}
