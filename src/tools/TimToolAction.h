// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLACTION_H_
#define TIMTOOLACTION_H_

#include <QAction>
#include <QtGui>
class TimTool;
#include <vector>

class TimToolAction: public QAction
{
  Q_OBJECT

public:
  TimToolAction(TimTool * tool, const QIcon & icon, const QString & text, const QString & status_tip, QObject * parent, bool checked = false);
  virtual ~TimToolAction();

private slots:
  void select(bool checked);

signals:
  void selected(TimTool * tool);

private:
  TimTool * m_tool;
};

typedef std::vector<TimToolAction* > TimToolActions_t;

#endif /* TIMTOOLACTION_H_ */
