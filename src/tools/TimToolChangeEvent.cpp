// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimToolChangeEvent.h"

TimToolChangeEvent::TimToolChangeEvent() {}

TimToolChangeEvent::~TimToolChangeEvent() {}

TimToolAction* TimToolChangeEvent::createAction(QObject *parent) {
  return new TimToolAction(this, QIcon(":/images/changeEvent.png"), QObject::tr("Change the resulting state of an event"),
                           QObject::tr("Change the resulting state of an event"), parent, false);
}

void TimToolChangeEvent::exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event) {
  caller->actionChangeEventTool(event);
}
