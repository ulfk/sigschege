// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMEVENTTOOL_H_
#define TIMEVENTTOOL_H_

#include "TimToolAction.h"
#include "TimToolInterface.h"
#include <QObject>
#include <vector>
#include <QSharedPointer>
class QGraphicsSceneMouseEvent;

class TimTool {
public:
  virtual TimToolAction* createAction(QObject *parent) = 0;
  virtual void exec(TimToolInterface *caller, QGraphicsSceneMouseEvent *event) = 0;
};

typedef std::vector<QSharedPointer<TimTool> > TimTools_t;

#endif /* TIMEVENTTOOL_H_ */
