// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMTOOLINTERFACE_H_
#define TIMTOOLINTERFACE_H_

class QGraphicsSceneMouseEvent;

/** TimTool Interface definition
 * This class defines the interface and default implementation for a TimMember object.
 */
class TimToolInterface {

public:
  virtual ~TimToolInterface() {}

  virtual void actionSelectTool(QGraphicsSceneMouseEvent * event) {};
  virtual void actionAddEventTool(QGraphicsSceneMouseEvent * event) {};
  virtual void actionRmEventTool(QGraphicsSceneMouseEvent * event) {};
  virtual void actionChangeEventTool(QGraphicsSceneMouseEvent * event) {};
};

#endif /* TIMTOOLINTERFACE_H_ */
