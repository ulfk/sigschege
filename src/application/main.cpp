// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include <QtGui>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[]) {
  QFileInfo ssgFile;

  Q_INIT_RESOURCE(sigschege);

  QCoreApplication::setOrganizationName("Sigschege Development");
  QCoreApplication::setApplicationName("Sigschege");

  QApplication app(argc, argv);

  if (argc>1) {
    ssgFile = QFileInfo(QString(argv[1]));
  }
  MainWindow mainWin(ssgFile.absoluteFilePath());
  mainWin.show();

  return app.exec();
}
