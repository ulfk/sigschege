// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMUTIL_H_
#define TIMUTIL_H_

#include <ostream>

template <class Type> class Range {
public:
  Range() {}
  Range(Type start, Type end): m_start(start), m_end(end) {}

  void setStart(Type start) { m_start = start; }
  void setEnd(Type end) { m_end = end; }

  Type getStart(void) const { return m_start; }
  Type getEnd(void) const { return m_end; }

  Type distance(void) const { return m_end-m_start; }
  bool contains(Type val) const { return (val>=m_start && val<=m_end); }

  bool operator<(const Range<Type> &rhs) { return m_start<rhs.getStart(); }
  bool operator>(const Range<Type> &rhs) { return m_start>rhs.getStart(); }

  bool operator==(const Range<Type> &rhs) const;

private:
  Type m_start;
  Type m_end;
};

template<class Type> bool Range<Type>::operator==(const Range<Type> &rhs) const {
  return m_start == rhs.m_start && m_end == m_end;
}

template<class Type> std::ostream &operator<<(std::ostream &os, const Range<Type> &range) {
    os << "<" << range.getStart() << ":" << range.getEnd() << ">";
    return os;
}

#endif // TIMUTIL_H
