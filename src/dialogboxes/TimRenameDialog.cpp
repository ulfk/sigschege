// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimRenameDialog.h"
#include "ui_TimRenameDialog.h"

TimRenameDialog::TimRenameDialog(QWidget *parent)
    : QDialog(parent)
{
  ui = new Ui::TimRenameDialogClass;
  ui->setupUi(this);
}

TimRenameDialog::~TimRenameDialog() {}

void TimRenameDialog::setName(const QString &name) {
  ui->lineEdit->setText(name);
}

QString TimRenameDialog::getName() {
  return ui->lineEdit->text();
}
