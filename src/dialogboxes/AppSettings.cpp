// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "AppSettings.h"
#include <QLabel>
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QGridLayout>

AppSettings::AppSettings(unsigned int &maxRecentFiles)
  : m_maxRecentFiles(maxRecentFiles) {
  setWindowTitle(tr("Sigschege Settings"));

  m_maxRecentFilesLabel= new QLabel(tr("Maximum Number of Recent File"));
  m_maxRecentFilesEdit= new QSpinBox(this);

  m_maxRecentFilesEdit->setMinimum(0);
  m_maxRecentFilesEdit->setMaximum(999);
  m_maxRecentFilesEdit->setValue(maxRecentFiles);

  QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                   | QDialogButtonBox::Cancel);
  connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(m_maxRecentFilesLabel, 0, 1);
  mainLayout->addWidget(m_maxRecentFilesEdit, 0, 2);
  mainLayout->addWidget(buttonBox,1, 1, 1, 2);

  setLayout(mainLayout);
}

void AppSettings::accept() {
  m_maxRecentFiles = m_maxRecentFilesEdit->value();
  QDialog::accept();
}
