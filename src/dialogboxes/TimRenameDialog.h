// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMRENAMEDIALOG_H
#define TIMRENAMEDIALOG_H

#include <QDialog>

namespace Ui {
  class TimRenameDialogClass;
}

class TimRenameDialog : public QDialog {
    Q_OBJECT

public:
    TimRenameDialog(QWidget *parent = 0);
    ~TimRenameDialog();

    void setName(const QString & name);
    QString getName();

private:
    Ui::TimRenameDialogClass *ui;
};

#endif // TIMRENAMEDIALOG_H
