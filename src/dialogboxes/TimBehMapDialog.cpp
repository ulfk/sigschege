// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include "TimBehMapDialog.h"
#include "TimVisMapDialog.h"
#include "TimRenameDialog.h"
#include "ui_TimBehMapDialog.h"

#include <QString>

TimBehMapDialog::TimBehMapDialog(QWidget *parent, TimScene *scene) :
  QDialog(parent), m_scene(scene)
{
  ui = new Ui::TimBehMapDialogClass;
  ui->setupUi(this);
  ui->slTableWidget->setItemDelegate(&m_spinDelegate);
  ui->behaviorMaps->addItems(m_scene->getBehaviorMapSettingList());
  if (ui->behaviorMaps->count() > 0)
    changedBehMapSelection(0);

  connect(ui->behaviorMaps       , SIGNAL( currentIndexChanged(int)            ), this, SLOT( changedBehMapSelection(int)           ));
  connect(ui->addButton          , SIGNAL( clicked()                           ), this, SLOT( newBehMap()                           ));
  connect(ui->delButton          , SIGNAL( clicked()                           ), this, SLOT( delBehMap()                           ));
  connect(ui->renameButton       , SIGNAL( clicked()                           ), this, SLOT( renameBehMap()                        ));

  connect(ui->slTableWidget      , SIGNAL( itemSelectionChanged()              ), this, SLOT( changedSlSelection()                  ));
  connect(ui->slAddButton        , SIGNAL( clicked()                           ), this, SLOT( newSl()                               ));
  connect(ui->slRmButton         , SIGNAL( clicked()                           ), this, SLOT( delSl()                               ));
  connect(ui->slUpButton         , SIGNAL( clicked()                           ), this, SLOT( upSl()                                ));
  connect(ui->slDownButton       , SIGNAL( clicked()                           ), this, SLOT( downSl()                              ));
  connect(ui->slTableWidget      , SIGNAL( itemChanged(QTableWidgetItem*)      ), this, SLOT( slItemChanged(QTableWidgetItem*)      ));

  connect(ui->visualMapListWidget, SIGNAL( itemSelectionChanged()              ), this, SLOT( changedVmSelection()                  ));
  connect(ui->vmAddButton        , SIGNAL( clicked()                           ), this, SLOT( newVm()                               ));
  connect(ui->vmDelButton        , SIGNAL( clicked()                           ), this, SLOT( delVm()                               ));
  connect(ui->vmUpButton         , SIGNAL( clicked()                           ), this, SLOT( upVm()                                ));
  connect(ui->vmDownButton       , SIGNAL( clicked()                           ), this, SLOT( downVm()                              ));
  connect(ui->visualMapListWidget, SIGNAL( itemDoubleClicked(QListWidgetItem*) ), this, SLOT( itemDoubleClickedVm(QListWidgetItem*) ));
  connect(ui->vmEditButton       , SIGNAL( clicked()                           ), this, SLOT( editVM()                              ));
}

TimBehMapDialog::~TimBehMapDialog() {}

void TimBehMapDialog::changedBehMapSelection(int index) {
  if (index >= 0) {
    m_map = m_scene->getBehaviorMapByName(ui->behaviorMaps->itemText(index));

    // fill signal level table. Block signal so that no change event gets triggered.
    ui->slTableWidget->blockSignals(true);
    ui->slTableWidget->setRowCount(m_map->getSignalLevelCount());
    for (unsigned int cnt = 0; cnt < m_map->getSignalLevelCount(); ++cnt) {
      ui->slTableWidget->setItem(cnt, 0, new QTableWidgetItem(QString::number(m_map->getSignalLevel(cnt))));
    }
    ui->slTableWidget->blockSignals(false);
    ui->visualMapListWidget->blockSignals(true);
    ui->visualMapListWidget->clear();
    ui->visualMapListWidget->addItems(m_map->getVisualMapPatterns());
    ui->visualMapListWidget->blockSignals(false);
    // enable delete button
    ui->delButton->setEnabled(true);
    // enable rename button
    ui->renameButton->setEnabled(true);
  } else {
    m_map.reset();
    ui->slTableWidget->blockSignals(true);
    ui->slTableWidget->clear();
    ui->slTableWidget->blockSignals(false);
    ui->visualMapListWidget->blockSignals(true);
    ui->visualMapListWidget->clear();
    ui->visualMapListWidget->blockSignals(false);
    ui->tabWidget->setEnabled(false);
    // disable delete button
    ui->delButton->setEnabled(false);
    // disable rename button
    ui->renameButton->setEnabled(false);
  }
  changedSlSelection();
  changedVmSelection();
}

void TimBehMapDialog::newBehMap()
{
  // find name for the behavior map
  unsigned int idx = 0;
  for(int cnt = 0; cnt < ui->behaviorMaps->count(); ++cnt) {
    QString str = ui->behaviorMaps->itemText(cnt);
    if(str.startsWith("new_")) {
      bool ok = false;
      unsigned int temp = str.remove("new_").toUInt(&ok);
      if(ok && temp >= idx)
        idx = temp + 1;
    }
  }

  // Build name
  QString name = QString("new_%1").arg(idx);

  // create and add new behavior map
  m_scene->addBehaviorMap(name, TimBehaviorMap_p( new TimBehaviorMap(name)));
  ui->behaviorMaps->addItem(name);
  ui->behaviorMaps->setCurrentIndex(ui->behaviorMaps->count()-1);
}

void TimBehMapDialog::delBehMap()
{
  int idx = ui->behaviorMaps->currentIndex();
  if (idx >= 0) {
    m_scene->delBehaviorMap(ui->behaviorMaps->itemText(idx));
    ui->behaviorMaps->removeItem(idx);
  }
}

void TimBehMapDialog::changedSlSelection()
{
  QList<QTableWidgetItem *> sel = ui->slTableWidget->selectedItems();
  if (!sel.empty()) {
    int row = ui->slTableWidget->row(sel.first());
    int max = ui->slTableWidget->rowCount()-1;
    ui->slRmButton->setEnabled(true);
    ui->slUpButton->setEnabled(row != 0);
    ui->slDownButton->setEnabled(row != max);
  } else {
    ui->slRmButton->setEnabled(false);
    ui->slUpButton->setEnabled(false);
    ui->slDownButton->setEnabled(false);
  }
}

void TimBehMapDialog::newSl() {
  m_map->insertSignalLevel(50);
  ui->slTableWidget->blockSignals(true);
  ui->slTableWidget->setRowCount(m_map->getSignalLevelCount());
  ui->slTableWidget->setItem(m_map->getSignalLevelCount()-1, 0, new QTableWidgetItem(QString::number(m_map->getSignalLevel(m_map->getSignalLevelCount()-1))));
  ui->slTableWidget->blockSignals(false);

}

void TimBehMapDialog::delSl() {
  QList<QTableWidgetItem *> sel = ui->slTableWidget->selectedItems();
  if (!sel.empty()) {
    int row = ui->slTableWidget->row(sel.first());
    m_map->rmSignalLevel(row);
    ui->slTableWidget->removeRow(row);
  }
}

void TimBehMapDialog::upSl()
{
  QList<QTableWidgetItem *> sel = ui->slTableWidget->selectedItems();
  if (sel.empty())
    return;
  int sourceRow = ui->slTableWidget->row(sel.first());
  int destRow = sourceRow - 1;

  QTableWidgetItem* sourceItems = ui->slTableWidget->takeItem(sourceRow,0);
  QTableWidgetItem* destItems   = ui->slTableWidget->takeItem(destRow,0);

  // set back in reverse order
  ui->slTableWidget->setItem(sourceRow, 0, destItems);
  ui->slTableWidget->setItem(destRow, 0, sourceItems);

  // Note : m_map gets updated through item changed signals

  // Update selection.
  ui->slTableWidget->selectRow(destRow);
}

void TimBehMapDialog::downSl() {
  QList<QTableWidgetItem *> sel = ui->slTableWidget->selectedItems();
  if (sel.empty())
    return;

  int sourceRow = ui->slTableWidget->row(sel.first());
  int destRow   = sourceRow + 1;

  QTableWidgetItem* sourceItems = ui->slTableWidget->takeItem(sourceRow,0);
  QTableWidgetItem* destItems   = ui->slTableWidget->takeItem(destRow,0);

  // set back in reverse order
  ui->slTableWidget->setItem(sourceRow, 0, destItems);
  ui->slTableWidget->setItem(destRow, 0, sourceItems);

  // Note : m_map gets updated through item changed signals

  // Update selection.
  ui->slTableWidget->selectRow(destRow);
}

void TimBehMapDialog::slItemChanged(QTableWidgetItem * item) {
  int row = ui->slTableWidget->row(item);
  m_map->updateSignalLevel(item->text().toUInt(), row);
}

void TimBehMapDialog::renameBehMap() {
  int idx = ui->behaviorMaps->currentIndex();
  if (idx >= 0) {
    TimRenameDialog renameDlg;
    renameDlg.setName(ui->behaviorMaps->itemText(idx));
    if (renameDlg.exec() == QDialog::Accepted) {
      m_scene->renameBehaviorMap(ui->behaviorMaps->itemText(idx), renameDlg.getName());
      ui->behaviorMaps->setItemText(idx, renameDlg.getName());
    }
  }
}

void TimBehMapDialog::changedVmSelection() {
  QList<QListWidgetItem *> sel = ui->visualMapListWidget->selectedItems();

  if (!sel.empty()) {
    int row = ui->visualMapListWidget->row(sel.first());
    int max = ui->visualMapListWidget->count() - 1;

    ui->vmDelButton->setEnabled(true);
    ui->vmEditButton->setEnabled(true);
    ui->vmUpButton->setEnabled(row != 0);
    ui->vmDownButton->setEnabled(row != max);
  } else {
    ui->vmDelButton->setEnabled(false);
    ui->vmEditButton->setEnabled(false);
    ui->vmUpButton->setEnabled(false);
    ui->vmDownButton->setEnabled(false);
  }
}

void TimBehMapDialog::newVm()
{
  m_map->insertVisualMap(TimVisualMap_p(new TimVisualMap(".")));
  ui->visualMapListWidget->blockSignals(true);
  ui->visualMapListWidget->addItem(".");
  ui->visualMapListWidget->blockSignals(false);
  editVMinternal(ui->visualMapListWidget->item(ui->visualMapListWidget->count()-1));
}

void TimBehMapDialog::delVm() {
  QList<QListWidgetItem *> sel = ui->visualMapListWidget->selectedItems();

  if (!sel.empty()) {
    int row = ui->visualMapListWidget->row(sel.first());

    m_map->rmVisualMap(row);
    delete sel.first();
  }
}

void TimBehMapDialog::upVm() {
  QList<QListWidgetItem *> sel = ui->visualMapListWidget->selectedItems();
  if (sel.empty())
    return;

  int src = ui->visualMapListWidget->row(sel.first());
  int dst = src - 1;

  QListWidgetItem* srcItem = ui->visualMapListWidget->takeItem(src);
  QListWidgetItem* desItem = ui->visualMapListWidget->takeItem(dst);

  // set back in reverse order
  ui->visualMapListWidget->insertItem(src,desItem);
  ui->visualMapListWidget->insertItem(dst, srcItem);

  // Note : m_map gets updated through item changed signals

  // Update selection.
  ui->visualMapListWidget->setCurrentRow(dst);

  m_map->swapVisualMap(dst,src);
}

void TimBehMapDialog::downVm() {
  QList<QListWidgetItem *> sel = ui->visualMapListWidget->selectedItems();
  if (sel.empty())
    return;

  int src = ui->visualMapListWidget->row(sel.first());
  int dst = src + 1;

  QListWidgetItem* srcItem = ui->visualMapListWidget->takeItem(src);
  QListWidgetItem* desItem = ui->visualMapListWidget->takeItem(dst);

  // set back in reverse order
  ui->visualMapListWidget->insertItem(src,desItem);
  ui->visualMapListWidget->insertItem(dst, srcItem);

  // Note : m_map gets updated through item changed signals

  // Update selection.
  ui->visualMapListWidget->setCurrentRow(dst);

  m_map->swapVisualMap(dst,src);
}

void TimBehMapDialog::editVM() {
  QList<QListWidgetItem*> sel = ui->visualMapListWidget->selectedItems();
  if (!sel.empty()) {
    editVMinternal(sel.first());
  }
}

void TimBehMapDialog::itemDoubleClickedVm(QListWidgetItem *item) {
  editVMinternal(item);
}

void TimBehMapDialog::editVMinternal(QListWidgetItem *item)
{
  int row = ui->visualMapListWidget->row(item);
  TimVisualMap_p vm = m_map->getVisualMap(row);
  TimVisMapDialog vmDialog;
  vmDialog.setVisualMap(*vm);
  int ret = vmDialog.exec();
  if (ret == QDialog::Accepted) {
    *vm = vmDialog.getVisualMap();
    delete item;
    ui->visualMapListWidget->insertItem(row, vm->getPattern());
  }
}
