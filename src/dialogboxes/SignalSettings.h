// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef SIGNALSETTINGS_H_
#define SIGNALSETTINGS_H_

#include <QDialog>
#include <memory>
class TimScene;
class TimManualSignal;
class TimBehaviorMapCollection;

QT_BEGIN_NAMESPACE
class QGroupBox;
class QDialogButtonBox;
class QLabel;
class QLineEdit;
class QDoubleSpinBox;
class QCheckBox;
class QComboBox;

QT_END_NAMESPACE

class SignalSettings : public QDialog {
  Q_OBJECT
    
public:
  SignalSettings(QWidget *parent, TimScene *scene, TimManualSignal *signal);

protected slots:

  virtual void accept();
  virtual void reject();
  void setApply2All(int newState);

private:
  QGroupBox *settingsGroupBox;
  QLabel *defaultSlopeLabel;
  QDoubleSpinBox *defaultSlopeEdit;
  QLabel *defaultUncertaintyLabel;
  QDoubleSpinBox *defaultUncertaintyEdit;
  QDialogButtonBox *buttonBox;
  QLabel *apply2AllLabel;
  QCheckBox *apply2AllCheck;
  QComboBox *behMapSelector;
  TimScene *m_scene;
  TimManualSignal *m_signal;
  std::shared_ptr<TimBehaviorMapCollection> bmc;
  bool m_apply2All;
};

#endif
