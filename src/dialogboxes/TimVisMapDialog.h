// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#ifndef TIMVISMAPDIALOG_H
#define TIMVISMAPDIALOG_H

#include <QDialog>
namespace Ui {
  class TimVisMapDialogClass;
}
#include "TimVisualMap.h"

class TimVisMapDialog : public QDialog{
    Q_OBJECT

public:
    TimVisMapDialog(QWidget *parent = 0);
    ~TimVisMapDialog();

    void setVisualMap(const TimVisualMap & vm);
    TimVisualMap getVisualMap() const;

public slots:
  void accept();

protected slots:
    void selectionChangedEM();
    void newEM();
    void delEM();

private:
    Ui::TimVisMapDialogClass *ui;

    TimVisualMap m_vm;
};

#endif // TIMVISMAPDIALOG_H
