// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef DIASETTINGS_H_
#define DIASETTINGS_H_

#include <QDialog>
#include <QFrame>
#include <QPushButton>
#include <vector>
#include "TimDiagramSettings.h"
class TimScene;
class DiaSettingDialog;

QT_BEGIN_NAMESPACE
class QGroupBox;
class QDialogButtonBox;
class QLabel;
class QLineEdit;
class QDoubleSpinBox;
class QSpinBox;
class QVBoxLayout;
class QCheckBox;
class QComboBox;
QT_END_NAMESPACE

class TimeRangeEdit : public QFrame
{
  Q_OBJECT

public:
  TimeRangeEdit(DiaSettingDialog *parent, TimDiagramSettings *settings, int idx);

private slots:
  void setStart(double value);
  void setEnd(double value);

private:
  QLabel *m_startTimeLabel;
  QDoubleSpinBox *m_startTimeEdit;
  QLabel *m_endTimeLabel;
  QDoubleSpinBox *m_endTimeEdit;
  QPushButton *m_deleteButton;

  DiaSettingDialog *m_dialog;
  TimDiagramSettings *m_settings;
  int m_idx;
};


class DiaSettingDialog : public QDialog {
  Q_OBJECT

public:
  DiaSettingDialog(QWidget *parent, TimScene *scene);
  void setStart(int idx, double value);
  void setEnd(int idx, double value);

protected slots:
  void addRange();
  void chooseMemberBorderColor();
  void chooseAlternatingBackgroundColor();
  virtual void accept();
  virtual void reject();

private:
  QLabel *m_labelWidthLabel;
  QSpinBox *m_labelWidthEdit;
  QLabel *m_diagramWidthLabel;
  QSpinBox *m_diagramWidthEdit;
  QLabel *m_elementHeightLabel;
  QSpinBox *m_elementHeightEdit;
  std::vector<TimeRangeEdit *>m_timeRange;
  QVBoxLayout *m_timeRangeLayout;
  QPushButton *addRangeButton;
  QLabel *snapDeltaTimeLabel;
  QDoubleSpinBox *snapDeltaTimeEdit;
  QDialogButtonBox *buttonBox;

  QCheckBox *drawBorderAroundMembersBox;
  QLabel *memberBorderColor;
  QPushButton *chooseMemberBorderColorButton;
  QCheckBox *drawAlternatingBackgroundsBox;
  QLabel *alternatingBackgroundColor;
  QPushButton *chooseAlternatingBackgroundColorButton;

  TimScene *m_scene;
  TimDiagramSettings newSettings;

};


#endif
