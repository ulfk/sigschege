// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef APPSETTINGS_H
#define APPSETTINGS_H

#include <QDialog>
#include <QSettings>
class QLabel;
class QSpinBox;

class AppSettings : public QDialog {
public:
  AppSettings(unsigned int &maxRecentFiles);

private:
  QLabel *m_maxRecentFilesLabel;
  QSpinBox *m_maxRecentFilesEdit;

  unsigned int &m_maxRecentFiles;

public slots:
  virtual void accept();
};

#endif // APPSETTINGS_H
