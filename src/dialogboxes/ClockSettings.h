// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef CLOCKSETTINGS_H_
#define CLOCKSETTINGS_H_

#include <QDialog>
class TimScene;
class TimClockSignal;
QT_BEGIN_NAMESPACE
class QGroupBox;
class QDialogButtonBox;
class QLabel;
class QLineEdit;
class QDoubleSpinBox;
QT_END_NAMESPACE

class ClockSettings : public QDialog {
  Q_OBJECT
    
public:
  ClockSettings(QWidget *parent, TimScene *scene, TimClockSignal *clock);

protected slots:
  virtual void accept();
  virtual void reject();

private:
  QGroupBox *settingsGroupBox;
  QLabel *offsetLabel;
  QDoubleSpinBox *offsetEdit;
  QLabel *periodLabel;
  QDoubleSpinBox *periodEdit;
  QLabel *activeLabel;
  QDoubleSpinBox *activeEdit;
  QLabel *riseLabel;
  QDoubleSpinBox *riseEdit;
  QLabel *fallLabel;
  QDoubleSpinBox *fallEdit;
  QLabel *riseUncLabel;
  QDoubleSpinBox *riseUncEdit;
  QLabel *fallUncLabel;
  QDoubleSpinBox *fallUncEdit;
  QDialogButtonBox *buttonBox;

  TimScene *m_scene;
  TimClockSignal *m_clock;
};

#endif // CLOCKSETTINGS_H_
