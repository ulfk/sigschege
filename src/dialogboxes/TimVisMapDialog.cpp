// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimVisMapDialog.h"
#include "ui_TimVisMapDialog.h"
#include <QDebug>

TimVisMapDialog::TimVisMapDialog(QWidget *parent)
    : QDialog(parent)
{
  ui = new Ui::TimVisMapDialogClass;
  ui->setupUi(this);

  connect(ui->EMTable, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChangedEM()));
  connect(ui->EMNewButton, SIGNAL(clicked()), this, SLOT(newEM()));
  connect(ui->EMDeleteButton, SIGNAL(clicked()), this, SLOT(delEM()));
}

TimVisMapDialog::~TimVisMapDialog(){}

void TimVisMapDialog::setVisualMap(const TimVisualMap &vm) {
  m_vm = vm;
  ui->patternEdit->setText(m_vm.getPattern());
  unsigned int slcnt = m_vm.getSignalLevelCount();

  // Create Active Signal items
  ui->ActiveSignals->clear();
  for (unsigned int cnt = 0; cnt < slcnt; ++cnt ) {
    QListWidgetItem * tempAS = new QListWidgetItem();
    tempAS->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
    tempAS->setCheckState(Qt::Unchecked);
    tempAS->setText(QString::number(cnt+1));
    ui->ActiveSignals->addItem(tempAS);
  }
  // Check active signal items
  ActiveSignalLevels_t as = vm.getActiveSignalLevels();
  for (ActiveSignalLevels_t::iterator it = as.begin(); it != as.end(); ++it) {
    if (*it < slcnt) {
      QListWidgetItem * i = ui->ActiveSignals->item(*it);
      i->setCheckState(Qt::Checked);
    } else {
      qDebug() << "Warning: Found an Active Signal Level that is larger than the number of Signal Level.";
    }
  }

  // Update Edge Mapping Table
  EdgeMapping_t em = m_vm.getEdgeMapping();
  ui->EMTable->setRowCount(em.size());
  ui->EMTable->setHorizontalHeaderLabels(QStringList() << "From" << "To");

  for (unsigned int cnt = 0; cnt < em.size(); ++cnt) {
    QStringList firstItem;
    ActiveSignalLevels_t & firstAS = em.at(cnt).first;
    for(ActiveSignalLevels_t::iterator asit = firstAS.begin(); asit != firstAS.end(); ++asit) {
      firstItem.append(QString::number(*asit));
    }

    ui->EMTable->setItem(cnt, 0, new QTableWidgetItem(firstItem.join(",")));
    ui->EMTable->setItem(cnt, 1, new QTableWidgetItem(QString::number(em.at(cnt).second)));

  }

  // setValue Properties
  ui->valuePosition->setValue(m_vm.getValuePos());
  ui->valueVisible->setChecked(m_vm.isValueVisible());
}

TimVisualMap TimVisMapDialog::getVisualMap() const {
  return m_vm;
}

void TimVisMapDialog::selectionChangedEM() {
  ui->EMDeleteButton->setEnabled(!ui->EMTable->selectedItems().empty());
}

void TimVisMapDialog::newEM() {
  ui->EMTable->blockSignals(true);
  unsigned int row = ui->EMTable->rowCount();
  ui->EMTable->insertRow(row);
  ui->EMTable->setItem(row, 0, new QTableWidgetItem());
  ui->EMTable->setItem(row, 1, new QTableWidgetItem());
  ui->EMTable->blockSignals(false);
}

void TimVisMapDialog::delEM() {
  QList<QTableWidgetItem *> sel = ui->EMTable->selectedItems();
  if (sel.empty())
    return;

  int row = ui->EMTable->row(sel.first());
  ui->EMTable->removeRow(row);
}

void TimVisMapDialog::accept() {
  // update pattern
  m_vm.setPattern(ui->patternEdit->text());

  // update Active Signals
  ActiveSignalLevels_t as;
  for (unsigned int cnt = 0; cnt < ui->ActiveSignals->count(); ++ cnt) {
    if (ui->ActiveSignals->item(cnt)->checkState() == Qt::Checked) {
      as.push_back(cnt);
    }
  }
  m_vm.setActiveSignalLevels(as);

  EdgeMapping_t em;
  for (unsigned int row = 0; row < ui->EMTable->rowCount(); ++row) {
    ActiveSignalLevels_t astemp;
    QStringList sl = ui->EMTable->item(row, 0)->text().split(",");
    foreach (const QString & t, sl) {
      astemp.push_back(t.toUInt());
    }
    unsigned int itemp = ui->EMTable->item(row, 1)->text().toUInt();
    em.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(astemp,itemp));
  }
  m_vm.setEdgeMapping(em);
  m_vm.setValuePos(ui->valuePosition->value());
  m_vm.setValueVisible(ui->valueVisible->isChecked());

  QDialog::accept();
}
