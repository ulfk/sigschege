// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMBEHMAPDIALOG_H
#define TIMBEHMAPDIALOG_H

#include <QDialog>
#include "TimSpinBoxDelegate.h"
#include "TimScene.h"

namespace Ui {
  class TimBehMapDialogClass;
}
class QTableWidgetItem;
class QListWidgetItem;
class QTableWidgetItem;

class TimScene;

class TimBehMapDialog : public QDialog {
    Q_OBJECT

public:
    TimBehMapDialog(QWidget *parent, TimScene *scene);
    ~TimBehMapDialog();

protected:
  void editVMinternal(QListWidgetItem *item);

protected slots:
  void changedBehMapSelection(int index);
  void newBehMap();
  void delBehMap();
  void renameBehMap();

  void changedSlSelection();
  void newSl();
  void delSl();
  void upSl();
  void downSl();
  void slItemChanged(QTableWidgetItem *item);

  void changedVmSelection();
  void newVm();
  void editVM();
  void delVm();
  void upVm();
  void downVm();
  void itemDoubleClickedVm(QListWidgetItem *item);

private:
    Ui::TimBehMapDialogClass *ui;
    TimScene *m_scene;
    TimSpinBoxDelegate m_spinDelegate;
    TimBehaviorMap_p m_map;
};

#endif // TIMBEHMAPDIALOG_H
