// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include <QtGui>

#include "ClockSettings.h"
#include "TimClockSignal.h"
#include <QVBoxLayout>
#include <QGridLayout>

#include "TimScene.h"
#include <QLabel>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QDialogButtonBox>


//! [0]
ClockSettings::ClockSettings(QWidget *parent, TimScene *scene, TimClockSignal *clock)
  : QDialog(parent) {
  setWindowTitle(tr("Clock Settings"));
  m_scene = scene;
  m_clock = clock;

  settingsGroupBox = new QGroupBox(tr("Clock Settings"));
  settingsGroupBox->setCheckable(false);

  offsetLabel = new QLabel(tr("Offset"));

  offsetEdit = new QDoubleSpinBox(settingsGroupBox);
  offsetEdit->setValue(clock->getOffset());
  offsetEdit->setDecimals(5);
  offsetEdit->setMinimum(-100000.0);
  offsetEdit->setMaximum(+100000.0);

  periodLabel = new QLabel(tr("Period"));
    
  periodEdit = new QDoubleSpinBox(settingsGroupBox);
  periodEdit->setDecimals(5);
  periodEdit->setMinimum(-100000.0);
  periodEdit->setMaximum(+100000.0);
  periodEdit->setValue(clock->getPeriod());

  activeLabel = new QLabel(tr("Active Time"));

  activeEdit = new QDoubleSpinBox(settingsGroupBox);
  activeEdit->setValue(clock->getActive());
  activeEdit->setDecimals(5);
  activeEdit->setMinimum(-100000.0);
  activeEdit->setMaximum(+100000.0);

  riseLabel = new QLabel(tr("Rise Time"));

  riseEdit = new QDoubleSpinBox(settingsGroupBox);
  riseEdit->setValue(clock->getRiseSlope());
  riseEdit->setDecimals(5);
  riseEdit->setMinimum(0.0);
  riseEdit->setMaximum(+100000.0);

  fallLabel = new QLabel(tr("Fall Time"));

  fallEdit = new QDoubleSpinBox(settingsGroupBox);
  fallEdit->setValue(clock->getFallSlope());
  fallEdit->setDecimals(5);
  fallEdit->setMinimum(0.0);
  fallEdit->setMaximum(+100000.0);

  riseUncLabel = new QLabel(tr("Rise Uncertainty"));

  riseUncEdit = new QDoubleSpinBox(settingsGroupBox);
  riseUncEdit->setValue(clock->getRiseUncertainty());
  riseUncEdit->setDecimals(5);
  riseUncEdit->setMinimum(0.0);
  riseUncEdit->setMaximum(+100000.0);

  fallUncLabel = new QLabel(tr("Fall Uncertainty"));

  fallUncEdit = new QDoubleSpinBox(settingsGroupBox);
  fallUncEdit->setValue(clock->getFallUncertainty());
  fallUncEdit->setDecimals(5);
  fallUncEdit->setMinimum(0.0);
  fallUncEdit->setMaximum(+100000.0);

  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
				   | QDialogButtonBox::Cancel);

  connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

  QGridLayout *layout = new QGridLayout;
  layout->addWidget(offsetLabel, 0, 0);
  layout->addWidget(offsetEdit, 0, 1);
  layout->addWidget(periodLabel, 1, 0);
  layout->addWidget(periodEdit, 1, 1);
  layout->addWidget(activeLabel, 2, 0);
  layout->addWidget(activeEdit, 2, 1);
  layout->addWidget(riseLabel, 3, 0);
  layout->addWidget(riseEdit, 3, 1);
  layout->addWidget(fallLabel, 4, 0);
  layout->addWidget(fallEdit, 4, 1);
  layout->addWidget(riseUncLabel, 5, 0);
  layout->addWidget(riseUncEdit, 5, 1);
  layout->addWidget(fallUncLabel, 6, 0);
  layout->addWidget(fallUncEdit, 6, 1);
  layout->addWidget(buttonBox, 7, 0, 1, 2);
  settingsGroupBox->setLayout(layout);

  connect(this, SIGNAL(accepted()), m_scene, SLOT(update()) );

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(settingsGroupBox, 0, 0);
  setLayout(mainLayout);
}

void ClockSettings::accept()  {
  m_clock->setOffset(offsetEdit->value(), false);
  m_clock->setPeriod(periodEdit->value(), false);
  m_clock->setActive(activeEdit->value(), false);
  m_clock->setFallSlope(fallEdit->value(), false);
  m_clock->setRiseSlope(riseEdit->value(), false);
  m_clock->setFallUncertainty(fallUncEdit->value(), false);
  m_clock->setRiseUncertainty(riseUncEdit->value());
  QDialog::accept();
}

void ClockSettings::reject()  {
  QDialog::reject();
}
