// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include <QtGui>
#include "DiaSettings.h"
#include <QVBoxLayout>
#include <QGridLayout>
#include "TimScene.h"
#include <QLabel>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QDialogButtonBox>
#include <QComboBox>
#include <QCheckBox>
#include <QColorDialog>

TimeRangeEdit::TimeRangeEdit(DiaSettingDialog *parent, TimDiagramSettings *settings, int idx) :
  QFrame(parent),
  m_dialog(parent),
  m_settings(settings),
  m_idx(idx)
{
  m_startTimeLabel = new QLabel(tr("Start"));
  m_startTimeEdit = new QDoubleSpinBox(this);

  m_startTimeEdit->setDecimals(5);
  m_startTimeEdit->setMinimum(-100000.0);
  m_startTimeEdit->setMaximum(+100000.0);
  m_startTimeEdit->setValue(m_settings->getStartTime(m_idx));

  m_endTimeLabel = new QLabel(tr("End"));
  m_endTimeEdit = new QDoubleSpinBox(this);

  m_endTimeEdit->setDecimals(5);
  m_endTimeEdit->setMinimum(-100000.0);
  m_endTimeEdit->setMaximum(+100000.0);
  m_endTimeEdit->setValue(m_settings->getEndTime(m_idx));

  QHBoxLayout *layout = new QHBoxLayout;
  layout->addWidget(m_startTimeLabel);
  layout->addWidget(m_startTimeEdit);
  layout->addWidget(m_endTimeLabel);
  layout->addWidget(m_endTimeEdit);

  setLayout(layout);

  connect(m_startTimeEdit, SIGNAL(valueChanged(double)), this, SLOT(setStart(double)));
  connect(m_endTimeEdit, SIGNAL(valueChanged(double)), this, SLOT(setEnd(double)));
}

void TimeRangeEdit::setStart(double value) {
    m_dialog->setStart(m_idx, value);
}

void TimeRangeEdit::setEnd(double value) {
    m_dialog->setEnd(m_idx, value);
}

void DiaSettingDialog::addRange() {
  newSettings.addTimeRange();
  TimeRangeEdit *range = new TimeRangeEdit(this, &newSettings, newSettings.numRanges()-1);
  m_timeRangeLayout->addWidget(range);
}

void DiaSettingDialog::chooseMemberBorderColor()
{
  chooseMemberBorderColorButton->setText(QColorDialog::getColor(m_scene->getDiagramSettings()->getMemberBorderColor(), this).name());
}

void DiaSettingDialog::chooseAlternatingBackgroundColor()
{
  chooseAlternatingBackgroundColorButton->setText(QColorDialog::getColor(m_scene->getDiagramSettings()->getAlternatingBackgroundColor(), this).name());
}

//! [0]
DiaSettingDialog::DiaSettingDialog(QWidget *parent, TimScene *scene)
  : QDialog(parent), m_scene(scene), newSettings(*(scene->getDiagramSettings()))
{
  setWindowTitle(tr("Document Properties"));

  QGroupBox *groupBox = new QGroupBox(tr("Time Ranges"));
  m_timeRangeLayout = new QVBoxLayout;
  for (int rIdx = 0; rIdx<m_scene->getDiagramSettings()->numRanges(); ++rIdx) {
      TimeRangeEdit *range = new TimeRangeEdit(this, &newSettings, rIdx);
      m_timeRangeLayout->addWidget(range);
    }
  groupBox->setLayout(m_timeRangeLayout);

  snapDeltaTimeLabel = new QLabel(tr("Snap Distance"));
  snapDeltaTimeEdit = new QDoubleSpinBox(this);
  snapDeltaTimeEdit->setDecimals(5);
  snapDeltaTimeEdit->setMinimum(-100000.0);
  snapDeltaTimeEdit->setMaximum(+100000.0);
  snapDeltaTimeEdit->setValue(m_scene->getDiagramSettings()->getSnapDeltaTime());

  m_labelWidthLabel = new QLabel(tr("Label Width"));
  m_labelWidthEdit = new QSpinBox(this);
  m_labelWidthEdit->setMinimum(10);
  m_labelWidthEdit->setMaximum(100000);
  m_labelWidthEdit->setValue(m_scene->getDiagramSettings()->getCol0Width());

  m_diagramWidthLabel = new QLabel(tr("Diagram Width"));
  m_diagramWidthEdit = new QSpinBox(this);
  m_diagramWidthEdit->setMinimum(10);
  m_diagramWidthEdit->setMaximum(100000);
  m_diagramWidthEdit->setValue(m_scene->getDiagramSettings()->getCol1Width());

  m_elementHeightLabel = new QLabel(tr("Signal Height"));
  m_elementHeightEdit = new QSpinBox(this);
  m_elementHeightEdit->setMinimum(10);
  m_elementHeightEdit->setMaximum(100000);
  m_elementHeightEdit->setValue(m_scene->getDiagramSettings()->getItemHeight());


  drawBorderAroundMembersBox = new QCheckBox(tr("Draw border around members"), this);
  drawBorderAroundMembersBox->setChecked(m_scene->getDiagramSettings()->getDrawBorderAroundMembers());

  drawAlternatingBackgroundsBox = new QCheckBox(tr("Draw alternating backgrounds"), this);
  drawAlternatingBackgroundsBox->setChecked(m_scene->getDiagramSettings()->getDrawAlternatingBackgrounds());

  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                   | QDialogButtonBox::Cancel);

  addRangeButton = new QPushButton(tr("Add Time Range"), this);

  memberBorderColor  = new QLabel(tr("Member border color"), this);

  chooseMemberBorderColorButton = new QPushButton(m_scene->getDiagramSettings()->getMemberBorderColor().name(), this);

  alternatingBackgroundColor = new QLabel(tr("Alternating Background color"));

  chooseAlternatingBackgroundColorButton = new QPushButton(m_scene->getDiagramSettings()->getAlternatingBackgroundColor().name(), this);

  connect(addRangeButton, SIGNAL(clicked()), this, SLOT(addRange()));

  connect(chooseMemberBorderColorButton, SIGNAL(clicked()), this, SLOT(chooseMemberBorderColor()));
  connect(chooseAlternatingBackgroundColorButton, SIGNAL(clicked()), this, SLOT(chooseAlternatingBackgroundColor()));

  connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

  connect(this, SIGNAL(accepted()), m_scene, SLOT(update()) );

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(groupBox, 0, 0, 6, 1);
  mainLayout->addWidget(snapDeltaTimeLabel, 0, 1);
  mainLayout->addWidget(snapDeltaTimeEdit, 0, 2);
  mainLayout->addWidget(addRangeButton, 1, 1);
  mainLayout->addWidget(m_labelWidthLabel, 2, 1);
  mainLayout->addWidget(m_labelWidthEdit, 2, 2);
  mainLayout->addWidget(m_diagramWidthLabel, 3, 1);
  mainLayout->addWidget(m_diagramWidthEdit, 3, 2);
  mainLayout->addWidget(m_elementHeightLabel, 4, 1);
  mainLayout->addWidget(m_elementHeightEdit, 4, 2);
  mainLayout->addWidget(drawBorderAroundMembersBox, 5, 1, 1, 2);
  mainLayout->addWidget(memberBorderColor, 6, 1, 1, 1);
  mainLayout->addWidget(chooseMemberBorderColorButton, 6, 2, 1, 1);
  mainLayout->addWidget(drawAlternatingBackgroundsBox, 7, 1, 1, 1);
  mainLayout->addWidget(alternatingBackgroundColor, 8, 1, 1, 1);
  mainLayout->addWidget(chooseAlternatingBackgroundColorButton, 8, 2, 1, 1);
  mainLayout->addWidget(buttonBox, 9, 1, 1, 2);

  setLayout(mainLayout);

}

void DiaSettingDialog::setStart(int idx, double value) {
    newSettings.setStartTime(idx, value);
}

void DiaSettingDialog::setEnd(int idx, double value) {
    newSettings.setEndTime(idx, value);
}


void DiaSettingDialog::accept()  {
  newSettings.setSnapDeltaTime(snapDeltaTimeEdit->value());
  newSettings.setCol0Width(m_labelWidthEdit->value());
  newSettings.setCol1Width(m_diagramWidthEdit->value());
  newSettings.setItemHeight(m_elementHeightEdit->value());
  newSettings.setDrawBorderAroundMembers(drawBorderAroundMembersBox->isChecked());
  newSettings.setMemberBorderColor(QColor(chooseMemberBorderColorButton->text()));
  newSettings.setDrawAlternatingBackgrounds(drawAlternatingBackgroundsBox->isChecked());
  newSettings.setAlternatingBackgroundColor(QColor(chooseAlternatingBackgroundColorButton->text()));
  if (newSettings != *(m_scene->getDiagramSettings()))
    m_scene->setDiagramSettingsByCmd(newSettings);
  QDialog::accept();
}

void DiaSettingDialog::reject()  {
  QDialog::reject();
}
