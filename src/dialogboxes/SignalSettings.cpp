// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include <QtGui>
#include "SignalSettings.h"
#include "TimManualSignal.h"
#include <QVBoxLayout>
#include <QGridLayout>
#include "TimScene.h"
#include <QLabel>
#include <QDoubleSpinBox>
#include <QGroupBox>
#include <QDialogButtonBox>
#include <QCheckBox>
#include <QComboBox>

//! [0]
SignalSettings::SignalSettings(QWidget *parent, TimScene *scene, TimManualSignal *signal)
  : QDialog(parent), m_apply2All(false)
{
  setWindowTitle(tr("Signal Settings"));
  m_scene = scene;
  m_signal = signal;
  bmc = scene->getBehaviorMapCollection();

  settingsGroupBox = new QGroupBox(tr("Signal Settings"));
  settingsGroupBox->setCheckable(false);


  defaultSlopeLabel = new QLabel(tr("Default Slope Time"));

  defaultSlopeEdit = new QDoubleSpinBox(settingsGroupBox);
  defaultSlopeEdit->setValue(signal->getDefaultSlope());

  defaultSlopeEdit->setDecimals(5);
  defaultSlopeEdit->setMinimum(0.0);
  defaultSlopeEdit->setMaximum(+100000.0);

  defaultUncertaintyLabel = new QLabel(tr("Default Uncertainty Time"));

  defaultUncertaintyEdit = new QDoubleSpinBox(settingsGroupBox);
  defaultUncertaintyEdit->setValue(signal->getDefaultUncertainty());

  defaultUncertaintyEdit->setDecimals(5);
  defaultUncertaintyEdit->setMinimum(0.0);
  defaultUncertaintyEdit->setMaximum(+100000.0);

  apply2AllCheck = new QCheckBox(tr("Apply to existing edges"));

  behMapSelector = new QComboBox(this);
  behMapSelector->addItems(bmc->getSettingList());
  behMapSelector->setCurrentText(m_signal->getWave()->getBehaviorMapId());


  buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
				   | QDialogButtonBox::Cancel);

  connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
  connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

  QGridLayout *layout = new QGridLayout;
  layout->addWidget(defaultSlopeLabel, 0, 0);
  layout->addWidget(defaultSlopeEdit, 0, 1);
  layout->addWidget(defaultUncertaintyLabel, 1, 0);
  layout->addWidget(defaultUncertaintyEdit, 1, 1);
  layout->addWidget(apply2AllCheck, 2, 0, 1, 2);
  layout->addWidget(behMapSelector, 3, 0, 1, 2);
  layout->addWidget(buttonBox, 4, 0, 1, 2);
  settingsGroupBox->setLayout(layout);

  connect(apply2AllCheck, SIGNAL(stateChanged(int)), this, SLOT(setApply2All(int)));
  connect(this, SIGNAL(accepted()), m_scene, SLOT(update()) );

  QGridLayout *mainLayout = new QGridLayout;
  mainLayout->addWidget(settingsGroupBox, 0, 0);

  setLayout(mainLayout);
}

void SignalSettings::setApply2All(int newState) {
  m_apply2All = (newState!=0);
}

void SignalSettings::accept()  {
  m_signal->setDefaultSlope(defaultSlopeEdit->value());
  m_signal->setDefaultUncertainty(defaultUncertaintyEdit->value());
  m_signal->getWave()->setBehaviorMapId(behMapSelector->currentText());
  if (m_apply2All) m_signal->applyDefaults();
  QDialog::accept();
}

void SignalSettings::reject()  {
  QDialog::reject();
}
