// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimSignal.h"
#include "TimScene.h"
#include "TimLabel.h"
#include "SSGWriter.h"
#include "TimCmdRmListItem.h"
#include "TimCmdAddEvent.h"
#include "TimBehaviorMap.h"
#include "TimVisualMap.h"
#include <QGraphicsSceneMouseEvent>
#include <QTransform>

TimSignal::TimSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap) :
  TimMember(parent, scene) {
  m_wave = std::make_shared<TimWave>(this, scene, behaviorMap);
  setFlag(ItemIsSelectable);
}

TimSignal::~TimSignal() {}

void TimSignal::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  TimMember::paint(painter, option, widget);
}

void TimSignal::SSGWrite(SSGWriter *writer) {}

QUndoCommand* TimSignal::createDeleteCmd() {
  return new TimCmdRmListItem(getScene(), this);
}

void TimSignal::timeRangeChange() {
  m_wave->timeRangeChange();
}

void TimSignal::mousePressEvent(QGraphicsSceneMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    // we do our work at mouseReleaseEvent TODO: why?
  } else {
    TimMember::mousePressEvent(event);
  }
}

void TimSignal::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    getScene()->getCurrentTool()->exec(this, event);
  } else {
    TimMember::mouseReleaseEvent(event);
  }
}

void TimSignal::contextMenuEvent ( QGraphicsSceneContextMenuEvent *event ) {
  TimMember::contextMenuEvent(event);
}


void TimSignal::actionSelectTool(QGraphicsSceneMouseEvent *event) {
  QTransform tf;
  getScene()->clearSelection();
  TimMemberBase *item = dynamic_cast<TimMemberBase*>(getScene()->itemAt(event->scenePos(), tf));
  if (item!=nullptr && item!=m_wave.get()) {
    item->setSelected(true);
  } else {
    this->setSelected(true);
  }
}

TimBehaviorMap_p TimSignal::getBehaviorMap() const {
  return getScene()->getBehaviorMapByName(m_wave->getBehaviorMapId());
}

std::shared_ptr<TimWave> TimSignal::getWave() {
  return m_wave;
}
