// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCLOCKSIGNAL_H_
#define TIMCLOCKSIGNAL_H_

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;
#include "TimSignal.h"
#include "TimDiagramSettings.h"
#include "TimEvent.h"
class TimScene;
class TimWave;

/** @brief The @c TimSignal class represents one signal in the Timing Diagram.
 *
 */
class TimClockSignal: public TimSignal {
public:
  /** @brief Creates a new @c TimSignal object.
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   * @param behaviorMap Name of the behaviour map of this signal.
   */
  TimClockSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap = "Binary");

  /** @brief DTor
   * Destroys the @c TimSignal object
   */
  virtual ~TimClockSignal();

  virtual void SSGWrite(SSGWriter *writer);

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd();

  /** @brief Set the offset of the clock, an offset of 0 sets the rising edge at time 0
    *
    * @param time Offset of the rising clock edge from time 0
    * @param update Flag to indicate if this signal must be updated
    */
  void setOffset(double time, bool update = true);

  /** @brief Set the period of the clock
    *
    * @param time new period of the clock
    * @param update Flag to indicate if this signal must be updated
    */
   void setPeriod(double time, bool update = true);

  /** @brief Set the active ('1') time of the clock
    *
    * @param time new period of the clock
    * @param update Flag to indicate if this signal must be updated
    */
   void setActive(double time, bool update = true);

  void setRiseSlope(double time, bool update = true);
  void setFallSlope(double time, bool update = true);
 
  void setRiseUncertainty(double time, bool update = true);
  void setFallUncertainty(double time, bool update = true);
 
  double getOffset(void) { return m_offset; }
  double getPeriod(void) { return m_period; }
  double getActive(void) { return m_active; }

  double getRiseSlope(void) { return m_rise_slope; }
  double getFallSlope(void) { return m_fall_slope; }

  double getRiseUncertainty(void) { return m_rise_uncertainty; }
  double getFallUncertainty(void) { return m_fall_uncertainty; }

  virtual void timeRangeChange();
  void calcEvents(void);
  virtual void setGeometry(const QRectF &rect);

protected:
  virtual QString getType() {return "ClockSignal";}
  virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent * event);

private:
  double m_offset;
  double m_period;
  double m_active;
  double m_rise_slope;
  double m_fall_slope;
  double m_rise_uncertainty;
  double m_fall_uncertainty;
};

#endif /* TIMSIGNAL_H_ */
