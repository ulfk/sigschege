// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMMEMBER_H_
#define TIMMEMBER_H_

#include "TimMemberBase.h"
#include "TimDiagramSettings.h"
class TimScene;
class TimLabel;
class QString;

/** @brief The @c TimMember class displays a time scale in the Timing Diagram.
 *
 */
class TimMember: public TimMemberBase {
public:
  /** @brief Creates a new @c TimMember object.
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   */
  TimMember(TimMemberBase *parent, TimScene *scene);

  /** @brief DTor
   * Destroys the @c TimMember object
   */
  virtual ~TimMember();

  /** @brief Sets the text of the label
   *
   * @param text The new text of the label
   */
  void setText (const QString &text);

  virtual int getHeightFactorInPercent();

  void setHeightFactorInPercent(int newValue);

  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
  /** @brief Pointer to the signal label
   */
  TimLabel *m_label;
  int m_heightFactorInPercent;
};

#endif /* TIMSCALE_H_ */
