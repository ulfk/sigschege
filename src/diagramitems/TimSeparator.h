// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMSEPARATOR_H_
#define TIMSEPARATOR_H_

#include <QtGui>
#include "TimMember.h"

class TimSeparator : public TimMember {
public:
  TimSeparator(TimMemberBase *parent, TimScene *scene);
  virtual ~TimSeparator();
  void SSGWrite(SSGWriter *writer);

  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
  virtual QString getType() {return "Separator";}
};

#endif /* TIMSEPARATOR_H_ */
