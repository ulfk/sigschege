// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimMemberBase.h"

#include "TimScene.h"

#include "TimLabel.h"
#include "TimUtil.h"

#include "TimScale.h"
#include "TimManualSignal.h"
#include "TimClockSignal.h"
#include "TimSeparator.h"

TimMemberBase::TimMemberBase(TimMemberBase *parent, TimScene *scene)
  : QGraphicsItem(parent), QGraphicsLayoutItem(0, false), m_scene(scene)
{
    m_size = QSizeF(scene->getDiagramSettings()->getFullWidth(), scene->getDiagramSettings()->getItemHeight());
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
}

TimMemberBase::~TimMemberBase() {}

TimMemberBase* TimMemberBase::create(const QString &type, TimMemberBase *parent, TimScene *scene)
{
  TimMemberBase *item = NULL;

  if (type == "signal") {
    item = new TimManualSignal(parent, scene, "Logic");
  } else if (type == "bus") {
    item = new TimManualSignal(parent, scene, "Bus");
  } else if (type == "clock") {
    item = new TimClockSignal(parent, scene);
  } else if (type == "scale") {
    item = new TimScale(parent, scene);
  } else if (type == "separator") {
    item = new TimSeparator(parent, scene);
  } else {
    qDebug() << "Error: Can not insert TimMemberBase of unknown type " << type << ".";
    return NULL;
  }
  return item;
}

QSizeF TimMemberBase::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const {
  TimDiagramSettings *settings = getScene()->getDiagramSettings();
  switch (which) {
  case Qt::MinimumSize:
  case Qt::PreferredSize:
    return QSizeF(getDiagramSettings()->getCol0Width() + getDiagramSettings()->getCol1Width(), getDiagramSettings()->getItemHeight());
  case Qt::MaximumSize:
    return QSizeF(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
  default:
    qWarning("r::TimMemberBase::sizeHint(): Don't know how to handle the value of 'which'");
    break;
  }
  return constraint;
}

void TimMemberBase::setGeometry(const QRectF &rect) {
  setPos(rect.topLeft());
  setSize(rect.size());
}

QRectF TimMemberBase::boundingRect() const {
  return QRectF(QPointF(0.0,0.0), size());
}

TimDiagramSettings* TimMemberBase::getDiagramSettings() const {
  return getScene()->getDiagramSettings();
}

TimScene* TimMemberBase::getScene() const {
  return m_scene;
}

QUndoCommand *TimMemberBase::createDeleteCmd() {
  return nullptr;
}

qreal TimMemberBase::calcSnapTime(qreal xpos, bool isWave) {
  TimDiagramSettings *diaSettings = getDiagramSettings();
  if (isWave)
    xpos += diaSettings->getCol0Width();
  int rIdx = diaSettings->getIdx4AbsPos(xpos);
  if (rIdx<0) return 0.0;
  double time = diaSettings->getTime4AbsPos(xpos);
  if (diaSettings->getSnapDeltaTime() <= 0.0)
    return time;
  double lower = diaSettings->getSnapDeltaTime() * floor( time / diaSettings->getSnapDeltaTime() );
  if ( (time-lower) < (diaSettings->getSnapDeltaTime()/2.0) )
    m_snapTime = lower;
  else
    m_snapTime = lower + diaSettings->getSnapDeltaTime();
  m_snapDeviation = (time - m_snapTime) * 2.0 / diaSettings->getSnapDeltaTime();
  return m_snapTime;
}

qreal TimMemberBase::calcSnapTimeT(qreal time) {
  double snap_delta_time = getDiagramSettings()->getSnapDeltaTime();
  if(snap_delta_time <= 0.0)
    return time;
  double lower = snap_delta_time * floor( time / snap_delta_time );
  if( (time - lower) >= (snap_delta_time / 2.0))
    lower += snap_delta_time;
  return lower;
}

int TimMemberBase::count () const {
  return 0;
}

void TimMemberBase::removeAt (int index) {

}

QSizeF TimMemberBase::size() const {
  return m_size;
}

void TimMemberBase::setSize(const QSizeF &size) {
  m_size = size;
}

QString TimMemberBase::getID(void) {
  return getType() + "@" + QString::number(getIdx());
}

int TimMemberBase::getIdx(void) {
  if (m_scene != nullptr) return m_scene->getIndexOf(this);
  else return -2;
}

int TimMemberBase::getHeight()
{
  return m_scene->getDiagramSettings()->getItemHeight() * getHeightFactorInPercent() / 100;
}
