// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMSCALE_H_
#define TIMSCALE_H_

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;
#include "TimMember.h"
#include "TimDiagramSettings.h"
class TimScene;

/** @brief The @c TimScale class displays a time scale in the Timing Diagram.
 *
 */
class TimScale: public TimMember {
public:
  /** @brief Creates a new @c TimScale object.
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   */
  TimScale(TimMemberBase *parent, TimScene *scene);

  /** @brief DTor
   * Destroys the @c TimScale object
   */
  virtual ~TimScale();

  /** @brief Paints the signal.
   *
   * @param painter Pointer to the painter object
   * @param option Pointer to painter options
   * @param widget Pointer to the painting widget
   */
  virtual void
      paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

  virtual void SSGWrite(SSGWriter *writer);

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd();

protected:
  virtual QString getType() {return "Scale";}
};

#endif /* TIMSCALE_H_ */
