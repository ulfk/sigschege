// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimClockSignal.h"
#include "TimScene.h"
#include "TimWave.h"
#include "TimLabel.h"
#include "SSGWriter.h"
#include "TimCmdRmListItem.h"
#include "TimCmdAddEvent.h"
#include "ClockSettings.h"
#include <cmath>
#include <iostream>

using namespace std;

TimClockSignal::TimClockSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap)
  : TimSignal(parent, scene, behaviorMap), m_rise_slope(1.0), m_fall_slope(1.0), m_rise_uncertainty(0.0), m_fall_uncertainty(0.0)
{
  m_label->setText(QObject::tr("Clock"));
  m_offset = 5.0;
  m_period = 15.0;
  m_active = 5.0;
  calcEvents();
}

TimClockSignal::~TimClockSignal() {}

void TimClockSignal::setOffset(double time, bool update) {
  m_offset = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setPeriod(double time, bool update) {
  m_period = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setActive(double time, bool update) {
  m_active = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setRiseSlope(double time, bool update) {
  m_rise_slope = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setFallSlope(double time, bool update) {
  m_fall_slope = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setRiseUncertainty(double time, bool update) {
  m_rise_uncertainty = time;
  if (update)
    calcEvents();
}

void TimClockSignal::setFallUncertainty(double time, bool update) {
  m_fall_uncertainty = time;
  if (update)
    calcEvents();
}

void TimClockSignal::timeRangeChange() {
  calcEvents();
}

void TimClockSignal::calcEvents(void) {
  m_wave->clear();
  TimDiagramSettings *diaSettings = getDiagramSettings();
  for (int rIdx = 0; rIdx<diaSettings->numRanges(); ++rIdx) {
      double start_time = diaSettings->getStartTime(rIdx);
      double end_time = diaSettings->getEndTime(rIdx);
      double n = (start_time - m_offset)/m_period;
      if (n == floor(n))
        n -= 1;
      n = floor(n);

      // find starting level
      bool fall_edge = (m_offset + n * m_period + m_active < start_time);
      double ev_time = m_offset + n * m_period + (fall_edge ? m_active : 0.0);

      while (ev_time < end_time) {
          // TODO: Use behavior map to get next value
          if (fall_edge) {
              m_wave->addTimEvent(false, ev_time, "0", m_fall_slope, m_fall_uncertainty);
            } else {
              m_wave->addTimEvent(false, ev_time, "1", m_rise_slope, m_rise_uncertainty);
            }

          if (fall_edge)
            ev_time += m_period - m_active;
          else
            ev_time += m_active;

          fall_edge = !fall_edge;
        }
    }
  m_scene->getLayout()->invalidate();
  m_scene->update();
}

QUndoCommand* TimClockSignal::createDeleteCmd() {
  return new TimCmdRmListItem(getScene(), this);
}


void TimClockSignal::SSGWrite(SSGWriter *writer) {
  writer->writeStartElement("clock");

  writer->writeStartElement("primarytext");
  writer->writeCDATA(m_label->getText());
  writer->writeEndElement();

  writer->writeStartElement("offset_time");
  writer->writeCharacters(QString::number(m_offset));
  writer->writeEndElement();

  writer->writeStartElement("period");
  writer->writeCharacters(QString::number(m_period));
  writer->writeEndElement();

  writer->writeStartElement("active_time");
  writer->writeCharacters(QString::number(m_active));
  writer->writeEndElement();

  writer->writeStartElement("rise_slope");
  writer->writeCharacters(QString::number(m_rise_slope));
  writer->writeEndElement();

  writer->writeStartElement("fall_slope");
  writer->writeCharacters(QString::number(m_fall_slope));
  writer->writeEndElement();

  writer->writeStartElement("rise_uncertainty");
  writer->writeCharacters(QString::number(m_rise_uncertainty));
  writer->writeEndElement();

  writer->writeStartElement("fall_uncertainty");
  writer->writeCharacters(QString::number(m_fall_uncertainty));
  writer->writeEndElement();

  writer->writeEndElement();
}

void TimClockSignal::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
    m_scene->clockSettingsDialog(this);
    TimSignal::contextMenuEvent(event);
}

void TimClockSignal::setGeometry(const QRectF &rect) {
  TimSignal::setGeometry(rect);
  m_wave->setGeometry(QRectF(getDiagramSettings()->getCol0Width(), 0, getDiagramSettings()->getCol1Width(), 50));

}
