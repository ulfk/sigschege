// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimEvent.h"
#include "TimSignal.h"
#include "TimWave.h"
#include "TimCmdRmEvent.h"
#include "SSGWriter.h"
#include <cmath>
#include <QPainter>
#include <QBrush>
#include <QColor>
#include <QDebug>
#include <QGraphicsSceneMouseEvent>

TimEvent::TimEvent(QGraphicsItem *parent, bool isInitial, const QString &value, double eventTime, double slope, double uncertainty)
  : QGraphicsWidget(parent), m_isInitial(isInitial), m_value(value), m_event_time(isInitial? std::numeric_limits<double>::lowest() : eventTime),
    m_slope_time(slope), m_uncertainty_time(uncertainty)
{
  if (!isInitial)
    setFlag(ItemIsSelectable);
}

TimEvent::TimEvent(const TimEvent &rhs) {
  *this = rhs;
}

TimEvent::~TimEvent() {}

TimEvent & TimEvent::operator=(const TimEvent &rhs) {
  m_slope_time       = rhs.m_slope_time;
  m_uncertainty_time = rhs.m_uncertainty_time;
  m_event_time       = rhs.m_event_time;
  m_value            = rhs.m_value;
  return *this;
}

double TimEvent::getSlopeTime() const {
  return m_slope_time;
}

void TimEvent::setSlopeTime(double time) {
  m_slope_time = time;
}

double TimEvent::getUncertaintyTime() const {
  return m_uncertainty_time;
}

void TimEvent::setUncertaintyTime(double time) {
  m_uncertainty_time = time;
}

double TimEvent::getEventTime() const {
  return m_event_time;
}

void TimEvent::setEventTime(double time) {
  m_event_time = time;
}

bool TimEvent::operator< (const TimEvent &rhs) const {
  if (m_isInitial && !rhs.m_isInitial)
    return true;
  if (rhs.m_isInitial)
    return false;
  return getEventTime() < rhs.getEventTime();
}

bool TimEvent::operator== (const TimEvent &rhs) const {
  if (m_isInitial)
    return rhs.m_isInitial;
  if (rhs.m_isInitial)
    return false;
  return getEventTime() == rhs.getEventTime();
}

double TimEvent::getEventFinishTime() const {
  return getEventTime() + getSlopeTime();
}

void TimEvent::SSGWrite(SSGWriter *writer) const {
  writer->writeStartElement("event");

  writer->writeStartElement("slope_time");
  writer->writeCharacters(QString::number(m_slope_time));
  writer->writeEndElement();

  writer->writeStartElement("uncertainty_time");
  writer->writeCharacters(QString::number(m_uncertainty_time));
  writer->writeEndElement();

  writer->writeStartElement("event_time");
  writer->writeCharacters(QString::number(m_event_time));
  writer->writeEndElement();

  writer->writeStartElement("new_state");
  writer->writeCDATA(m_value);
  writer->writeEndElement();

  writer->writeStartElement("is_initial");
  writer->writeCharacters(QVariant(m_isInitial).toString());
  writer->writeEndElement();

  writer->writeEndElement();
}

const QString& TimEvent::getValue() const {
  return m_value;
}

void TimEvent::setValue(const QString& value) {
  m_value = value;
  TimWave* wave = getTimWave();
  if (wave != nullptr)
    wave->update();
}

void TimEvent::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {}

TimSignal* TimEvent::getTimSignal() const {
  if (parentItem()) {
    if (parentItem()->parentItem()) {
      TimSignal *temp = dynamic_cast<TimSignal*>(parentItem()->parentItem());
      if (!temp) // TODO: replace this (and other) qdebug() by something reasonable
        qDebug() << "TimEvent::getTimSignal: Conversion failed.";
      return temp;
    }
  }
  return 0;
}

TimWave* TimEvent::getTimWave() const {
  if (parentItem()) {
    return static_cast<TimWave*>(parentItem());
  }
  qDebug() << "TimEvent::getTimSignal: parent not valid.";
  return 0;
}

void TimEvent::setPrevActiveSignals(const TimEvent *prev) {
  TimSignal *sig = prev->getTimSignal();
  if (!sig) {
    m_prev_act_signal.clear();
    return;
  }
  m_prev_act_signal = sig->getBehaviorMap()->getVisualMap(prev->getValue())->getActiveSignalLevels();
}

void TimEvent::mousePressEvent(QGraphicsSceneMouseEvent *event) {

  if (event->button() == Qt::LeftButton) {
    // we do our work at mouseReleaseEvent
    event->ignore();
  } else {
    QGraphicsWidget::mousePressEvent(event);
  }
}

void TimEvent::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    event->ignore();
  } else {
    QGraphicsWidget::mouseReleaseEvent(event);
  }
}

void TimEvent::contextMenuEvent (QGraphicsSceneContextMenuEvent *event) {
  QGraphicsWidget::contextMenuEvent(event);
}

QUndoCommand* TimEvent::createDeleteCmd() {

  TimWave *wave = getTimWave();
  if (!wave)
    return 0;
  return new TimCmdRmEvent(wave, wave->getSharedEventPtr(this));
}
