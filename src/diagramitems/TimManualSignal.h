// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMMANUALSIGNAL_H_
#define TIMMANUALSIGNAL_H_

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;

#include "TimSignal.h"
#include "TimDiagramSettings.h"
#include "TimWave.h"
#include "TimEvent.h"

class TimScene;

/** @brief The @c TimManual Signal class represents a signal with manually defined states and transitions.
 *
 */
class TimManualSignal: public TimSignal {

public:
  /** @brief Creates a new @c TimSignal object.
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   * @param behaviorMap The behavior map for this signal.
   */
  TimManualSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap = QString("Logic"));

  /** @brief DTor
   * Destroys the @c TimSignal object
   */
  virtual ~TimManualSignal();

  virtual void SSGWrite(SSGWriter *writer);

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd();

  virtual void actionAddEventTool(QGraphicsSceneMouseEvent *event);
  virtual void actionRmEventTool(QGraphicsSceneMouseEvent *event);
  virtual void actionChangeEventTool(QGraphicsSceneMouseEvent *event);

  /** @brief Adds a new timing event
    *
    * @param isInitial The initial state of the signal
    * @param time Time of the new timing event
    * @param value The new state.
    * @param slope Slope for the transition to the new state
    * @param uncertainty Uncertainty for the transition to the new state
    */
   void addTimEvent(bool isInitial, double time, const QString &value, double slope, double uncertainty);

  /** Removes the Timing event at time
   *
   * @param time Time of the timing event
   */
  void rmTimEvent(double time);
  void setDefaultSlope(double time, bool update = true);
  double getDefaultSlope(void) { return m_default_slope; }
  void setDefaultUncertainty(double time, bool update = true);
  double getDefaultUncertainty(void) { return m_default_uncertainty; }
  void applyDefaults(void);
  virtual void setGeometry(const QRectF & rect);

protected:
  virtual QString getType() {return "ManualSignal";}
  virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent * event);

private:
  double m_default_slope;
  double m_default_uncertainty;

  struct {
    bool operator() (const TimEvent* i, const TimEvent* j) {
      return i->getEventTime() < j->getEventTime();
    }
    bool operator() (const TimEvent* i, const TimEvent & j) {
      return i->getEventTime() < j.getEventTime();
    }
  } ev_p_comp;

};

#endif /* TIMSIGNAL_H_ */
