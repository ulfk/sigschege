// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMMEMBERBASE_H_
#define TIMMEMBERBASE_H_

#include "TimDiagramSettings.h"
#include "TimToolInterface.h"
#include "TimDeletable.h"
#include <QGraphicsItem>
#include <QGraphicsLayoutItem>
class TimScene;
class SSGWriter;
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;

/** @brief The @c TimMember class displays a time scale in the Timing Diagram.
 *
 */
class TimMemberBase
    : public QGraphicsItem, public QGraphicsLayoutItem, public TimToolInterface, public TimDeletable
{
public:
  /** @brief Creates a new @c TimMemberBase object.
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   */
  TimMemberBase(TimMemberBase* parent, TimScene *scene);

  /** @brief DTor
   * Destroys the @c TimMemberBase object
   */
  virtual ~TimMemberBase();

  /** @ brief Factory for specific TimMemberBase types
   *
   * Supported types:
   *
   *  - signal
   *  - scale
   *
   * @param type TimMemberBase type
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   * @return Pointer to a TimMemberBase object of specific type, null if type is unknown.
   */
  static TimMemberBase* create(const QString &type, TimMemberBase *parent, TimScene *scene);

  /** @brief Returns the bounding rect for this signal.
   *
   * @return Returns the bounding rect.
   */
  virtual QRectF boundingRect() const;

  /** @brief Paints the signal.
   *
   * @param painter Pointer to the painter object
   * @param option Pointer to painter options
   * @param widget Pointer to the painting widget
   */
  virtual void
      paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {}

  /** @brief Sets the geometry of this object
   *
   * @param rect Reference to the geometry.
   */
  virtual void setGeometry(const QRectF &rect);

  /** @brief Write XML code for this class.
   *
   * This function writes the XML code for this object while saving a Sigschege XML file.
   * It is a pure virtual function which must be implemented by the derived classes (TimSignal,
   * TimScale, ...)
   *
   * @param writer Pointer to the SSGWriter object, needed for callback.
   */
  virtual void SSGWrite(SSGWriter *writer) {};

  /** @brief Get a pointer to layout data
   *
   * @return Returns a pointer to the layout data
   */
  TimDiagramSettings* getDiagramSettings() const;


  /** @brief Get a pointer to the timing scene
   *
   * @return Returns a pointer to the timing scene
   */
  virtual TimScene* getScene() const;

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd();


  /** @brief This method handles a change of the time range
   *
   * This method is called when the time range changes and the geometry
   * must be recalculated. It has to be implemented by elements which depend
   * on the time.
   */
  virtual void timeRangeChange() { }

  /** @brief This method calculates the snap time belonging to the given mouse position.
   *
   * This method is called to calculate the time that would be chosen by the time snapping
   * mechanism for the given mouse position. It can be called both from the scene for displaying
   * a marker or from the Event to calculate the place to insert a new event.
   *
   * @param xpos The horizontal position of the mouse
   * @param isWave Boolean parameter to indicate if this member is a wave or not to indicate coordinate origin. Default: false.
   * @return A time that is a multiple of the snap distance and close to the mouse.
   */
  qreal calcSnapTime(qreal xpos, bool isWave = false);

  /** @brief This method calculates the snap time
   *
   * @param time A time.
   * @return A time that is a multipl of the snap time.
   */
  qreal calcSnapTimeT(qreal time);

  /** @brief This method returns the current snap time.
   *
   */
  qreal getSnapTime() { return m_snapTime; }

  /** @brief This method returns the current deviation between snap time and real time.
   *
   */
  qreal getSnapDeviation() { return m_snapDeviation; }

  /** @brief This method returns the current height factor in percent.
   *
   */
  virtual int getHeightFactorInPercent() = 0;

  virtual int count () const;

  virtual void removeAt ( int index );

  virtual QSizeF size() const;

  virtual void setSize(const QSizeF & size);

  virtual QString getID(void);

  int getIdx(void);

  int getHeight();

protected:
  virtual QString getType() { return "UNDEF"; };

  /* @brief Returns a hint to the size of this signal.
   *
   * @param which Defines which hint type should be returned
   * @param constraint Specified any existing constraint.
   */
  virtual QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint = QSizeF()) const;

  /** @brief Pointer to the timing scene.
   *
   * This is a weak pointer.  (no ownership)
   */
  TimScene *m_scene;

  /** @brief The snap time calculated from the latest pointer movement.
   *
   */
  qreal m_snapTime;

  /** @brief Deviation between actual and snapped time.
   *
   * The deviation between the time the pointer hovers over and the time
   * that is selected by the snapping algorithm, range is -1.0 to +1.0.
   */
  qreal m_snapDeviation;
  QSizeF m_size;
};



#endif /* TIMMEMBERBASE_H_ */
