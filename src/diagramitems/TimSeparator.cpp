// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimSeparator.h"
#include "SSGWriter.h"
#include "TimLabel.h"

TimSeparator::TimSeparator(TimMemberBase *parent, TimScene *scene)
  : TimMember(parent, scene) {}

TimSeparator::~TimSeparator() {}

void TimSeparator::SSGWrite(SSGWriter *writer) {
  writer->writeStartElement("separator");
  writer->writeStartElement("primarytext");
  writer->writeCDATA(m_label->getText());
  writer->writeEndElement();
  writer->writeEndElement();
}

void TimSeparator::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  TimMember::paint(painter, option, widget);
}
