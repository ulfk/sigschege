// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimLabel.h"
#include "SSGWriter.h"
#include "TimScene.h"
#include <QGraphicsSceneMouseEvent>
#include <stdexcept>

DiagramTextItem::DiagramTextItem(QGraphicsItem *parent, QGraphicsScene *scene)
    : QGraphicsTextItem(parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
}

DiagramTextItem::DiagramTextItem(const QString &text, QGraphicsItem *parent, QGraphicsScene *scene)
  : QGraphicsTextItem(text, parent)
{
    setFlag(QGraphicsItem::ItemIsSelectable);
}

void DiagramTextItem::focusOutEvent(QFocusEvent *event)
{
    setTextInteractionFlags(Qt::NoTextInteraction);
    emit lostFocus(this);
    QGraphicsTextItem::focusOutEvent(event);
}

void DiagramTextItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) {
  if (textInteractionFlags() == Qt::NoTextInteraction) setTextInteractionFlags(Qt::TextEditorInteraction);
  QGraphicsTextItem::mouseDoubleClickEvent(event);
}

////////////////////////////////////////////////////////////////////////////////

TimLabel::TimLabel(TimMemberBase *parent, TimScene *scene, const QString &text)
  : TimMemberBase(parent, scene), m_diagramSettings(scene->getDiagramSettings())
{
  m_text = new DiagramTextItem(text, this);
}

QSizeF TimLabel::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const {
  QRectF prec = parentItem()->boundingRect();
  switch (which) {
    case Qt::MinimumSize:
      return QSizeF(m_diagramSettings->getCol0Width(), prec.width());
    case Qt::PreferredSize:
      return QSizeF(m_diagramSettings->getCol0Width(), prec.width());
    case Qt::MaximumSize:
      return QSizeF(QWIDGETSIZE_MAX, QWIDGETSIZE_MAX);
    default:
      qWarning("r::TimWave::sizeHint(): Don't know how to handle the value of 'which'");
      break;
    }
  return constraint;
}

void TimLabel::setGeometry(const QRectF &rect) {
  setPos(10, 0);
}

QRectF TimLabel::boundingRect() const {
  qreal penWidth = 1;
  return QRectF(0 - penWidth/2, 0 - penWidth/2, 30 + penWidth, 50 + penWidth);
}

void TimLabel::SSGWrite(SSGWriter *writer) {}

void TimLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  //painter->drawText(boundingRect(), Qt::AlignCenter, m_label);
  QFont font = m_text->font();
  if (font.pointSize() != getHeight()/5) {
      font.setPointSize(getHeight()/5);
      m_text->setFont(font);
    }
}

void TimLabel::mousePressEvent ( QGraphicsSceneMouseEvent * event ) {
  event->ignore();
}

void TimLabel::setText(const QString &text) {
  m_text->setPlainText(text);
}

QString TimLabel::getType() {
  return "Label";
}

int TimLabel::getHeightFactorInPercent()
{
  TimMember *signal = dynamic_cast<TimMember*>(parentItem());
  if (signal == nullptr) {
      throw std::runtime_error("Illegal type");
    }
  return signal->getHeightFactorInPercent();
}

