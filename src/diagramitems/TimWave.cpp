// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimWave.h"
#include "TimScene.h"
#include "SSGWriter.h"
#include <algorithm>
#include <memory>
#include <stdexcept>

TimWave::TimWave(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap)
  : TimMemberBase(parent, scene), m_behaviorMapId(behaviorMap)
{
  addTimEvent(TimEvent::createInitial(this, "0"));
}

TimWave::~TimWave() {}

void TimWave::timeRangeChange() {}

void TimWave::addTimEvent(bool isInitial, double time, const QString &value, double slope, double uncertainty) {
  std::shared_ptr<TimEvent> t ;
  if (isInitial)
    t = TimEvent::createInitial(this, value);
  else
    t = TimEvent::create(this, value, time, (slope < 0.0) ? 0.0 : slope, (uncertainty < 0.0) ? 0.0 : uncertainty);
  addTimEvent(t);
}

void TimWave::addTimEvent(std::shared_ptr<TimEvent> event) {
  event->setParentItem(this);
  m_events.erase(std::remove_if(m_events.begin(), m_events.end(), [event](std::shared_ptr<TimEvent> ev)
             {return (event->getEventTime() == ev->getEventTime()) || (event->getIsInitial() && ev->getIsInitial());}), m_events.end());
  m_events.push_back(event);
  std::sort(m_events.begin(), m_events.end(), ev_p_comp);
  m_scene->getLayout()->invalidate();
  m_scene->update();
}

void TimWave::rmTimEvent(double time) {
  std::shared_ptr<TimEvent> eventAt = TimEvent::create(0, "0", time);
  auto it = std::lower_bound(m_events.begin(), m_events.end(), eventAt, ev_p_comp);
  if (it == m_events.end() || (*it)->getEventTime() != time)
    return ;
  std::shared_ptr<TimEvent> t = *it;
  m_events.erase(it);

  m_scene->getLayout()->invalidate();
  m_scene->update();
}

void TimWave::rmTimEvent(std::shared_ptr<TimEvent> event) {
  auto it = std::find(m_events.begin(), m_events.end(), event);
  if (it == m_events.end() || (*it) != event)
    return ;
  m_scene->removeItem(it->get());
  m_events.erase(it);
  m_scene->getLayout()->invalidate();
  m_scene->update();
}

void TimWave::setGeometry(const QRectF &rect) {
  TimMemberBase::setGeometry(rect);
}

struct TimPaintData {
  QPainter *painter;
  double startTime;
  double endTime;
  double scaleFactor;
  TimBehaviorMap_p behMap;
  int height;
  int xOffset;
};

void TimWave::drawState(const TimPaintData& pd, const QString &state, double startTime, double endTime) {
  auto behMap = getBehaviorMap();
  std::shared_ptr<TimVisualMap> visMap = pd.behMap->getVisualMap(state);
  ActiveSignalLevels_t activeLevels = visMap->getActiveSignalLevels();
  if (endTime>pd.endTime) endTime = pd.endTime;
  if (startTime<pd.startTime) startTime = pd.startTime;
  int startX = pd.xOffset+(startTime-pd.startTime)*pd.scaleFactor;
  int endX = pd.xOffset+(endTime-pd.startTime)*pd.scaleFactor;
  std::for_each(activeLevels.begin(), activeLevels.end(), [&](int level) {
      int y = pd.height * (100-pd.behMap->getSignalLevel(level)) / 100;
      pd.painter->drawLine(startX, y, endX, y);
    });
  
  if (visMap->isValueVisible()) {
    int verticalDistanceFromEdge = visMap->getValuePos()>50? 100-visMap->getValuePos() : visMap->getValuePos();
    QRect textRect {
      startX,
      pd.height * (static_cast<int>(visMap->getValuePos())-verticalDistanceFromEdge) / 100,
      endX - startX,
      pd.height * 2 * verticalDistanceFromEdge / 100,
    };
    pd.painter->drawText(textRect, Qt::AlignCenter, state);
  }
}

void TimWave::drawTransition(const TimPaintData& pd, const QString &oldState, const TimEvent &ev, double startHiddenTime, double endHiddenTime) {
  auto visMap = pd.behMap->getVisualMap(ev.getValue());
  ActiveSignalLevels_t activeLevels = visMap->getActiveSignalLevels();
  EdgeMapping_t edgeMapping = visMap->getEdgeMapping();

  double startTime = ev.getEventTime();
  double slopeTime = ev.getSlopeTime();
  double uncertainTime = ev.getUncertaintyTime();

  int xStart = pd.xOffset + (startTime-pd.startTime)*pd.scaleFactor;
  int slopeDelta = slopeTime*pd.scaleFactor;
  int uncertainDelta = uncertainTime*pd.scaleFactor;

  ActiveSignalLevels_t prev_act_signal = pd.behMap->getVisualMap(oldState)->getActiveSignalLevels();

  if (startHiddenTime>0.0 || endHiddenTime>0.0) {
    int xClipStart = (startTime+startHiddenTime-pd.startTime)*pd.scaleFactor;
    int xClipEnd = (ev.getEventFinishTime()-endHiddenTime-pd.startTime)*pd.scaleFactor;
    pd.painter->setClipRegion(QRegion(xClipStart, 0, xClipEnd-xClipStart, pd.height));
    pd.painter->setClipping(true);
  }

  // draw edges
  // for each edge mapping
  //   check if all required previous active levels are existing
  //   if so, draw lines from all required previous active levels to target level
  for (EdgeMapping_t::const_iterator mit = edgeMapping.begin(); mit != edgeMapping.end(); ++mit) {
    
    if (std::includes(prev_act_signal.begin(), prev_act_signal.end(), mit->first.begin(), mit->first.end())) {
      unsigned int rel_level = 100 - pd.behMap->getSignalLevel(mit->second);
      unsigned int level = (pd.height * rel_level) / 100;
      
      for(ActiveSignalLevels_t::const_iterator pit = mit->first.begin(); pit != mit->first.end(); ++pit) {
        unsigned int prev_rel_level = 100 - pd.behMap->getSignalLevel(*pit);
        unsigned int prev_level = (pd.height * prev_rel_level) / 100;

        // draw edge
        pd.painter->drawLine(xStart, prev_level, xStart + slopeDelta, level);

        if (uncertainDelta != 0) {
          // draw prev_level until uncertainty ends
          pd.painter->drawLine(xStart, prev_level, xStart + uncertainDelta, prev_level);
          // draw uncertainty edge
          pd.painter->drawLine(xStart + uncertainDelta, prev_level, xStart + uncertainDelta + slopeDelta, level);
        }
      }
    }
  }
  pd.painter->setClipping(false);
}

void TimWave::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  TimPaintData pd;
  TimDiagramSettings *diaSettings = getDiagramSettings();
  QSharedPointer<TimVisualMap> visMap;
  pd.painter = painter;
  pd.behMap = getBehaviorMap();
  pd.scaleFactor = diaSettings->getScaleFactor();
  pd.height = getHeight();
  QFont font = painter->font();
  font.setPointSize(pd.height/5);
  painter->setFont(font);

  for (int rIdx = 0; rIdx<diaSettings->numRanges(); ++rIdx) {

      pd.startTime = diaSettings->getStartTime(rIdx);
      pd.endTime     = diaSettings->getEndTime(rIdx);
      pd.xOffset = diaSettings->getXStart(rIdx);

      // Process the first event
      QString lastState = m_events.size()>0? m_events[0]->getValue() : "0";
      double lastTime = pd.startTime;
      ActiveSignalLevels_t activeLevels;
      auto evIt = m_events.begin();
      std::vector<std::shared_ptr<TimEvent>>::iterator nxt_evIt;
      if (evIt != m_events.end()) {
          while (1) {
              nxt_evIt = evIt+1;
              if (nxt_evIt == m_events.end() || (*nxt_evIt)->getEventFinishTime()>pd.startTime) {
                  lastState = (*evIt)->getValue(); // this is the state the drawing starts with
                  evIt = nxt_evIt; // first event in visible area
                  break;
                }
              evIt = nxt_evIt;
            }
        }
      double currentTime = pd.startTime;
      if (evIt != m_events.end()) { // elements in visible area
          double startHiddenTime;
          double endHiddenTime;
          while (evIt != m_events.end() && currentTime<pd.endTime) {
              if ((*evIt)->getEventTime() > currentTime) {
                  drawState(pd, lastState, currentTime, (*evIt)->getEventTime());
                  startHiddenTime = 0.0;
                } else {
                  startHiddenTime = currentTime - (*evIt)->getEventTime();
                  if (currentTime!=pd.startTime) startHiddenTime /= 2;
                }
              auto nextEvIt = evIt+1;
              if (nextEvIt!=m_events.end() && (*nextEvIt)->getEventTime()<=pd.endTime) {
                  endHiddenTime = (*evIt)->getEventFinishTime()>(*nextEvIt)->getEventTime()?
                        ((*evIt)->getEventFinishTime()-(*nextEvIt)->getEventTime())/2.0 : 0.0;
                } else {
                  endHiddenTime = ((*evIt)->getEventFinishTime()>pd.endTime) ? (*evIt)->getEventFinishTime()>pd.endTime : 0.0;
                }
              drawTransition(pd, lastState, **evIt, startHiddenTime, endHiddenTime);
              currentTime = (*evIt)->getEventFinishTime();
              lastState = (*evIt)->getValue();
              evIt = nextEvIt;
            }
        }
      if (currentTime<pd.endTime) drawState(pd, lastState, currentTime, pd.endTime);
    }
}

std::shared_ptr<TimEvent> TimWave::getEvent(double time) {
  std::shared_ptr<TimEvent> eventAt = TimEvent::create(0, "0" , time);
  auto it = std::upper_bound(m_events.begin(), m_events.end(), eventAt, ev_p_comp);
  Q_ASSERT(it != m_events.begin());
  it--;
  return *it;
}

void TimWave::clear() {
  m_events.clear();
}

void TimWave::setAllSlopes(double slope) {
  for_each(m_events.begin(), m_events.end(), [&](std::shared_ptr<TimEvent> event){
      event->setSlopeTime(slope);
    });
}

void TimWave::setAllUncertainties(double uncertainty) {
  for_each(m_events.begin(), m_events.end(), [&](std::shared_ptr<TimEvent> event){
      event->setUncertaintyTime(uncertainty);
    });
}


void TimWave::SSGWrite(SSGWriter *writer) {
  writer->writeStartElement("behavior_map");
  writer->writeCDATA(m_behaviorMapId);
  writer->writeEndElement();

  writer->writeStartElement("wave");
  for (auto it = m_events.begin(); it != m_events.end(); ++it) {
    (*it)->SSGWrite(writer);
  }
  writer->writeEndElement();
}

TimBehaviorMap_p TimWave::getBehaviorMap() const {
  return getScene()->getBehaviorMapByName(getBehaviorMapId());
}

void TimWave::setBehaviorMapId(const QString &id)
{
  m_behaviorMapId = id;
}

int TimWave::getHeightFactorInPercent()
{
  TimMember *signal = dynamic_cast<TimMember*>(parentItem());
  if (signal == nullptr)
    throw std::runtime_error("Illegal type");
  return signal->getHeightFactorInPercent();
}
