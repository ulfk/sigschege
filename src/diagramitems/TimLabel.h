// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMLABEL_H_
#define TIMLABEL_H_

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;
#include "TimMember.h"
#include "TimDiagramSettings.h"
class QFocusEvent;
class QGraphicsItem;
class QGraphicsScene;
class QGraphicsSceneMouseEvent;

class DiagramTextItem : public QGraphicsTextItem
{
  Q_OBJECT
  
public:
  DiagramTextItem(QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);
  DiagramTextItem(const QString &text, QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);
  
signals:
    void lostFocus(DiagramTextItem *item);
    void selectedChange(QGraphicsItem *item);

protected:
    void focusOutEvent(QFocusEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
};


/** @brief The @c TimLabel class is used to draw the signal labels.
 *
 * The @c TimLabel class ist used to draw the signal labels.
 */
class TimLabel: public TimMemberBase {
public:

  /** @brief CTor
   *
   * @param parent Pointer to the parent object
   * @param scene Pointer to the timing scene.
   * @param text Label text.
   */
  TimLabel(TimMemberBase *parent, TimScene *scene, const QString &text = QString());

  /** @brief Returns the bounding rect.
   *
   * @return Returns the bounding rect.
   */
  virtual QRectF boundingRect() const;

  /** @brief Sets the geometry
   *
   * @param rect The geometry stored in a rect.
   */
  virtual void setGeometry(const QRectF &rect);

  virtual void SSGWrite(SSGWriter *writer);
  virtual void
        paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

  /** @brief Sets the text of the label
   *
   * @param text The new text of the label
   */
  void setText(const QString &text);

  /** @brief Return the text of the label
   *
   * @return text The text of the label
   */
  QString getText(void);

  /** @brief Creates a delete command
   *
   * This is a dummy implementation that returns NULL.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd() {return NULL;}

protected:
  virtual QString getType();

  /** @brief Returns a hint of the object size.
   *
   * @param which Specifies which hint is requested.
   * @param constraint The constrains that apply.
   */
  virtual QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint =
      QSizeF()) const;

  void mousePressEvent (QGraphicsSceneMouseEvent *event);

  virtual int getHeightFactorInPercent();

private:
  /** @brief Stores a pointer to the layout data.
   */
  TimDiagramSettings *m_diagramSettings;

  DiagramTextItem* m_text;
};

inline QString TimLabel::getText(void) {
  return m_text->toPlainText();  
}

#endif /* TIMLABEL_H_ */
