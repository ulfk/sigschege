// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMDELETABLE_H_
#define TIMDELETABLE_H_

class QUndoCommand;

/** @brief Deletable interface
 *
 */
struct TimDeletable {
  virtual ~TimDeletable() {}

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd() = 0;
};

#endif /* TIMDELETABLE_H_ */
