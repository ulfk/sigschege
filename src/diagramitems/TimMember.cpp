// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimMember.h"
#include "TimScene.h"
#include "TimLabel.h"

TimMember::TimMember(TimMemberBase *parent, TimScene *scene)
  : TimMemberBase(parent, scene), m_heightFactorInPercent(100)
{
  m_label = new TimLabel(this, getScene(), "Name");
  m_label->setGeometry(QRectF(0, 0, getDiagramSettings()->getCol0Width(), getDiagramSettings()->getItemHeight()));
}

TimMember::~TimMember() {}

void TimMember::setText (const QString &text) {
  m_label->setText(text);
}

int TimMember::getHeightFactorInPercent()
{
  return m_heightFactorInPercent;
}

void TimMember::setHeightFactorInPercent(int newValue)
{
  if (newValue > 0)
    m_heightFactorInPercent = newValue;
}

void TimMember::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  painter->save();
  if (isSelected())
    {
      painter->setBrush(QBrush(QColor(100, 100, 255, 100)));
    } else if (m_scene->getDiagramSettings()->getDrawAlternatingBackgrounds()) {
      if (m_scene->getIndexOf(this)%2 == 1)
        painter->setBrush(QBrush(m_scene->getDiagramSettings()->getAlternatingBackgroundColor()));
      else
        painter->setBrush(QBrush(QColor(255, 255, 255, 0)));
    }
  if (m_scene->getDiagramSettings()->getDrawBorderAroundMembers())
    {
      painter->setPen(m_scene->getDiagramSettings()->getMemberBorderColor());
    } else {
      painter->setPen(QColor(0, 0, 0, 0));
    }
  if (m_scene->getDiagramSettings()->getDrawAlternatingBackgrounds() || m_scene->getDiagramSettings()->getDrawBorderAroundMembers())
    painter->drawRoundedRect(boundingRect(), 5, 5);
  painter->restore();
}
