// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimManualSignal.h"
#include "TimScene.h"
#include "TimLabel.h"
#include "SSGWriter.h"
#include "TimCmdRmListItem.h"
#include "TimCmdAddEvent.h"
#include "TimCmdRmEvent.h"
#include "TimCmdChangeEvent.h"

#include <iostream>
#include <limits>
#include <algorithm>
#include <QGraphicsSceneMouseEvent>
#include <QInputDialog>
#include <QGraphicsView>
#include <QWidget>

TimManualSignal::TimManualSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap)
  : TimSignal(parent, scene, behaviorMap), m_default_slope(1.0), m_default_uncertainty(0.0)
{}

TimManualSignal::~TimManualSignal() {}

void TimManualSignal::setDefaultSlope(double time, bool update) {
  m_default_slope = time;
  if (update)
    this->update();
}

void TimManualSignal::setDefaultUncertainty(double time, bool update) {
  m_default_uncertainty = time;
  if (update)
    this->update();
}

void TimManualSignal::applyDefaults() {
  m_wave->setAllSlopes(m_default_slope);
  m_wave->setAllUncertainties(m_default_uncertainty);
}

void TimManualSignal::addTimEvent(bool isInitial, double time, const QString &value, double slope, double uncertainty) {
  m_wave->addTimEvent(isInitial, time, value, slope, uncertainty);
}

void TimManualSignal::rmTimEvent(double time) {
  m_wave->rmTimEvent(time);
}

void TimManualSignal::actionAddEventTool(QGraphicsSceneMouseEvent *event) {
  double posTime = getDiagramSettings()->getTime4AbsPos(event->pos().x());
  qreal snap_time = calcSnapTimeT(posTime);
  QString next_value;
  std::shared_ptr<TimEvent> ev = m_wave->getEvent(snap_time);
  QStringList choices;

  if (snap_time==ev->getEventTime())
    return; // cowardly refuse to have 2 events at the same time

  switch (getBehaviorMap()->getValueChoiceMethod()) {
    case TimBehaviorMap::toggle:
      if(ev) {
        next_value = getBehaviorMap()->getNextValue(ev->getValue());
      }
      break;
    case TimBehaviorMap::select:
      choices = getBehaviorMap()->getVisualMapPatterns();
      next_value = QInputDialog::getItem(dynamic_cast<QWidget*>(m_scene->parent()), QObject::tr("Choose new State Value"),
                                         QObject::tr("New State Value"), choices,
                                         0, false, nullptr);
      break;
    case TimBehaviorMap::input:
      next_value = QInputDialog::getText(dynamic_cast<QWidget*>(m_scene->parent()), QObject::tr("Enter new State Value"),
                                         QObject::tr("New State Value"), QLineEdit::Normal,
                                         "", nullptr);
      break;
    }

  getScene()->pushCmd(new TimCmdAddEvent(this, snap_time, next_value, m_default_slope, m_default_uncertainty));
}

void TimManualSignal::actionRmEventTool(QGraphicsSceneMouseEvent *event)
{
  double posTime = getDiagramSettings()->getTime4AbsPos(event->pos().x());
  qreal snap_time = calcSnapTimeT(posTime);
  auto ev = m_wave->getEvent(snap_time);

  if (snap_time==ev->getEventTime()) {
      getScene()->pushCmd(new TimCmdRmEvent(m_wave.get(), ev));
    }
}

void TimManualSignal::actionChangeEventTool(QGraphicsSceneMouseEvent *event) {
  double posTime = getDiagramSettings()->getTime4AbsPos(event->pos().x());
  auto ev = m_wave->getEvent(posTime);
  QString next_value;
  QStringList choices;
  switch (getBehaviorMap()->getValueChoiceMethod()) {
    case TimBehaviorMap::toggle:
      if(ev) {
        next_value = getBehaviorMap()->getNextValue(ev->getValue());
      }
      break;
    case TimBehaviorMap::select:
      choices = getBehaviorMap()->getVisualMapPatterns();
      next_value = QInputDialog::getItem(dynamic_cast<QWidget*>(m_scene->parent()), QObject::tr("Choose new State Value"),
                                         QObject::tr("New State Value"), choices,
                                         0, false, nullptr);
      break;
    case TimBehaviorMap::input:
      next_value = QInputDialog::getText(dynamic_cast<QWidget*>(m_scene->parent()), QObject::tr("Enter new State Value"),
                                         QObject::tr("New State Value"), QLineEdit::Normal,
                                         "", nullptr);
      break;
    }
  getScene()->pushCmd(new TimCmdChangeEvent(ev, next_value));
}


QUndoCommand* TimManualSignal::createDeleteCmd() {
  return new TimCmdRmListItem(getScene(), this);
}


void TimManualSignal::SSGWrite(SSGWriter *writer) {
  writer->writeStartElement("signal");

  writer->writeStartElement("primarytext");
  writer->writeCDATA(m_label->getText());
  writer->writeEndElement();

  writer->writeStartElement("default_slope");
  writer->writeCharacters(QString::number(m_default_slope));
  writer->writeEndElement();

  writer->writeStartElement("default_uncertainty");
  writer->writeCharacters(QString::number(m_default_uncertainty));
  writer->writeEndElement();

  m_wave->SSGWrite(writer);

  writer->writeEndElement();
}

void TimManualSignal::contextMenuEvent(QGraphicsSceneContextMenuEvent *event) {
    m_scene->signalSettingsDialog(this);
    TimSignal::contextMenuEvent(event);
}

void TimManualSignal::setGeometry(const QRectF &rect) {
  TimSignal::setGeometry(rect);
  m_wave->setGeometry(QRectF(getDiagramSettings()->getCol0Width(), 0, getDiagramSettings()->getCol1Width(), rect.height()));
}
