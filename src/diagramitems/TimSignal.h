// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMSIGNAL_H_
#define TIMSIGNAL_H_

#include <memory>
#include <QString>
#include "TimMember.h"
#include "TimDiagramSettings.h"
#include "TimBehaviorMap.h"
#include "TimWave.h"
#include "TimEvent.h"
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class QGraphicsSceneMouseEvent;
class QGraphicsSceneContextMenuEvent;
class QUndoCommand;
class TimScene;

/** @brief The @c TimSignal class represents one signal in the Timing Diagram.
 *
 */
class TimSignal: public TimMember {
public:
  /** @brief Creates a new @c TimSignal object.
   *
   * @param parent Pointer to the parent object.
   * @param scene Pointer to the timing scene.
   * @param behaviorMap The Behavior Map to use for this signal.
   */
  TimSignal(TimMemberBase *parent, TimScene *scene, const QString &behaviorMap = QString("Logic"));

  /** @brief DTor
   * Destroys the @c TimSignal object
   */
  virtual ~TimSignal();

  /** @brief Paints the signal.
   *
   * @param painter Pointer to the painter object
   * @param option Pointer to painter options
   * @param widget Pointer to the painting widget
   */
  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

  virtual void SSGWrite(SSGWriter *writer);

  /** @brief Creates a delete command
   *
   * This method create a delete command that deletes this item when executed.
   *
   * Note: The caller is responsible to delete the created command.
   *
   * @return Pointer to created delete command
   */
  virtual QUndoCommand* createDeleteCmd();
  virtual void timeRangeChange();
  virtual void actionSelectTool(QGraphicsSceneMouseEvent *event);
  TimBehaviorMap_p getBehaviorMap() const;
  std::shared_ptr<TimWave> getWave();

protected:
  std::shared_ptr<TimWave> m_wave;
  virtual void mousePressEvent (QGraphicsSceneMouseEvent *event);
  virtual void mouseReleaseEvent (QGraphicsSceneMouseEvent *event);
  virtual void contextMenuEvent (QGraphicsSceneContextMenuEvent *event);
};

#endif /* TIMSIGNAL_H_ */
