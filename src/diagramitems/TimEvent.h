// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMEVENT_H_
#define TIMEVENT_H_

#include "TimVisualMap.h"
#include "TimDeletable.h"
#include <QString>
#include <QGraphicsWidget>
#include <memory>
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class SSGWriter;
class TimSignal;
class TimWave;

class TimEvent : public QGraphicsWidget, public TimDeletable {
public:
  static inline std::unique_ptr<TimEvent> create(QGraphicsItem *parent, const QString &value = QString("0"),
                                       double eventTime = 0.0, double slope = 0.0, double uncertainty = 0.0);
  static inline std::unique_ptr<TimEvent> createInitial(QGraphicsItem *parent, const QString &value = QString("0"));

  TimEvent(const TimEvent &rhs);
  virtual ~TimEvent();
  TimEvent & operator=(const TimEvent &rhs);
  double getEventFinishTime() const;
  double getSlopeTime() const;
  void   setSlopeTime(double time);
  double getUncertaintyTime() const;
  void   setUncertaintyTime(double time);
  double getEventTime() const;
  void   setEventTime(double time);
  const QString& getValue() const;
  void setValue(const QString& value);
  bool getIsInitial() {return m_isInitial;};

  bool operator<(const TimEvent &rhs) const;
  bool operator>(const TimEvent &rhs) const;
  bool operator<=(const TimEvent &rhs) const;
  bool operator>=(const TimEvent &rhs) const;
  bool operator==(const TimEvent &rhs) const;
  bool operator!=(const TimEvent &rhs) const;

  /** @brief Write XML code for this class.
   *
   * This function writes the XML code for this object while saving a Sigschege XML file.
   * It is a pure virtual function which must be implemented by the derived classes (TimSignal,
   * TimScale, ...)
   *
   * @param writer Pointer to the SSGWriter object, needed for callback.
   */
  virtual void SSGWrite(SSGWriter *writer) const;

  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
  TimSignal *getTimSignal() const;
  TimWave *getTimWave() const;
  void setPrevActiveSignals(const TimEvent *prev);
  virtual QUndoCommand* createDeleteCmd();

protected:
  virtual void mousePressEvent (QGraphicsSceneMouseEvent *event );
  virtual void mouseReleaseEvent (QGraphicsSceneMouseEvent *event);
  virtual void contextMenuEvent (QGraphicsSceneContextMenuEvent *event );

private:
  TimEvent(QGraphicsItem *parent, bool isInitial, const QString &value = QString("0"),
           double eventTime = 0.0, double slope = 0.0, double uncertainty = 0.0);
  double m_slope_time;
  double m_uncertainty_time;
  double m_event_time;
  ActiveSignalLevels_t m_prev_act_signal;
  QString m_value;
  bool m_isInitial;
};

inline std::unique_ptr<TimEvent> TimEvent::create(QGraphicsItem *parent, const QString &value, double eventTime, double slope, double uncertainty)
{
  std::unique_ptr<TimEvent> evPtr(new TimEvent(parent, false, value, eventTime, slope, uncertainty));
  return evPtr;
}

inline std::unique_ptr<TimEvent> TimEvent::createInitial(QGraphicsItem *parent, const QString &value)
{
  std::unique_ptr<TimEvent> evPtr(new TimEvent(parent, true, value));
  return evPtr;
}

inline bool TimEvent::operator>(const TimEvent &rhs) const { return (rhs<*this);}
inline bool TimEvent::operator<=(const TimEvent &rhs) const { return !(*this>rhs);}
inline bool TimEvent::operator>=(const TimEvent &rhs) const { return !(*this<rhs);}
inline bool TimEvent::operator!=(const TimEvent &rhs) const { return !(*this==rhs);}

#endif /* TIMEVENT_H_ */
