// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimScale.h"
#include "TimScene.h"
#include "TimUtil.h"
#include "TimLabel.h"
#include "SSGWriter.h"
#include "TimCmdRmListItem.h"
using namespace std;

TimScale::TimScale(TimMemberBase *parent, TimScene *scene)
  : TimMember(parent, scene)
{
  m_label->setText(QObject::tr("Time"));
  setFlag(ItemIsSelectable);
}

TimScale::~TimScale() {}

void TimScale::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) {
  TimMember::paint(painter, option, widget);

  int height = getHeight();
  QFont font = painter->font();

  TimDiagramSettings *diaSettings;
  diaSettings = getDiagramSettings();
  double timeDist = diaSettings->getTotalTime();
  double normFactor = pow(10, floor(log10(timeDist))); 
  double timeDistNorm = timeDist/normFactor; // (1<=timeDistNorm<10)
  double labelDistNorm;
  if (timeDistNorm>8.0) {
    labelDistNorm = 2.0;
  } else if (timeDistNorm>5.0) {
    labelDistNorm = 1.0;
  } else if (timeDistNorm>2.5) {
    labelDistNorm = 0.5;
  } else if (timeDistNorm>1.5) {
    labelDistNorm = 0.2;
  } else {
    labelDistNorm = 0.1;
  }
  
  int labelCnt = static_cast<int>(floor(timeDistNorm/labelDistNorm));
  double labelDistance = labelDistNorm*normFactor;
  double tickDistance;
  if (labelCnt>5) {
    tickDistance = labelDistance/5.0;
  } else {
    tickDistance = labelDistance/10.0;
  }

  for (int rIdx = 0; rIdx<diaSettings->numRanges(); ++rIdx) {
      Range<double>timeRange(diaSettings->getTimeRange(rIdx));
      Range<int> layoutRange(diaSettings->getLayoutRange(rIdx));
      double tickTime;
      for (tickTime = timeRange.getStart(); tickTime<timeRange.getEnd(); tickTime += tickDistance) {
          int place = diaSettings->getCol0Width()+layoutRange.getStart()+static_cast<int>(static_cast<double>(layoutRange.distance())*(tickTime-timeRange.getStart())/timeRange.distance());
          painter->drawLine(place, height/10, place, 3*height/10);
      }

      int tdistcoord = layoutRange.getStart()+static_cast<int>(static_cast<double>(layoutRange.distance())*labelDistance/timeRange.distance());
      for (tickTime = timeRange.getStart(); tickTime<timeRange.getEnd(); tickTime += labelDistance) {
          QRect textBox;
          int place = diaSettings->getCol0Width()+layoutRange.getStart()+static_cast<int>(static_cast<double>(layoutRange.distance())*(tickTime-timeRange.getStart())/timeRange.distance());
          painter->drawLine(place, height/10, place, height/2);
          font.setPointSize(height/5);
          painter->setFont(font);
          painter->drawText(QRect(place-tdistcoord/2, height/2, tdistcoord, height-2), Qt::AlignHCenter|Qt::AlignTop, QString("%1").arg(tickTime, 0, 'g', -1), &textBox);
      }
  }

}

void TimScale::SSGWrite(SSGWriter *writer) {
  writer->writeStartElement("timescale");
  writer->writeStartElement("primarytext");
  writer->writeCDATA(m_label->getText());
  writer->writeEndElement();
  writer->writeEndElement();
}

QUndoCommand* TimScale::createDeleteCmd() {
  return new TimCmdRmListItem(getScene(), this);
}
