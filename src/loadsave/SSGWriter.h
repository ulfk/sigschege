// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef SSGWRITER_H_
#define SSGWRITER_H_

#include <QXmlStreamWriter>
class TimScene;
class QGraphicsLinearLayout;

/** @brief The SSG Writer writes a timing diagram in XML format.
 *
 */
class SSGWriter: public QXmlStreamWriter {
public:
  /** @brief Ctor
   *
   * @param tscene Timing diagram scene that shall be saved into the file.
   */
  SSGWriter(TimScene *tscene = 0);

  /** @brief Write XML file to device.
   *
   * This method saves the Sigschege document as an XML file into device.
   *
   * @return Returns success.
   */
  bool write(QIODevice *device);

  /** @brief Write Behavior Map Collection in XML format to device.
     *
     * This method saves the current Behavior Map Collection into device.
     *
     * @return Returns success.
     */
  bool writeBmc(QIODevice *device);

protected:
  void writeSettings(TimDiagramSettings *diaSettings);

private:
  TimScene *m_scene;
  QGraphicsLinearLayout *m_layout;
};

#endif /* SSGWRITER_H_ */
