// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimSignal.h"
#include "TimManualSignal.h"
#include "TimClockSignal.h"
#include "TimScale.h"
#include "TimSeparator.h"
#include "TimScene.h"
#include "SSGReader.h"
#include <QIODevice>

SSGReader::SSGReader(TimScene *scene) {
  m_scene = scene;
}

bool SSGReader::read(QIODevice *device) {
  setDevice(device);
  while (!atEnd()) {
    readNext();
    if (isStartElement()) {
      if (name() == "sigschege" && attributes().value("version") == "0.1")
        readSigschege();
      else
        raiseError(QObject::tr("The file is not a Sigschege version 0.1 file @") + name());
    }
  }
  return !error();
}

bool SSGReader::readBmcFile(QIODevice *device) {
  setDevice(device);
  while (!atEnd()) {
    readNext();
    if (isStartElement()) {
      if (name() == "sigschege-behaviormapcollection" && attributes().value("version") == "0.1") {
        readBmc();
      } else
        raiseError(QObject::tr("The file is not a Sigschege behavior map collection version 0.1 file @") + name());
    }
  }
  return !error();
}

bool SSGReader::readEdgeMapping(EdgeMapping_t &em) {
  std::pair<ActiveSignalLevels_t, unsigned int> newEdgeMappingTarget;
  while (!atEnd()) {
      readNext();
      if (isEndElement())
        break;
      if (isStartElement()) {
          QString valString = readElementText();
          bool ok;
          int value = valString.toInt(&ok);
          if (!ok || value<0 || value>100)
            return false;
          if (name() == "source") {
              newEdgeMappingTarget.first.push_back(value);
            } else if (name() == "target") {
              newEdgeMappingTarget.second = value;
            }
        }
    }
  em.push_back(newEdgeMappingTarget);
  return true;
}

bool SSGReader::readVisualMap(TimBehaviorMap &bm) {
  auto vm = std::make_shared<TimVisualMap>();
  EdgeMapping_t em;
  while (!atEnd()) {
      readNext();
      if (isEndElement()) {
          break;
        }
      if (isStartElement()) {
          if (name() == "pattern") {
              QString pattern = readElementText();
              vm->setPattern(pattern);
            } else if (name() == "level") {
              QString levelStr = readElementText();
              bool ok;
              int level = levelStr.toInt(&ok);
              if (!ok || level<0 || level>100)
                return false;
              if (!vm->insertSignalLevel(level))
                return false;
            } else if (name() == "edgemapping") {
              if (!readEdgeMapping(em))
                return false;
            } else if (name() == "valuevisible") {
              QString valString = readElementText();
              vm->setValueVisible(valString=="true");
            } else if (name() == "valuepos") {
              QString valString = readElementText();
              vm->setValuePos(valString.toInt());
            }
        }
    }
  vm->setEdgeMapping(em);
  return true;
}

bool SSGReader::readBehaviorMap(std::shared_ptr<TimBehaviorMapCollection> bmc) {
    bool nameFound = false;
    auto bm = std::make_shared<TimBehaviorMap>();
    while (!atEnd()) {
      readNext();
      if (isEndElement()) {
       break;
        }
      if (isStartElement()) {
        if (name() == "name") {
            if (nameFound) return false;
            QString name = readElementText();
            bmc->registerBehaviorMap(name, bm);
          }
        else if (name() == "defaultMap") {
            QString pattern = readElementText();

          }
        else if (name() == "visualmap") {
            readVisualMap(*bm);
          }
        }
  }
  return nameFound;
}

bool SSGReader::readBmc() {
  while (!(isStartElement() && name() == "behaviormapcollection")) {
      readNext();
      if (atEnd())
        return false;
    }
  auto bmc = std::make_shared<TimBehaviorMapCollection>();
  while (!atEnd()) {
      readNext();
      if (isEndElement()) {
        break;
      }
      if (isStartElement()) {
          if (name() == "behaviormap") {
              readBehaviorMap(bmc);
            }
        }
    }
  return true;
}

void SSGReader::readSigschege() {
  Q_ASSERT(isStartElement() && name() == "sigschege");
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "settings")
        readSettings();
      else if (name() == "signal")
        readSignal();
      else if (name() == "clock")
        readClock();
      else if (name() == "timescale")
        readTimeScale();
      else if (name() == "separator")
        readSeparator();
      else if (name() == "behaviormapcollection")
        readBmc();
    }
  }
}

void SSGReader::readSettings() {
  Q_ASSERT(isStartElement() && name() == "settings");
  auto diaSettings = m_scene->getDiagramSettings();
  double startTime, endTime;
  diaSettings->clearTimeRanges();
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "startTime") {
        startTime = QString(readElementText()).toDouble();
      } else if (name() == "endTime") {
        endTime = QString(readElementText()).toDouble();
        diaSettings->addTimeRange(startTime, endTime);
      } else if (name() == "col0width") {
        diaSettings->setCol0Width(readElementText().toInt());
      } else if (name() == "col1width") {
        diaSettings->setCol1Width(readElementText().toInt());
      } else if (name() == "itemHeight") {
        diaSettings->setItemHeight(readElementText().toInt());
      } else if (name() == "drawBorderAroundMembers") {
        diaSettings->setDrawBorderAroundMembers(QVariant(readElementText()).toBool());
      } else if (name() == "memberBorderColor") {
        diaSettings->setMemberBorderColor(QColor(readElementText()));
      } else if (name() == "drawAlternatingBackgrounds") {
        diaSettings->setDrawAlternatingBackgrounds(QVariant(readElementText()).toBool());
      } else if (name() == "alternatingBackgroundColor") {
        diaSettings->setAlternatingBackgroundColor(QColor(readElementText()));
      }
    }
  }
}

void SSGReader::readClock() {
  Q_ASSERT(isStartElement() && name() == "clock");
  TimClockSignal *new_clock =
      dynamic_cast<TimClockSignal*> (m_scene->insertTimMemberByType("clock"));
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "primarytext")
          new_clock->setText(readElementText());
      else if (name() == "offset_time") {
          new_clock->setOffset(QString(readElementText()).toDouble());
      } else if (name() == "period") {
          new_clock->setPeriod(QString(readElementText()).toDouble());
      } else if (name() == "active_time") {
          new_clock->setActive(QString(readElementText()).toDouble());
      } else if (name() == "rise_slope") {
          new_clock->setRiseSlope(QString(readElementText()).toDouble());
      } else if (name() == "fall_slope") {
          new_clock->setFallSlope(QString(readElementText()).toDouble());
      } else if (name() == "rise_uncertainty") {
          new_clock->setRiseUncertainty(QString(readElementText()).toDouble());
      } else if (name() == "fall_uncertainty") {
          new_clock->setFallUncertainty(QString(readElementText()).toDouble());
      }
    }
  }
}

void SSGReader::readSignal() {
  Q_ASSERT(isStartElement() && name() == "signal");
  TimManualSignal *new_signal =
      dynamic_cast<TimManualSignal*> (m_scene->insertTimMemberByType("signal"));
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "primarytext") {
        new_signal->setText(readElementText());
      } else if (name() == "default_slope") {
	new_signal->setDefaultSlope(QString(readElementText()).toDouble());
      } else if (name() == "default_uncertainty") {
	new_signal->setDefaultUncertainty(QString(readElementText()).toDouble());
      } else if (name() == "behavior_map") {
        new_signal->getWave()->setBehaviorMapId(readElementText());
      } else if (name() == "wave") {
        readWave(new_signal);
      }
    }
  }
}

void SSGReader::readWave(TimManualSignal *signal) {
  Q_ASSERT(isStartElement() && name() == "wave");
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "event") {
        double event_time;
        double slope_time;
        double uncertainty_time;
        bool isInitial {false};
        QString new_state;
        while (!atEnd()) {
          readNext();
          if (isEndElement())
            break;
          if (isStartElement()) {
            if (name() == "slope_time") {
              slope_time = QString(readElementText()).toDouble();
            } else if (name() == "uncertainty_time") {
              uncertainty_time = QString(readElementText()).toDouble();
            } else if (name() == "event_time") {
              event_time = QString(readElementText()).toDouble();
            } else if (name() == "new_state") {
              new_state = QString(readElementText());
            } else if (name() == "is_initial") {
              isInitial = QVariant(readElementText()).toBool();
            }
          }
        }
        signal->addTimEvent(isInitial, event_time, new_state, slope_time, uncertainty_time);
      }
    }
  }
}

void SSGReader::readTimeScale() {
  Q_ASSERT(isStartElement() && name() == "timescale");
  QString title = "";
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "primarytext")
        title = readElementText();
    }
  }
  TimScale *new_scale =
      dynamic_cast<TimScale*> (m_scene->insertTimMemberByType("scale", -1, title));
}

void SSGReader::readSeparator() {
  Q_ASSERT(isStartElement() && name() == "separator");
  QString title = "";
  while (!atEnd()) {
    readNext();
    if (isEndElement())
      break;
    if (isStartElement()) {
      if (name() == "primarytext")
        title = readElementText();
    }
  }
  TimSeparator *new_separator =
      dynamic_cast<TimSeparator*> (m_scene->insertTimMemberByType("separator", -1, title));
}
