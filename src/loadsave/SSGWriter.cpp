// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include <QIODevice>
#include <typeinfo>
#include <iostream>
#include "TimSignal.h"
#include "TimScale.h"
#include "TimScene.h"
#include "SSGWriter.h"

using namespace std;

SSGWriter::SSGWriter(TimScene *scene) {
  m_scene = scene;
  m_layout = scene->getLayout();
  setAutoFormatting(true);
}

void SSGWriter::writeSettings(TimDiagramSettings *diaSettings)
{
  writeStartElement("settings");

  writeStartElement("col0width");
  writeCharacters(QString::number(diaSettings->getCol0Width()));
  writeEndElement();

  writeStartElement("col1width");
  writeCharacters(QString::number(diaSettings->getCol1Width()));
  writeEndElement();

  writeStartElement("itemHeight");
  writeCharacters(QString::number(diaSettings->getItemHeight()));
  writeEndElement();

  writeStartElement("drawBorderAroundMembers");
  writeCharacters(QString::number(diaSettings->getDrawBorderAroundMembers()));
  writeEndElement();

  writeStartElement("memberBorderColor");
  writeCharacters(diaSettings->getMemberBorderColor().name());
  writeEndElement();

  writeStartElement("drawAlternatingBackgrounds");
  writeCharacters(QString::number(diaSettings->getDrawAlternatingBackgrounds()));
  writeEndElement();

  writeStartElement("alternatingBackgroundColor");
  writeCharacters(diaSettings->getAlternatingBackgroundColor().name());
  writeEndElement();

  for (int rIdx = 0; rIdx<diaSettings->numRanges(); ++rIdx) {
      writeStartElement("startTime");
      writeCharacters(QString::number(diaSettings->getStartTime(rIdx)));
      writeEndElement();

      writeStartElement("endTime");
      writeCharacters(QString::number(diaSettings->getEndTime(rIdx)));
      writeEndElement();
  }
  writeEndElement();
}

bool SSGWriter::write(QIODevice *device) {
  auto diaSettings = m_scene->getDiagramSettings();
  setDevice(device);
  
  writeStartDocument();
  writeDTD("<!DOCTYPE sigschege>");
  writeStartElement("sigschege");
  writeAttribute("version", "0.1");
  writeSettings(diaSettings);
  m_scene->writeBehaviorMapCollection(this);

  // iterate through the QGraphicsLinearLayout, which contains the timing diagram
  // Each member must have SSGWrite() implemented to write its content itself (using
  // a backpointer here)
  int index;
  for (index = 0; index < m_layout->count(); ++index) {
    dynamic_cast<TimMember*>(m_layout->itemAt(index))->SSGWrite(this);
  }

  writeEndDocument();
  return true;
}

bool SSGWriter::writeBmc(QIODevice *device) {
  setDevice(device);
  writeStartDocument();
  writeDTD("<!DOCTYPE sigschege-bmc>");
  writeStartElement("sigschege-behaviormapcollection");
  writeAttribute("version", "0.1");
  m_scene->getBehaviorMapCollection()->SSGWrite(this);
  writeEndElement();
  writeEndDocument();
  return true;
}

