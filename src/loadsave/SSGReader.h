// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef SSGREADER_H_
#define SSGREADER_H_

#include <QXmlStreamReader>
class TimScene;
class TimSignal;
class TimManualSignal;
#include <memory>

/** @brief The SSG Reader reads a timing diagram in XML format.
 *
 */
class SSGReader: public QXmlStreamReader {
public:
  /** @brief Ctor
   *
   * @param scene Timing diagram scene to read file into.
   */
  SSGReader(TimScene *scene = 0);

  /** @brief Reads timing diagram from file.
   *
   * This method reads a Sigschege document from a file in XML format.
   *
   * @return Returns success.
   */
  bool read(QIODevice *device);

  /** @brief Reads a behavior map collection from file.
     *
     * This method reads a Sigschege behavior map collection from a file in XML format.
     *
     * @return Returns success.
     */
  bool readBmcFile(QIODevice *device);

private:
  TimScene *m_scene;

  /** @brief Read an Edge Mapping of a Visual Map from the XML file.
     *
     */
  bool readEdgeMapping(EdgeMapping_t &em);
  /** @brief Read a Visual Map from the XML file.
     *
     */
  bool readVisualMap(TimBehaviorMap &bm);
  /** @brief Read a Behavior Map from the XML file.
     *
     */
  bool readBehaviorMap(std::shared_ptr<TimBehaviorMapCollection> bmc);
  /** @brief Read the Behavior Map Collection from the XML file.
     *
     */
  bool readBmc();
  /** @brief Read the Sigschege content from the XML file.
     *
     */
  void readSigschege();

  /** @brief Read the diagram settings from the XML file.
     *
     */
  void readSettings();

  /** @brief Read a signal declaration from the XML file.
     *
     */
  void readSignal();

  /** @brief Read a clock declaration from the XML file.
     *
     */
  void readClock();

  /** @brief Read a wave declaration from the XML file.
     *
     */
  void readWave(TimManualSignal *signal);

  /** @brief Read a timescale declaration from the XML file.
     *
     */
  void readTimeScale();

  /** @brief Read a separator declaration from the XML file.
     *
     */
  void readSeparator();
};

#endif /* SSGREADER_H_ */
