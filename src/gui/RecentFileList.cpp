// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "RecentFileList.h"
#include <stdexcept>
#include <sstream>
#include <set>
#include <algorithm>
#include <QString>
#include <QAction>
#include <QFileInfo>

using namespace std;

RecentFileList::RecentFileList() : QObject(), maxFiles(15) {}


void RecentFileList::push_back(const QString &filePath) {
  _push_back(filePath);
  _finishChange();
}

void RecentFileList::_finishChange() {
  emit changed();
  oldRecentFiles.clear();
}

void RecentFileList::_push_back(const QString &filePath) {
  QFileInfo fileInfo(filePath);
  shared_ptr<QAction> newQAction = make_shared<QAction>(fileInfo.fileName(), nullptr);
  newQAction->setToolTip(filePath);
  connect(newQAction.get(), &QAction::triggered, this, [newQAction,this](){emit loadRecentFile(newQAction->toolTip());});
  recentFiles.push_back(newQAction);
  _cleanUp();
  _finishChange();
}

void RecentFileList::_cleanUp() {
  set<QString> existing;
  oldRecentFiles = recentFiles;
  recentFiles.erase(recentFiles.rend().base(),
                    remove_if(recentFiles.rbegin(), recentFiles.rend(),
                              [&](shared_ptr<QAction> a)
                    {
                      bool result = existing.contains(a->toolTip());
                      existing.insert(a->toolTip());
                      return result;
                    }).base());
  if (recentFiles.size()>maxFiles)
    recentFiles.erase(recentFiles.begin(), recentFiles.begin()+(recentFiles.size()-maxFiles));
}


unsigned int RecentFileList::size() const {
  return recentFiles.size();
}


shared_ptr<QAction> RecentFileList::operator [](unsigned int i) const {
  if (i>=recentFiles.size())
    {
      std::ostringstream s;
      s << "RecentFileList: " <<  i << " of " << recentFiles.size() << endl;
      throw std::out_of_range(s.str());
    }
  return recentFiles[i];
}


void RecentFileList::setFiles(const QStringList &filePaths) {
  for (auto filePath: filePaths) {
      _push_back(filePath);
    }
  _finishChange();
}


QStringList RecentFileList::getFiles() {
  QStringList result;
  for (auto action: recentFiles) {
      result.push_back(action->toolTip());
    }
  return result;
}

void RecentFileList::setMaxFiles(unsigned int value) {
  maxFiles = value;
  if (maxFiles<recentFiles.size()) {
     _cleanUp();
     _finishChange();
    }
}

unsigned int RecentFileList::getMaxFiles() {
  return maxFiles;
}

shared_ptr<QAction> &RecentFileList::operator [](unsigned int i) {
  if (i>=recentFiles.size())
    {
      std::ostringstream s;
      s << "RecentFileList: " <<  i << " of " << recentFiles.size() << endl;
      throw std::out_of_range(s.str());
    }
  return recentFiles[i];
}
