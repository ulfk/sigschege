// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#ifndef TIMLAYOUTDATA_H_
#define TIMLAYOUTDATA_H_

#include <map>
#include <vector>
#include <utility>
#include <QString>
#include <QVariant>
#include <QColor>
#include "TimUtil.h"

typedef std::map<QString, QVariant> TimDiagramSettings_t;

/** @brief The @c TimDiagramSettings class stores all "global" timing diagram data.
 *
 */
class TimDiagramSettings {
public:

  /** @brief CTor
   */
  TimDiagramSettings();

  /** @brief DTor
   */
  virtual ~TimDiagramSettings();
  int numRanges() { return m_timeRanges.size(); }

  /** @brief Get the start time of range idx 
   *
   * @param idx Index of the queried time range
   * @return Start time of range idx
   */
  double getStartTime(int idx) const;

  void clearTimeRanges(void);

  /** @brief Add a time range. If the range overlaps with an existing range it will be refused
   *
   * @param range The new time range
   * @return True if the time range was added.
   */
  bool addTimeRange(const Range<double> &range);

  /** @brief Add a time range. If the range overlaps with an existing range it will be refused
   *
   * @param start Start time
   * @param end End time
   * @return True if the time range was added.
   */
  bool addTimeRange(double start, double end);

  /** @brief Add a time range. An allowed time range will automatically be chosen.
   *
   * @return True if the time range was added (and it always will).
   */
  bool addTimeRange();

  /** @brief Set the start time for range idx. This will only be done if it does not cause an overlap.
   *
   * @param idx Index of the queried time range
   * @param start Start time
   * @return True if the change was done.
   */
  bool setStartTime(int idx, double start);

  /** @brief Set the end time.
   *
   * @param idx Index of the queried time range
   * @param end End time
   * @return True if the change was done.
   */
  bool setEndTime(int idx, double end);

  /** @brief Get the end time of range idx
   *
   * @param idx Index of the queried time range
   * @return End time.
   */
  double getEndTime(int idx) const;

  /** @brief Returns the time range with index idx.
   * @return Time range
   */
  const Range<double> &getTimeRange(int idx) { return m_timeRanges[idx]; }

  /** @brief Get the snap delta time
   *
   * @return Snap delta time .
   */
  double getSnapDeltaTime() const { return m_snapDeltaTime; }

  /** @brief Set the snap delta.
   *
   * @param delta Snap delta time .
   */
  void setSnapDeltaTime(double delta) { m_snapDeltaTime = delta; }

  /** @brief Get the width of col 0 (label).
   *
   * @return Width in pixel.
   */
  unsigned int getCol0Width() const { return m_col0Width; }

  /** @brief Set width of col 0 (label)
   * @param width Width in pixel.
   */
  void setCol0Width(int width) { m_col0Width = width; updateScaleFactor(); }

  /** @brief Get the width of col 1 (waveform).
   *
   * @return Width in pixel.
   */
  int getCol1Width() const { return m_col1Width; }

  /** @brief Set width of col 1 (waveform)
   * @param width Width in pixel.
   */
  void setCol1Width(int width) { m_col1Width = width; updateScaleFactor(); }

  /** @brief Get the item height.
   *
   * @return Height in pixel.
   */
  int getItemHeight() const { return m_itemHeight; }

  /** @brief Set height of each item
   * @param height Height in pixel.
   */
  void setItemHeight(int height) { m_itemHeight = height; updateScaleFactor(); }

  /** @brief Get the full width.
   *
   * @return Full width in pixel.
   */
  int getFullWidth() const { return m_col0Width+m_col1Width; }

  /** @brief Returns the scale factor
   * @return The scale factor
   */
  double getScaleFactor() const { return m_scaleFactor; }

  /** @brief Returns the total time, i.e. the sum of all time ranges
   * @return Total time
   */
  double getTotalTime() const { return m_totalTime; }

  /** @brief Returns the start position layout range with index idx.
   * @return Start position of layout range
   */
  int getXStart(int idx) { return m_layoutRanges[idx].getStart(); }

  /** @brief Returns the end position layout range with index idx.
   * @return End position of layout range
   */
  int getXEnd(int idx) { return m_layoutRanges[idx].getEnd(); }

  /** @brief Returns the layout range with index idx.
   * @return Layout range
   */
  const Range<int> &getLayoutRange(int idx) { return m_layoutRanges[idx]; }

  /** @brief Returns the time at absolute X position absPos
   * @return Corresponding time
   */
  double getTime4AbsPos(int absPos);
  int getIdx4AbsPos(int absPos);

  bool getDrawBorderAroundMembers() { return drawBorderAroundMembers; }
  void setDrawBorderAroundMembers(bool drawBorderAroundMembers) { this->drawBorderAroundMembers = drawBorderAroundMembers; }
  QColor getMemberBorderColor() { return memberBorderColor; }
  void setMemberBorderColor(const QColor &memberBorderColor) { this->memberBorderColor = memberBorderColor; }
  bool getDrawAlternatingBackgrounds() { return drawAlternatingBackgrounds; }
  void setDrawAlternatingBackgrounds(bool drawAlternatingBackgrounds) { this->drawAlternatingBackgrounds = drawAlternatingBackgrounds; }
  QColor getAlternatingBackgroundColor() { return alternatingBackgroundColor; }
  void setAlternatingBackgroundColor(const QColor &alternatingBackgroundColor) { this->alternatingBackgroundColor = alternatingBackgroundColor; }

  bool operator==(const TimDiagramSettings &rhs);

private:
  /** @brief Recalculates the m_scale_factor
   */
  void updateScaleFactor();

  int m_col0Width, m_col1Width;
  int m_itemHeight;
  std::vector< Range<double> > m_timeRanges;
  std::vector< Range<int> > m_layoutRanges;
  double m_snapDeltaTime;
  bool m_useSnapping;
  int m_rangeSeparatorWidth;
  double m_scaleFactor;
  double m_totalTime;
  bool drawBorderAroundMembers;
  QColor memberBorderColor;
  bool drawAlternatingBackgrounds;
  QColor alternatingBackgroundColor;
};

inline double TimDiagramSettings::getStartTime(int idx) const {
  return idx<0 || idx>=m_timeRanges.size()? 0.0 : m_timeRanges[idx].getStart();
}

inline double TimDiagramSettings::getEndTime(int idx) const {
  return idx<0 || idx>=m_timeRanges.size()? 0.0 : m_timeRanges[idx].getEnd();
}

#endif /* TIMLAYOUTDATA_H_ */
