// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QToolBar>
#include <QComboBox>
#include <QMainWindow>
#include "TimToolAction.h"
#include "RecentFileList.h"
class TimScene;
class TimClockSignal;
class TimManualSignal;
#include <QGraphicsView>
#include <QMenuBar>
#include <QMenu>

/** @brief Main window of Sigschege
 */
class MainWindow: public QMainWindow {
Q_OBJECT

public:
  /** @brief CTor
   *
   * @param ssgFile File name of the corresponding SSG file
   * @param parent Pointer to the parent object
   */
  MainWindow(const QString &ssgFile, QWidget *parent = 0);

  /** @brief DTor
   */
  virtual ~MainWindow();

public slots:
  void loadRecentFile(const QString& filePath);

private:
  /** @brief Create all action objects
   */
  void createActions();

  /** @brief Create menus.
   */
  void createMenus();

  /** @brief Create tool bars
   */
  void createToolBars();

  /** @brief Create status bar
   */
  void createStatusBar();

  /** @brief Create timing diagram scene and view.
   */
  void createTopView();

  /** @brief Adapt the view size to fit the mainwindow width
   */
  void viewToWidth(void);

protected:
  void closeEvent(QCloseEvent *event);
  virtual void resizeEvent ( QResizeEvent * event );
  
private:
  QAction *m_newAct;
  QAction *m_openAct;
  QAction *m_saveAct;
  QAction *m_saveAsAct;
  QAction *m_exportAct;
  QAction *m_loadBmcAct;
  QAction *m_saveBmcAct;
  QAction *m_exitAct;
  QAction *m_addSignalAct;
  QAction *m_addBusAct;
  QAction *m_addClockAct;
  QAction *m_addScaleAct;
  QAction *m_addSeparator;
  QAction *m_rmSignalAct;
  QAction *m_setDiaProps;
  QAction *m_appSettingsAct;
  QAction *m_editBehaviorMaps;

  QActionGroup *m_SigGroup;
  TimToolActions_t m_toolActions;

  QAction *m_undoCmd;
  QAction *m_redoCmd;

  QAction *m_aboutAct;

  QMenu *m_fileMenu;
  QMenu *m_recentFilesMenu;
  QMenu *m_editMenu;
  QMenu *m_helpMenu;

  QToolBar *m_editToolBar;
  QToolBar *m_signalToolBar;

  QGraphicsView *m_view;
  TimScene *m_scene;

  QComboBox *sceneScaleCombo;

  RecentFileList m_recentFiles;

  /** @brief If this file is modified, ask the user what do do.
   *
   * This method is called if the current timing diagram will be obsoleted. It checks if it
   * is modified and asks the user what to do.
   * The choices are save, discard and cancel. 
   *
   * @return Returns true if the action that asks to invalidate the document may proceed, i.e.
   * the user chose Discard or Save.
   */
  bool maybeSave();
  QString askForFileName(void);
  bool save(QString &fileName);
  bool loadFile(const QString &fileName);
  bool loadBmcFile(const QString &fileName);

  QString m_filename;
  bool m_keepViewToWidth;

private slots:
  void cmdNew();
  void cmdOpen();
  void cmdSave();
  void cmdSaveAs();
  void cmdExport();
  void cmdLoadBmc();
  void cmdSaveBmc();
  void cmdAddSignal();
  void cmdAddBus();
  void cmdAddClock();
  void cmdAddScale();
  void cmdAddSeparator();
  void cmdEditDiaProperties();
  void cmdEditAppSettings();
  void cmdEditBehaviorMaps();
  void cmdEditClockProperties(TimClockSignal* clock);
  void cmdEditSignalProperties(TimManualSignal* signal);
  void selectionChanged();
  void updateRecentFiles();
  void about();

  /** @brief A scale change of the scene was requested, change it in the view.
   */
  void sceneScaleChanged(const QString &scale);
  void sceneGeometryChanged();
};

#endif // MAINWINDOW_H
