// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimDiagramSettings.h"

TimDiagramSettings::TimDiagramSettings()
  : m_col0Width(50),
    m_col1Width(400),
    m_itemHeight(50),
    m_snapDeltaTime(5.0),
    m_useSnapping(true),
    m_rangeSeparatorWidth(11),
    drawBorderAroundMembers(false),
    memberBorderColor(QColor("green")),
    drawAlternatingBackgrounds(true),
    alternatingBackgroundColor("lightgrey")
{
  m_timeRanges.clear();
  m_timeRanges.push_back(Range<double>(0.0, 100.0));
  updateScaleFactor();
}

TimDiagramSettings::~TimDiagramSettings() {}

void TimDiagramSettings::clearTimeRanges()
{
  m_timeRanges.clear();
}

bool TimDiagramSettings::addTimeRange(const Range<double> &range) {
  std::vector<Range<double>>::iterator it;
  if (m_timeRanges.empty()) {
      it = m_timeRanges.begin();
    } else {
      it = lower_bound(m_timeRanges.begin(), m_timeRanges.end(), range);

      if ((it-1!=m_timeRanges.begin() && (it-1)->getEnd()>=range.getStart())
          || (it!=m_timeRanges.end() && range.getEnd()>=it->getStart())) return false;
    }
  m_timeRanges.insert(it, range);
  updateScaleFactor();
  return true;
}

bool TimDiagramSettings::addTimeRange(double start, double end) {
    return addTimeRange(Range<double>(start, end));
}

bool TimDiagramSettings::addTimeRange() {
  double startTime;
  double distance;
  if (m_timeRanges.empty()) {
      startTime = 0.0;
      distance = 100.0;
  } else {
    auto lastRange = m_timeRanges[m_timeRanges.size()-1];
    double maxTime = lastRange.getEnd();
    distance = lastRange.distance();
    startTime = maxTime + distance;
}
    bool result = addTimeRange(Range<double>(startTime, startTime + distance));
    updateScaleFactor();
    return result;
}

bool TimDiagramSettings::setStartTime(int idx, double start) {
    if ((idx>0 && m_timeRanges[idx-1].getEnd()>start) || start>m_timeRanges[idx].getEnd()) return false;
    m_timeRanges[idx].setStart(start);
    updateScaleFactor();
    return true;
}

bool TimDiagramSettings::setEndTime(int idx, double end) {
  if (m_timeRanges[idx].getStart()>end || (m_timeRanges.size()>idx+1 && end>m_timeRanges[idx+1].getStart())) return false;
  m_timeRanges[idx].setEnd(end);
  updateScaleFactor();
  return true;
}

void TimDiagramSettings::updateScaleFactor() {
    int numRanges = m_timeRanges.size();
    int timeWidth = m_col1Width - (numRanges-1)*m_rangeSeparatorWidth;
    m_totalTime = 0.0;
    for_each(m_timeRanges.begin(), m_timeRanges.end(), [&](Range<double> range){
        m_totalTime += range.distance();
    });
    m_scaleFactor = timeWidth / (m_totalTime>0.0? m_totalTime : 1.0);
    int startX = 0;
    m_layoutRanges.clear();
    for_each(m_timeRanges.begin(), m_timeRanges.end(), [&](Range<double> range){
        m_layoutRanges.push_back(Range<int>(startX, startX+range.distance()*m_scaleFactor));
        startX += m_rangeSeparatorWidth+range.distance()*m_scaleFactor;
    });
}


double TimDiagramSettings::getTime4AbsPos(int absPos) {
  int pos = absPos - m_col0Width;
  for (int i=0; i<m_layoutRanges.size(); ++i) {
      if (m_layoutRanges[i].contains(pos)) {
          return m_timeRanges[i].getStart() + static_cast<double>(pos-m_layoutRanges[i].getStart())/m_scaleFactor;
        }
    }
  return 0.0;
}

int TimDiagramSettings::getIdx4AbsPos(int absPos) {
    int pos = absPos - m_col0Width;
    for (int i=0; i<m_layoutRanges.size(); ++i) {
        if (m_layoutRanges[i].contains(pos)) {
            return i;
        }
    }
    return -1;
}

bool TimDiagramSettings::operator==(const TimDiagramSettings &rhs)
{
  return m_col0Width == rhs.m_col0Width
      && m_col1Width == rhs.m_col1Width
      && m_itemHeight == rhs.m_itemHeight
      && m_timeRanges == rhs.m_timeRanges
      && m_layoutRanges == rhs.m_layoutRanges
      && m_snapDeltaTime == rhs.m_snapDeltaTime
      && m_useSnapping == rhs.m_useSnapping
      && m_rangeSeparatorWidth == rhs.m_rangeSeparatorWidth
      && m_scaleFactor == rhs.m_scaleFactor
      && drawBorderAroundMembers == rhs.drawBorderAroundMembers
      && drawAlternatingBackgrounds == rhs.drawAlternatingBackgrounds
      && memberBorderColor == rhs.memberBorderColor
      && alternatingBackgroundColor == rhs.alternatingBackgroundColor;

}
