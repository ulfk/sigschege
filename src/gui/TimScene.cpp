// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include "TimScene.h"
#include "TimUtil.h"
#include "TimMemberBase.h"
#include "TimScale.h"
#include "TimSignal.h"
#include "TimClockSignal.h"
#include "TimCmdInsertListItem.h"
#include "TimDeletable.h"
#include "TimToolAction.h"
#include "TimToolSelect.h"
#include "TimToolAddEvent.h"
#include "TimToolRmEvent.h"
#include "TimToolChangeEvent.h"
#include "TimBehaviorMap.h"
#include "TimVisualMap.h"
#include <cassert>
#include <iostream>
#include <memory>
#include <QGraphicsSceneMouseEvent>
#include "TimCmdChangeDiagramSettings.h"

using namespace std;

TimScene::TimScene(QObject *parent) :
    QGraphicsScene(parent) {
  m_layout = new QGraphicsLinearLayout(Qt::Vertical);
  m_layout->setMinimumWidth(width());
  m_layout->setSpacing(5.0);

  QGraphicsWidget *form = new QGraphicsWidget;
  form->setLayout(m_layout);
  addItem(form);

  m_posLine = nullptr;

  m_tools = new TimToolCollection(this);
  m_tools->registerTool(new TimToolSelect());
  m_tools->registerTool(new TimToolAddEvent());
  m_tools->registerTool(new TimToolRmEvent());
  m_tools->registerTool(new TimToolChangeEvent());

  defaultBehaviorMaps();
  updateRect();
}

TimScene::~TimScene() {}

void TimScene::clear(void) {
  int cnt = 0;
  int index;
  QGraphicsLayoutItem *item;
  if (m_layout != 0) {
    cnt = m_layout->count();
  }

  for (index = cnt - 1; index >= 0; --index) {
    item = m_layout->itemAt(index);
    m_layout->removeAt(index);
    // then remove it from the scene
    removeItem(dynamic_cast<QGraphicsItem*>(item));
  }

  m_layout->setMaximumSize(0, 0); // adapt the size
  m_undoStack.clear();
}

void TimScene::insertTimListItem(int index, TimMemberBase *item) {
  // add item to graphic scene
  addItem(item);
  // add item to layout manager
  m_layout->insertItem(index, item);
  updateRect();
}

int TimScene::removeTimListItem(TimMemberBase *item) {
  // indexOf is missing in Qt < 4.6 :-(
  int cnt = m_layout->count();
  int index;
  for (index = 0; index < cnt; ++index) {
    if (item == m_layout->itemAt(index)) {
      break;
    }
  }
  // First remove the signal from the layout
  m_layout->removeItem(item);
  m_layout->setMaximumSize(0, 0); // adapt the size
  // then remove it from the scene
  removeItem(item);
  update();
  return index;
}

TimDiagramSettings* TimScene::getDiagramSettings() {
  return &m_diagramSettings;
}

void TimScene::setDiagramSettings(const TimDiagramSettings &newDiagramSettings) {
  m_diagramSettings = newDiagramSettings;
  settingChange();
  updateRangeSeparators();
  emit geometryChanged();
}

void TimScene::setDiagramSettingsByCmd(const TimDiagramSettings &newDiagramSettings)
{
  m_undoStack.push(new TimCmdChangeDiagramSettings(this, newDiagramSettings));
}

QGraphicsLinearLayout* TimScene::getLayout() {
  return m_layout;
}

void TimScene::setLabelWidth(unsigned int width) {
  long scene_width = getDiagramSettings()->getFullWidth();
  if (width > scene_width) {
    width = scene_width;
  }
  getDiagramSettings()->setCol0Width(width + 10);
  setSceneWidth(scene_width);
}

void TimScene::setSceneWidth(unsigned int width) {
  long w = (int) width - (int) getDiagramSettings()->getCol0Width();
  if (w >= 0) {
    getDiagramSettings()->setCol1Width(w);
  } else {
    getDiagramSettings()->setCol1Width(0);
  }
  updateRect();
}

QAction* TimScene::createRedoAction() {
  return m_undoStack.createRedoAction(this);
}

QAction* TimScene::createUndoAction() {
  return m_undoStack.createUndoAction(this);
}

void TimScene::pushCmd(QUndoCommand* cmd) {
  m_undoStack.push(cmd);
}

void TimScene::beginMacro(const QString& text) {
  m_undoStack.beginMacro(text);
}

void TimScene::endMacro() {
  m_undoStack.endMacro();
}

QAction* TimScene::getActionRemoveItems() {
  QAction *rmAct = new QAction(tr("Remove Item(s)"), this);
  rmAct->setEnabled(false);
  rmAct->setIcon(QIcon(":/images/rm.png"));
  rmAct->setStatusTip(tr("Removes one or more items(s) from the timing diagram"));
  connect(rmAct, SIGNAL(triggered()), this, SLOT(removeItems()));
  return rmAct;
}

void TimScene::removeItems() {

  bool del = false;
  QList<QGraphicsItem*> selected_items = selectedItems();
  if (selected_items.isEmpty()) {
    return;
  }
  for (QList<QGraphicsItem*>::Iterator it = selected_items.begin(); it != selected_items.end(); ++it) {
    QGraphicsItem *qi = *it;
    if (qi != NULL) {
      TimDeletable * tm = dynamic_cast<TimDeletable*>(qi);
      if (tm) {
        if (!del) {
          del = true;
          beginMacro("Delete Item(s)");
        }
        QUndoCommand *cmd = tm->createDeleteCmd();
        if (cmd != NULL) {
          pushCmd(cmd);
        }
      }
    }
  }
  if (del)
    endMacro();
}

void TimScene::updateRect(void) {
  int cnt = m_layout->count();
  QGraphicsLayoutItem *item;
  int index;
  int height = 0;
  for (index = cnt - 1; index >= 0; --index) {
    item = m_layout->itemAt(index);
    height += dynamic_cast<TimMemberBase*>(item)->boundingRect().height() + m_layout->spacing();
  }
  if (height < 50)
    height = 50;
  setSceneRect(0, 0, m_diagramSettings.getFullWidth() + m_layout->spacing(), height);
  updateRangeSeparators();
}

void TimScene::settingChange(void) {
  int cnt = m_layout->count();
  TimMember *item;
  int index;
  for (index = cnt - 1; index >= 0; --index) {
    item = dynamic_cast<TimMember*>(m_layout->itemAt(index));
    if (item!=nullptr) {
        item->timeRangeChange();
        item->setPreferredHeight(item->getHeight());
        item->setPreferredWidth(m_diagramSettings.getFullWidth());
      }
  }
  updateRect();
  invalidate();
}

void TimScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) {
  int cnt = m_layout->count();
  QGraphicsLayoutItem *item;
  int index;
  TimMemberBase* curMember;
  TimMemberBase* pointerVictim = nullptr;
  qreal time;

  for (index = cnt - 1; index >= 0; --index) {
    item = m_layout->itemAt(index);
    curMember = dynamic_cast<TimMemberBase*>(item);
    if (curMember->isUnderMouse())
      pointerVictim = curMember;
  }

  if (m_posLine != nullptr) {
    removeItem(m_posLine);
    m_posLine = nullptr;
  }

  if (pointerVictim) {
    QPointF ppos = pointerVictim->mapFromScene(mouseEvent->scenePos());
    int rIdx = m_diagramSettings.getIdx4AbsPos(ppos.x());
    if (rIdx>=0) {
        const Range<double> timeRange(m_diagramSettings.getTimeRange(rIdx));
        time = pointerVictim->calcSnapTime(ppos.x());
        qreal tpos = m_diagramSettings.getCol0Width()
                + m_diagramSettings.getXStart(rIdx) + (time-m_diagramSettings.getStartTime(rIdx)) * m_diagramSettings.getScaleFactor()
                - pointerVictim->mapFromScene(QPointF(0.0, 0.0)).x();

        if (timeRange.contains(time)) {
            QPen myPen = QPen(QColor(128, 0, 64, 255.0 * (1.0 - 0.7 * fabs(pointerVictim->getSnapDeviation()))), 2);
            m_posLine = addLine(QLineF(QPointF(tpos, pointerVictim->y()), QPointF(tpos, pointerVictim->y() + pointerVictim->boundingRect().height())), myPen);
        }
    }
  }
}

void TimScene::updateRangeSeparators(void) {
    for (auto rangeSeparator: m_rangeSeparators) removeItem(rangeSeparator);
    m_rangeSeparators.clear();
    TimMemberBase* firstElm = nullptr;

    if (m_layout->count()==0) return; // rest of function not needed (and would cause segfault when accessing itemAt(0))

    for (int index = m_layout->count() - 1; index >= 0; --index) {
      auto item = m_layout->itemAt(index);
      auto clkPtr = dynamic_cast<TimClockSignal*>(item);
      if (clkPtr != nullptr) clkPtr->calcEvents();
    }

    for (int i=0; i<m_diagramSettings.numRanges()-1; ++i) {
        firstElm = dynamic_cast<TimMemberBase*>(m_layout->itemAt(0));
        int xOffset = m_diagramSettings.getCol0Width() - firstElm->mapFromScene(QPointF(0.0, 0.0)).x();
        int xStart = xOffset+m_diagramSettings.getLayoutRange(i).getEnd()+2;
        int xEnd = xOffset+m_diagramSettings.getLayoutRange(i+1).getStart()-2;
        QPolygonF polygon;
        for (int j=0; j<10; ++j) {
            polygon << QPointF(xStart, 2*j*height()/20) << QPointF(xEnd, (2*j+1)*height()/20);
        }
        for (int j=9; j>=0; --j) { // TODO: hack to avoid line from last to first point
            polygon << QPointF(xEnd, (2*j+1)*height()/20) << QPointF(xStart, 2*j*height()/20);
        }
        m_rangeSeparators.push_back(addPolygon(polygon));
    }
}

void TimScene::createInsertTimMemberByTypeCmd(const QString & type, int index) {
  if (index == -1) {
    // if a member is selected, insert the new member just above it
    for (index=0; index<m_layout->count(); ++index) {
      if (dynamic_cast<TimMember*>(m_layout->itemAt(index))->isSelected())
        break;
    }
  }
  m_undoStack.push(new TimCmdInsertListItem(this, TimMemberBase::create(type, 0, this), index));
}

TimMemberBase* TimScene::insertTimMemberByType(const QString & type, int index, const QString & title) {
  TimMember *item = dynamic_cast<TimMember *>(TimMemberBase::create(type, 0, this));

  // in case creation failed
  if (!item) {
    return nullptr;
  }

  item->setText(title);

  // add item to scene and layout
  addItem(item);
  m_layout->insertItem(index, item);
  // redraw
  updateRect();
  return item;
}

void TimScene::removeTimMemberAt(int index) {
  if (index < 0)
    index = m_layout->count() - 1;
  TimMember * item = dynamic_cast<TimMember*>(m_layout->itemAt(index));
  m_layout->removeAt(index);
  m_layout->setMaximumSize(0, 0); // Adapted the size
  removeItem(item);
  updateRect(); // redraw
}

int TimScene::getIndexOf(TimMemberBase *item) {
  for (int idx = m_layout->count()-1; idx>=0; --idx) 
    if (item == dynamic_cast<TimMemberBase*>(m_layout->itemAt(idx))) return idx;
  return -1;
}

TimToolActions_t TimScene::createToolActions(QObject *parent) {
  return m_tools->createActions(parent);
}

void TimScene::clockSettingsDialog(TimClockSignal *clock) {
  emit callClockDialog(clock);
}

void TimScene::signalSettingsDialog(TimManualSignal *signal) {
  emit callSignalDialog(signal);
}

void TimScene::defaultBehaviorMaps() {
  if (!m_behaviorMaps)
    m_behaviorMaps = std::make_shared<TimBehaviorMapCollection>();
  // remove all old behavior maps
  m_behaviorMaps->clear();
  // create default behavior map(s)
  {
    TimBehaviorMap_p temp(new TimBehaviorMap("Binary"));
    temp->insertSignalLevel(20);
    temp->insertSignalLevel(80);
    temp->setValueChoiceMethod(TimBehaviorMap::toggle);

    TimVisualMap_p t1(new TimVisualMap("0", ActiveSignalLevels_t{0}));
    t1->setEdgeMapping(EdgeMapping_t{std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{1}, 0)});
    temp->insertVisualMap(t1);

    TimVisualMap_p t2(new TimVisualMap("1", ActiveSignalLevels_t{1}));
    t2->setEdgeMapping(EdgeMapping_t{std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{0}, 1)});
    temp->insertVisualMap(t2);

    m_behaviorMaps->registerBehaviorMap("Binary", temp);
  }

  {
    TimBehaviorMap_p temp(new TimBehaviorMap("Logic"));
    temp->insertSignalLevel(20);
    temp->insertSignalLevel(50);
    temp->insertSignalLevel(80);
    temp->setValueChoiceMethod(TimBehaviorMap::select);

    TimVisualMap_p t1(new TimVisualMap("0", ActiveSignalLevels_t{0}));
    EdgeMapping_t em1;
    em1.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{0}, 0));
    em1.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{1}, 0));
    em1.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{2}, 0));
    t1->setEdgeMapping(em1);
    temp->insertVisualMap(t1);

    TimVisualMap_p t2(new TimVisualMap("1", ActiveSignalLevels_t{2}));
    EdgeMapping_t em2;
    em2.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{0}, 2));
    em2.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{1}, 2));
    em2.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{2}, 2));
    t2->setEdgeMapping(em2);
    temp->insertVisualMap(t2);

    TimVisualMap_p t3(new TimVisualMap("X", ActiveSignalLevels_t{0, 2}));
    em1.insert(em1.end(), em2.begin(), em2.end());
    t3->setEdgeMapping(em1);
    temp->insertVisualMap(t3);

    TimVisualMap_p t4(new TimVisualMap("Z", ActiveSignalLevels_t{1}));
    EdgeMapping_t em4;
    em4.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{0}, 1));
    em4.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{1}, 1));
    em4.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{2}, 1));
    t4->setEdgeMapping(em4);
    temp->insertVisualMap(t4);

    m_behaviorMaps->registerBehaviorMap("Logic", temp);
  }

  {
    TimBehaviorMap_p temp(new TimBehaviorMap("Bus"));
    temp->insertSignalLevel(20);
    temp->insertSignalLevel(80);
    temp->setValueChoiceMethod(TimBehaviorMap::input);

    ActiveSignalLevels_t asl{0,1};

    TimVisualMap_p t1(new TimVisualMap(".*", asl));
    EdgeMapping_t em1;
    em1.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{0}, 1));
    em1.push_back(std::pair<ActiveSignalLevels_t, unsigned int>(ActiveSignalLevels_t{1}, 0));
    t1->setEdgeMapping(em1);
    t1->setValueVisible(true);

    temp->insertVisualMap(t1);

    m_behaviorMaps->registerBehaviorMap("Bus", temp);
  }

}

TimBehaviorMap_p TimScene::getBehaviorMapByName(const QString &name) const {
  return m_behaviorMaps->getBehaviorMapByName(name);
}

QStringList TimScene::getBehaviorMapSettingList() const {
  return m_behaviorMaps->getSettingList();
}

void TimScene::renameBehaviorMap(const QString &oldName, const QString &newName) {
  TimBehaviorMap_p map = m_behaviorMaps->deregisterBehaviorMap(oldName);
  map->setName(newName);
  m_behaviorMaps->registerBehaviorMap(newName, map);
}

void TimScene::delBehaviorMap(const QString &name) {
  m_behaviorMaps->deregisterBehaviorMap(name);
}

void TimScene::addBehaviorMap(const QString &name, TimBehaviorMap_p map) {
  m_behaviorMaps->registerBehaviorMap(name, map);
}

