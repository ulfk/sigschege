// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMINGSCENE_H_
#define TIMINGSCENE_H_

#include "TimDiagramSettings.h"
#include "TimToolCollection.h"
#include "TimBehaviorMapCollection.h"
#include <QtGui>
#include <QGraphicsScene>
#include <QGraphicsLinearLayout>
#include <QGraphicsLineItem>
#include <QGraphicsPolygonItem>
#include <QUndoStack>
#include <memory>
class TimMemberBase;
class TimClockSignal;
class TimManualSignal;

/** @brief The timing scene holds all element of the timing diagram.
 *
 * The timing scene holds all elements of the timing diagram. Beside this element, it stores
 * the layout data as well.
 */
class TimScene: public QGraphicsScene {
Q_OBJECT

public:
  /** @brief Ctor
   *
   * @param parent Pointer to the owning parent object.
   */
  TimScene(QObject *parent = 0);

  virtual ~TimScene();

  /** @brief Adds item to the timing diagram list.
   *
   * @param index Index of the new item. -1 will add at the end of the list.
   * @param item Pointer to the item which should be added at index
   */
  void insertTimListItem(int index, TimMemberBase * item);

  /** @brief Removes item from the timing scene
   *
   * This method removes the specified item from the timing scene. It will not destroy the item.
   *
   * @param item Pointer to the item to remove.
   * @returns Returns the old index.
   */
  int removeTimListItem(TimMemberBase * item);

  /** @brief Get a pointer to the layout data.
   *
   * This method returns a pointer to the layout data of this scene.
   *
   * @return Returns a pointer to the layout data of this scene.
   */
  TimDiagramSettings* getDiagramSettings(); // TODO: return as read-only, if at all

  void setDiagramSettings(const TimDiagramSettings &newDiagramSettings);

  void setDiagramSettingsByCmd(const TimDiagramSettings &newDiagramSettings);

  QGraphicsLinearLayout* getLayout();
  
  /** @brief Sets the label width.
   *
   * This method sets the label width. The width will be stored in the layout data.
   *
   * @param width Label width.
   */
  void setLabelWidth(unsigned int width);

  /** @brief Sets the scene width.
   *
   * This method sets the scene width. The width will be stored in the layout data.
   *
   * @param width Scene width.
   */
  void setSceneWidth(unsigned int width);

  /** @brief Creates a redo action
   *
   * This method creates a redo action that can be used in toolbars and menus.
   *
   * @return Redo action
   */
  QAction* createRedoAction();

  /** @brief Creates a undo action
   *
   * This method creates a undo action that can be used in toolbars and menus.
   *
   * @return Undo action
   */
  QAction* createUndoAction();

  /** @brief Push a command to the undo stack
   *
   * This method will execute and push the command to the undo stack.
   *
   * @param cmd Pointer to the command to execute and push to the undo stack.
   * @sa startMacro
   * @sa endMacro
   */
  void pushCmd(QUndoCommand* cmd);

  /** @brief Start a macro command sequence.
   *
   * This method starts a macro command sequence. After starting the macro, every command pushed
   * to the command stack will be combined as a single macro.
   *
   * @param text Name of the macro command sequence.
   * @sa pushCmd
   * @sa endMacro
   */
  void beginMacro(const QString& text);

  /** @brief Ends a macro command sequence.
   *
   * This method end a macro command sequence.
   * @sa pushCmd
   * @sa endMacro
   */
  void endMacro();

  /** @brief Return if the diagram is unmodified 
   *
   * This method returns true if the timing diagram has not been changed since being opened,
   * saved or created.
   *
   * @return modified flag
   */
  bool isClean (void) { return m_undoStack.isClean(); }
 
  /** @brief Reset the modification status of the diagram 
   *
   * This method allows to reset the modification status of this diagram manually, e.g. if some
   * modifications are done implicitly after creation, which shouldn't count as modification.
   *
   */
  void setClean(void) { m_undoStack.setClean(); }

  /** @brief Clears the diagram completely 
   *
   * This method the diagram completely and resets it to its starting state, directly after construction.
   * Note that this is not an undoable action of course.
   *
   */
  void clear(void);

  /** @brief Get a pointer to the signal manager.
   *
   * @return Pointer to the signal manager.
   */
  TimTool * getCurrentTool() {return m_tools->getCurrent();}

  /** @brief Creates a QAction object to remove item(s)
   *
   * @return Pointer to a @c QAction object.
   */
  QAction* getActionRemoveItems();

  void settingChange(void);

  /** @brief Creates and pushes a InsertTimMemberByName command.
   *
   * @param type TimMember type to insert
   * @param index Index position (-1 will add TimMember to the end)
   */
  void createInsertTimMemberByTypeCmd(const QString & type, int index = -1);

  /** @brief Insert a new TimMember
   *
   * This function will create and insert a new TimMember item to the scene at index position index. The type of TimMember is defined by type.
   *
   * @param type The Type of the new TimMember
   * @param index The index position (-1 will add to the end)
   * @param title Title of this member
   * @return Pointer to the created TimMember or NULL if creation failed
   */
  TimMemberBase* insertTimMemberByType(const QString & type, int index = -1, const QString & title = QString());

  /** @brief Remove the TimMember at index
   *
   * @param index Index of the TimMember to remove
   */
  void removeTimMemberAt(int index);

  int getIndexOf(TimMemberBase *item);

  TimToolActions_t createToolActions(QObject * parent);

  void clockSettingsDialog(TimClockSignal *clock);

  void signalSettingsDialog(TimManualSignal *signal);

  void defaultBehaviorMaps();

  void renameBehaviorMap(const QString & oldName, const QString & newName);

  TimBehaviorMap_p getBehaviorMapByName(const QString & name) const;

  QStringList getBehaviorMapSettingList() const;

  void delBehaviorMap(const QString & name);

  void addBehaviorMap(const QString & name, TimBehaviorMap_p map);

  /** @brief Set a new behavior map collection
   *
   * This function will replace the current behavior map collection with the one passed as argument.
   *
   * @param bmc The new behavior map collection
   */
  void setBehaviorMapCollection(std::shared_ptr<TimBehaviorMapCollection> bmc);

  /** @brief Get the behavior map collection
   *
   * This function will return a pointer to the current behavior map collection
   *
   */
   std::shared_ptr<TimBehaviorMapCollection> getBehaviorMapCollection();

  /** @brief Load a behavior map collection from the given file
   *
   * This function will load a behavior map collection from the given file to replace the currently active one.
   *
   * @param name The name of the file to load
   */
  void loadBehaviorMapCollection(const QString & name);

  /** @brief Save a behavior map collection to the given file
   *
   * This function will save the the currently active behavior map collection to the given file.
   * It creates a dedicated XML file containing only the Bmc.
   *
   * @param name The name of the file to save
   */

  void saveBehaviorMapCollection(const QString & name);

  /** @brief Write a behavior map collection to the given stream writer
   *
   * This function will write the the currently active behavior map collection to the given file.
   *
   * @param writer Open XML stream to write to
   */
  void writeBehaviorMapCollection(QXmlStreamWriter *writer);

  void updateRangeSeparators(void);

protected slots:
  void removeItems();

signals:
  void callClockDialog(TimClockSignal* clock);
  void callSignalDialog(TimManualSignal* signal);
  void geometryChanged();

protected:
  void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
  /** @brief Update size of the scene
   *
   */
  void updateRect(void);

  /** @brief Pointer to the linear layout object.
   *
   * This layout object is used to organise the signals in a vertical list.
   */
  QGraphicsLinearLayout *m_layout;

  /** @brief Layout data object
   *
   * This object stores all global timing diagram data.
   */
  TimDiagramSettings m_diagramSettings;

  QUndoStack m_undoStack;

  QGraphicsLineItem * m_posLine;

  std::vector<QGraphicsPolygonItem *> m_rangeSeparators;

  TimToolCollection * m_tools;

  std::shared_ptr<TimBehaviorMapCollection> m_behaviorMaps;
};

inline void TimScene::setBehaviorMapCollection(std::shared_ptr<TimBehaviorMapCollection> bmc) {
  m_behaviorMaps = bmc;
}

inline std::shared_ptr<TimBehaviorMapCollection> TimScene::getBehaviorMapCollection() {
  return m_behaviorMaps;
}

inline void TimScene::loadBehaviorMapCollection(const QString &name) {}

inline void TimScene::writeBehaviorMapCollection(QXmlStreamWriter *writer) {
  m_behaviorMaps->SSGWrite(writer);
}

#endif /* TIMINGSCENE_H_ */
