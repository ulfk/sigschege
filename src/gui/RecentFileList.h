// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef RECENTFILELIST_H
#define RECENTFILELIST_H

#include <deque>
#include <memory>
#include <QObject>

class QAction;
class QString;

class RecentFileList : public   QObject{
  Q_OBJECT

signals:
  void changed();
  void loadRecentFile(const QString &filePath);

public:
  RecentFileList();
  void push_back(const QString &filePath);
  unsigned int size() const;
  std::shared_ptr<QAction>& operator [](unsigned int i);
  std::shared_ptr<QAction> operator [](unsigned int i) const;
  inline std::deque<std::shared_ptr<QAction>>::iterator begin();
  inline std::deque<std::shared_ptr<QAction>>::iterator end();
  void setFiles(const QStringList &filePaths);
  QStringList getFiles();
  void setMaxFiles(unsigned int value);
  unsigned int getMaxFiles();

private:
  std::deque<std::shared_ptr<QAction>> recentFiles;
  std::deque<std::shared_ptr<QAction>> oldRecentFiles;
  unsigned int maxFiles;

  void _push_back(const QString &filePath);
  void _cleanUp();
  void _finishChange();
};

inline std::deque<std::shared_ptr<QAction>>::iterator RecentFileList::begin() {
  return recentFiles.begin();
}

inline std::deque<std::shared_ptr<QAction>>::iterator RecentFileList::end() {
  return recentFiles.end();
}

#endif // RECENTFILELIST_H
