// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "mainwindow.h"
#include "TimScene.h"
#include "TimSignal.h"
#include "DiaSettings.h"
#include "TimBehMapDialog.h"
#include "ClockSettings.h"
#include "SignalSettings.h"
#include "TimScale.h"
#include "SSGReader.h"
#include "SSGWriter.h"
#include "AppSettings.h"
#include <QStatusBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QSvgGenerator>
#include <QInputDialog>

MainWindow::MainWindow(const QString &ssgFile, QWidget *parent) : QMainWindow(parent), m_recentFiles(), m_keepViewToWidth(true) {

  setWindowTitle(tr("Sigschege"));

  createTopView();

  createActions();
  createMenus();
  createToolBars();
  createStatusBar();

  connect(&m_recentFiles, SIGNAL(changed()), this, SLOT(updateRecentFiles()));
  // handle changed selection
  connect(m_scene, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

  QSettings settings;

  QVariant setting = settings.value("Geometry");
  if (setting == QVariant()) {
    resize(800, 600);
  } else {
    setGeometry(setting.toRect());
  }
  bool ok;
  m_recentFiles.setMaxFiles(settings.value("MaxRecentFiles").toUInt(&ok));
  if (!ok)
    m_recentFiles.setMaxFiles(15);
  connect(&m_recentFiles, SIGNAL(loadRecentFile(const QString &)), this, SLOT(loadRecentFile(const QString &)));
  m_recentFiles.setFiles(settings.value("RecentFileList").toStringList());

  viewToWidth();

  if (ssgFile.size()>0) {
    loadFile(ssgFile);
  }
}

MainWindow::~MainWindow() {}

void MainWindow::createActions() {
  m_newAct = new QAction(tr("&New"), this);
  m_newAct->setShortcut(tr("Ctrl+N"));
  m_newAct->setStatusTip(tr("Start a new timing diagram"));
  connect(m_newAct, SIGNAL(triggered()), this, SLOT(cmdNew()));

  m_openAct = new QAction(tr("&Open"), this);
  m_openAct->setShortcut(tr("Ctrl+L"));
  m_openAct->setStatusTip(tr("Open a timing diagram"));
  connect(m_openAct, SIGNAL(triggered()), this, SLOT(cmdOpen()));

  m_saveAct = new QAction(tr("&Save"), this);
  m_saveAct->setShortcut(tr("Ctrl+S"));
  m_saveAct->setStatusTip(tr("Save this timing diagram"));
  connect(m_saveAct, SIGNAL(triggered()), this, SLOT(cmdSave()));

  m_saveAsAct = new QAction(tr("Save &As"), this);
  m_saveAsAct->setStatusTip(tr("Save the timing diagram under a new name"));
  connect(m_saveAsAct, SIGNAL(triggered()), this, SLOT(cmdSaveAs()));

  m_exportAct = new QAction(tr("&Export"), this);
  m_exportAct->setStatusTip(tr("Export the timing diagram as SVG"));
  connect(m_exportAct, SIGNAL(triggered()), this, SLOT(cmdExport()));

  m_loadBmcAct = new QAction(tr("Load Behaviormap Collection"), this);
  m_loadBmcAct->setStatusTip(tr("Load Behaviormap Collection into current timing diagram"));
  connect(m_loadBmcAct, SIGNAL(triggered()), this, SLOT(cmdLoadBmc()));

  m_saveBmcAct = new QAction(tr("Save Behaviormap Collection"), this);
  m_saveBmcAct->setStatusTip(tr("Save Behaviormap Collection from current timing diagram"));
  connect(m_saveBmcAct, SIGNAL(triggered()), this, SLOT(cmdSaveBmc()));

  m_exitAct = new QAction(tr("E&xit"), this);
  m_exitAct->setShortcut(tr("Ctrl+Q"));
  m_exitAct->setStatusTip(tr("Exit the application"));
  connect(m_exitAct, SIGNAL(triggered()), this, SLOT(close()));

  m_addSignalAct = new QAction(tr("Add Signal"), this);
  m_addSignalAct->setIcon(QIcon(":/images/addSig.png"));
  m_addSignalAct->setStatusTip(tr("Adds a new signal to the timing diagram"));
  connect(m_addSignalAct, SIGNAL(triggered()), this, SLOT(cmdAddSignal()));

  m_addBusAct = new QAction(tr("Add Bus"), this);
  m_addBusAct->setIcon(QIcon(":/images/addBus.png"));
  m_addBusAct->setStatusTip(tr("Adds a new bus to the timing diagram"));
  connect(m_addBusAct, SIGNAL(triggered()), this, SLOT(cmdAddBus()));

  m_addClockAct = new QAction(tr("Add Clock"), this);
  m_addClockAct->setIcon(QIcon(":/images/addClk.png"));
  m_addClockAct->setStatusTip(tr("Adds a new clock to the timing diagram"));
  connect(m_addClockAct, SIGNAL(triggered()), this, SLOT(cmdAddClock()));

  m_addScaleAct = new QAction(tr("Add Time Scale"), this);
  m_addScaleAct->setIcon(QIcon(":/images/addScale.png"));
  m_addScaleAct->setStatusTip(tr("Adds a new time scale to the timing diagram"));
  connect(m_addScaleAct, SIGNAL(triggered()), this, SLOT(cmdAddScale()));

  m_addSeparator = new QAction(tr("Add Separator"), this);
  m_addSeparator->setIcon(QIcon(":/images/addSeparator.png"));
  m_addSeparator->setStatusTip(tr("Adds a new separator to the timing diagram"));
  connect(m_addSeparator, SIGNAL(triggered()), this, SLOT(cmdAddSeparator()));

  m_setDiaProps = new QAction(tr("Edit Diagram Properties"), this);
  //m_addSignalAct->setIcon(QIcon(":/images/add.png"));
  m_setDiaProps->setStatusTip(tr("Edit the global properties of this timing diagram"));
  connect(m_setDiaProps, SIGNAL(triggered()), this, SLOT(cmdEditDiaProperties()));

  m_appSettingsAct = new QAction(tr("Configure Sigschege Settings"), this);
  m_appSettingsAct->setStatusTip(tr("Edit the general Sigschege Settings"));
  connect(m_appSettingsAct, SIGNAL(triggered()), this, SLOT(cmdEditAppSettings()));

  m_editBehaviorMaps = new QAction(tr("Edit Behavior Maps"), this);
  m_editBehaviorMaps->setStatusTip(tr("Create, Delete, Modify Behavior Maps"));
  connect(m_editBehaviorMaps, SIGNAL(triggered()), this, SLOT(cmdEditBehaviorMaps()));

  m_rmSignalAct = m_scene->getActionRemoveItems();

  m_aboutAct = new QAction(tr("&About"), this);
  m_aboutAct->setStatusTip(tr("Show a short description of this software"));
  connect(m_aboutAct, SIGNAL(triggered()), this, SLOT(about()));

  connect(m_scene, SIGNAL(callClockDialog(TimClockSignal*)), this, SLOT(cmdEditClockProperties(TimClockSignal*)));
  connect(m_scene, SIGNAL(callSignalDialog(TimManualSignal*)), this, SLOT(cmdEditSignalProperties(TimManualSignal*)));
  connect(m_scene, SIGNAL(geometryChanged()), this, SLOT(sceneGeometryChanged()));

  // create Tool Actions and add them to one action group
  m_SigGroup = new QActionGroup(this);
  m_toolActions = m_scene->createToolActions(this);
  for(TimToolActions_t::iterator it = m_toolActions.begin(); it != m_toolActions.end(); ++ it)
  {
    m_SigGroup->addAction(*it);
  }
  m_undoCmd = m_scene->createUndoAction();
  m_undoCmd->setIcon(QIcon(":/images/undo.png"));
  m_redoCmd = m_scene->createRedoAction();
  m_redoCmd->setIcon(QIcon(":/images/redo.png"));

  sceneScaleCombo = new QComboBox;
  QStringList scales;
  scales << "Width" << tr("25%") << tr("50%") << tr("75%") << tr("100%") << tr("125%") << tr("150%")<< tr("200%") << tr("300%");
  sceneScaleCombo->addItems(scales);
  sceneScaleCombo->setCurrentIndex(0);
  connect(sceneScaleCombo, SIGNAL(currentIndexChanged(const QString &)),
	  this, SLOT(sceneScaleChanged(const QString &)));
}

void MainWindow::createMenus() {

  // Create and init file menu
  m_fileMenu = menuBar()->addMenu(tr("&File"));
  m_fileMenu->addAction(m_newAct);
  m_fileMenu->addAction(m_openAct);
  m_recentFilesMenu = m_fileMenu->addMenu(tr("&Recent Files"));
  m_recentFilesMenu->setToolTipsVisible(true);
  m_fileMenu->addAction(m_saveAct);
  m_fileMenu->addAction(m_saveAsAct);
  m_fileMenu->addAction(m_exportAct);
  m_fileMenu->addSeparator();
  m_fileMenu->addAction(m_loadBmcAct);
  m_fileMenu->addAction(m_saveBmcAct);
  m_fileMenu->addSeparator();
  m_fileMenu->addAction(m_exitAct);

  // Create and init edit menu
  m_editMenu = menuBar()->addMenu(tr("&Edit"));
  m_editMenu->addAction(m_undoCmd);
  m_editMenu->addAction(m_redoCmd);
  m_editMenu->addSeparator();
  m_editMenu->addAction(m_addSignalAct);
  m_editMenu->addAction(m_addClockAct);
  m_editMenu->addAction(m_addScaleAct);
  m_editMenu->addAction(m_addSeparator);
  m_editMenu->addAction(m_rmSignalAct);
  m_editMenu->addSeparator();
  m_editMenu->addAction(m_setDiaProps);
  m_editMenu->addAction(m_editBehaviorMaps);
  m_editMenu->addSeparator();
  m_editMenu->addAction(m_appSettingsAct);

  m_helpMenu = menuBar()->addMenu(tr("&Help"));
  m_helpMenu->addAction(m_aboutAct);

}

void MainWindow::createToolBars() {
  // Create and init the edit tool bar
  m_editToolBar = addToolBar(tr("Edit"));
  m_editToolBar->addAction(m_undoCmd);
  m_editToolBar->addAction(m_redoCmd);
  m_editToolBar->addSeparator();
  m_editToolBar->addAction(m_addSignalAct);
  m_editToolBar->addAction(m_addBusAct);
  m_editToolBar->addAction(m_addClockAct);
  m_editToolBar->addAction(m_addScaleAct);
  m_editToolBar->addAction(m_addSeparator);
  m_editToolBar->addAction(m_rmSignalAct);
  m_editToolBar->addWidget(sceneScaleCombo);

  // Create and init the signal tool bar
  m_signalToolBar = new QToolBar(tr("Signal"), this);
  addToolBar(Qt::LeftToolBarArea, m_signalToolBar);

  // Add Tool Actions to tool bar
  for(TimToolActions_t::iterator it = m_toolActions.begin(); it != m_toolActions.end(); ++ it)
  {
    m_signalToolBar->addAction(*it);
  }
}

void MainWindow::createStatusBar() {
  statusBar()->showMessage(tr("Ready"));
}

void MainWindow::createTopView() {
  m_scene = new TimScene(this);

  m_scene->setLabelWidth(50);
  m_scene->setSceneWidth(1000);

  m_view = new QGraphicsView(m_scene);
  m_view->setAlignment(Qt::AlignLeft | Qt::AlignTop);
  setCentralWidget(m_view);
}

void MainWindow::cmdNew() {
  maybeSave();
}

bool MainWindow::loadFile(const QString &fileName) {
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("QXmlStream Sigschege Timing Diagram"), tr("Cannot read file %1:\n%2.") .arg(fileName) .arg(file.errorString()));
    return false;
  }

  SSGReader reader(m_scene);
  if (!reader.read(&file)) {
    QMessageBox::warning(this, tr("QXmlStream Sigschege Timing Diagram"), tr("Parse error in file %1 at line %2, column %3:\n%4") .arg(fileName) .arg(reader.lineNumber()) .arg(reader.columnNumber()) .arg(reader.errorString()));
    return false;
  }

  statusBar()->showMessage(tr("File loaded"), 2000);
  m_filename = fileName;
  m_recentFiles.push_back(fileName);
  m_scene->settingChange();
  return true;
}

bool MainWindow::loadBmcFile(const QString &fileName) {
  QFile file(fileName);
  if (!file.open(QFile::ReadOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("QXmlStream Sigschege Behavior Map Collection"), tr("Cannot read file %1:\n%2.") .arg(fileName) .arg(file.errorString()));
    return false;
  }

  SSGReader reader(m_scene);
  if (!reader.readBmcFile(&file)) {
    QMessageBox::warning(this, tr("QXmlStream Behavior Map Collection"), tr("Parse error in file %1 at line %2, column %3:\n%4") .arg(fileName) .arg(reader.lineNumber()) .arg(reader.columnNumber()) .arg(reader.errorString()));
    return false;
  }

  statusBar()->showMessage(tr("Behavior Map Collection loaded"), 2000);
  m_scene->settingChange();
  return true;
}

void MainWindow::cmdOpen() {
  if (!maybeSave()) {
    return;
  }

  QString fileName =
    QFileDialog::getOpenFileName(this, tr("Open Timing Diagram"), QDir::currentPath(), tr("Sigschege Timing Diagrams (*.ssg)"));
  if (fileName.isEmpty())
    return;

  loadFile(fileName);
}

void MainWindow::loadRecentFile(const QString& filePath) {
  QFileInfo fInfo(filePath);
  if (!fInfo.exists() || !fInfo.isReadable()) {
      QMessageBox::warning(this, tr("Opening Recent File"),
                           tr("Cannot access file %1")
                           .arg(filePath));
      return;
    }
  if (!maybeSave())
    return;
  loadFile(filePath);
}

QString MainWindow::askForFileName(void) {
  return QFileDialog::getSaveFileName(this, tr("Save Diagram As"),
				 QDir::currentPath(),
				 tr("Sigschege Files (*.ssg)"));
}

bool MainWindow::save(QString &fileName) {
  QFile file(fileName);
  bool success;
  if (!file.open(QFile::WriteOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("QXmlStream Bookmarks"),
			 tr("Cannot write file %1:\n%2.")
			 .arg(fileName)
			 .arg(file.errorString()));
    return false;
  }

  SSGWriter writer(m_scene);
  success = writer.write(&file);
  if (success) {
      m_recentFiles.push_back(fileName);
      m_scene->setClean();
    }
  return success;
}

void MainWindow::cmdSave() {
  if (m_filename.isEmpty())
    m_filename =  askForFileName();
  save(m_filename);
}

void MainWindow::cmdSaveAs() {
  QString fileName = askForFileName();    
  if (fileName.isEmpty())
    return;
  
  save(fileName);
}

void MainWindow::cmdExport() {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Export Diagram To"),
						  QDir::currentPath(),
						  tr("Scalable Vector Graphics (*.svg)"));
    
  if (fileName.isEmpty())
    return;
  
  QSvgGenerator *gen = new QSvgGenerator();
  gen->setFileName(fileName);
  gen->setTitle(tr("Timing Diagram generated by Sigschege"));
  QRectF srect = m_scene->itemsBoundingRect();
  QSizeF ssize = srect.size();
  gen->setResolution(72);
  gen->setViewBox(srect);
  gen->setSize(QSize(ssize.width(),ssize.height()));
  QPainter *svgPainter = new QPainter(gen);
  m_scene->render(svgPainter);
  svgPainter->end();
  delete svgPainter;
  delete gen;
}

void MainWindow::cmdLoadBmc() {
  QString fileName =
    QFileDialog::getOpenFileName(this, tr("Load Behaviormap Collection"), QDir::currentPath(), tr("Sigschege Behaviormap Collections (*.bmc)"));
  if (fileName.isEmpty())
    return;

  loadBmcFile(fileName);
}

void MainWindow::cmdSaveBmc() {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save Behaviormap Collection"),
                                                  QDir::currentPath(),
                                                  tr("Sigschege Behaviormap Collections (*.bmc)"));

  if (fileName.isEmpty())
    return;

  QFile file(fileName);
  if (!file.open(QFile::WriteOnly | QFile::Text)) {
    QMessageBox::warning(this, tr("QXmlStream Bookmarks"),
                         tr("Cannot write file %1:\n%2.")
                         .arg(fileName)
                         .arg(file.errorString()));
    return;
  }
  SSGWriter writer(m_scene);
  writer.writeBmc(&file);
}

void MainWindow::closeEvent(QCloseEvent *event) {
  if (maybeSave()) {
    QSettings settings;
    settings.setValue("Geometry", geometry());
    settings.setValue("RecentFileList", m_recentFiles.getFiles());
    event->accept();
  } else {
    event->ignore();
  }
}

void MainWindow::cmdAddSeparator() {
  m_scene->createInsertTimMemberByTypeCmd("separator", -1);
}

void MainWindow::cmdAddSignal() {
  m_scene->createInsertTimMemberByTypeCmd("signal", -1);
}

void MainWindow::cmdAddBus() {
  m_scene->createInsertTimMemberByTypeCmd("bus", -1);
}

void MainWindow::cmdAddClock() {
  m_scene->createInsertTimMemberByTypeCmd("clock", -1);
}

void MainWindow::cmdAddScale() {
  m_scene->createInsertTimMemberByTypeCmd("scale", -1);
}

void MainWindow::selectionChanged() {
  // get new selected items
  QList<QGraphicsItem*> item_list = m_scene->selectedItems();

  // check if any item is selected
  if (item_list.isEmpty()) {
    // disable delete item
    m_rmSignalAct->setEnabled(false);
  } else {
    // enable delete item
    m_rmSignalAct->setEnabled(true);
  }
}

void MainWindow::updateRecentFiles() {
  m_recentFilesMenu->clear();
  for (auto action: m_recentFiles) {
      m_recentFilesMenu->addAction(action.get());
    }
}

void MainWindow::about()
{
  QString appString =  tr("<b>Sigschege \<development-version\></b>") ;
  QMessageBox::about(this,
                     tr("About Sigschege"),
                     appString + tr("<p>"
                     "<p>A software for creating timing diagrams for digital electrical circuits and bus protocols."
                     "<p><p>Copyright by Ingo Hinrichs, Ulf Klaperski"
                     "<p>Distributed under the terms of of the GNU General Public License, version 3."
                     ));
}

bool MainWindow::maybeSave() {
  bool affirmative = true;
  if (!m_scene->isClean()) {

    QMessageBox::StandardButton user_choice;
    user_choice
        = QMessageBox::warning(this, tr("Application"), tr("The timing diagram is modified.\n"
          "Do you want to save the changes?"), QMessageBox::Save
            | QMessageBox::Discard | QMessageBox::Cancel);

    if (user_choice == QMessageBox::Save) {

      if (m_filename.isEmpty()) {
        m_filename =  askForFileName();
      }

      affirmative = save(m_filename);
    } else if (user_choice == QMessageBox::Cancel) {
      affirmative = false;
    }
  }

  if (affirmative) {
    m_scene->clear();
    return true;
  } else
    return false;
}

void MainWindow::cmdEditDiaProperties() {
  DiaSettingDialog setting_dialog(this, m_scene);
  setting_dialog.exec();
}

void MainWindow::cmdEditAppSettings() {
  unsigned int maxRecentFiles = m_recentFiles.getMaxFiles();
  AppSettings dialog(maxRecentFiles);
  dialog.exec();
  m_recentFiles.setMaxFiles(maxRecentFiles);
  QSettings settings;
  settings.setValue("MaxRecentFiles", m_recentFiles.getMaxFiles());
}

void MainWindow::cmdEditBehaviorMaps() {
  TimBehMapDialog behmaps(this, m_scene);
  behmaps.exec();
  m_scene->update();
}

void MainWindow::cmdEditClockProperties(TimClockSignal* clock) {
  ClockSettings setting_dialog(this, m_scene, clock);
  setting_dialog.exec();
}

void MainWindow::cmdEditSignalProperties(TimManualSignal* signal) {
  SignalSettings setting_dialog(this, m_scene, signal);
  setting_dialog.exec();
}

void MainWindow::sceneScaleChanged(const QString &scale) {
  if (scale == "Width") {
    m_keepViewToWidth = true;
    viewToWidth();
  } else {
    m_keepViewToWidth = false;
    double newScale = scale.left(scale.indexOf(tr("%"))).toDouble() / 100.0;
    QTransform oldTransform = m_view->transform();
    m_view->resetTransform();
    m_view->translate(oldTransform.dx(), oldTransform.dy());
    m_view->scale(newScale, newScale);
    }
}

void MainWindow::sceneGeometryChanged()
{
  if (m_keepViewToWidth) viewToWidth();
}

void MainWindow::viewToWidth(void) {
  double sceneWidth = m_scene->width();
  double viewWidth = m_view->size().width();
  double newScale = 0.99 * viewWidth / sceneWidth;
  QTransform oldTransform = m_view->transform();
  m_view->resetTransform();
  m_view->translate(oldTransform.dx(), oldTransform.dy());
  m_view->scale(newScale, newScale);
}

void MainWindow::resizeEvent ( QResizeEvent * event ) {
  QMainWindow::resizeEvent(event);
  if (m_keepViewToWidth) viewToWidth();
}

