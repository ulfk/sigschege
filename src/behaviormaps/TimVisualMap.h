// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMVISUALMAP_H_
#define TIMVISUALMAP_H_

#include <QRegExp>
#include <QString>
#include <QRectF>
#include <QXmlStreamWriter>
#include <vector>
#include <map>
#include <memory>
class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;
class TimBehaviorMap;

typedef std::vector<unsigned int> ActiveSignalLevels_t;
typedef std::vector<std::pair<ActiveSignalLevels_t, unsigned int>> EdgeMapping_t;


class TimVisualMap {
public:
  TimVisualMap();
  TimVisualMap(const QString &pattern, ActiveSignalLevels_t activeSignalLevels = ActiveSignalLevels_t(), const TimBehaviorMap *map = 0);
  TimVisualMap(const TimVisualMap &rhs);
  ~TimVisualMap();

  TimVisualMap & operator=(const TimVisualMap &rhs);
  bool operator==(const QString &rhs) const;
  void setBehaviorMap(const TimBehaviorMap *map);
  bool match(const QString &value) const;
  void setPattern(const QString &pattern);
  void setPattern(const QRegExp &pattern);
  QString getPattern() const;

  ActiveSignalLevels_t getActiveSignalLevels() const;
  void setActiveSignalLevels(const ActiveSignalLevels_t &activeSignalLevels);
  bool insertSignalLevel(int level, int idx=-1);
  unsigned int getSignalLevelCount() const;

  EdgeMapping_t getEdgeMapping() const;
  void setEdgeMapping(const EdgeMapping_t &em);

  unsigned int getValuePos() const;
  void setValuePos(unsigned int valuePos);

  bool isValueVisible() const;
  void setValueVisible(bool valueVisible);

  void SSGWrite(QXmlStreamWriter *writer);

private:
    QRegExp m_regexp;
    const TimBehaviorMap *m_behaviorMap;
protected:
    ActiveSignalLevels_t m_activeSignalLevels;
    EdgeMapping_t m_edgeMapping;
    unsigned int m_valuePos;
    bool m_valueVisible;
};

#include <QSharedPointer>

typedef std::shared_ptr<TimVisualMap> TimVisualMap_p;
typedef std::vector<TimVisualMap_p> TimVisualMaps_t;

#endif /* TIMVISUALMAP_H_ */
