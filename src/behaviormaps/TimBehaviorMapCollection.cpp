// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimBehaviorMapCollection.h"
#include <QSharedPointer>
#include <QFile>
#include <QMessageBox>

TimBehaviorMapCollection::TimBehaviorMapCollection() : m_default_map(new TimBehaviorMap()) {}

TimBehaviorMapCollection::~TimBehaviorMapCollection() {}

void TimBehaviorMapCollection::clear() {
  m_behavior_maps.clear();
}

void TimBehaviorMapCollection::registerBehaviorMap(const QString &name, TimBehaviorMap_p behavior_map) {
  m_behavior_maps[name] = behavior_map;
}

void TimBehaviorMapCollection::registerBehaviorMap(const QString &name, TimBehaviorMap * behavior_map) {
  m_behavior_maps[name] = TimBehaviorMap_p(behavior_map);
}

TimBehaviorMap_p TimBehaviorMapCollection::deregisterBehaviorMap(const QString &name) {
  TimBehaviorMaps_t::iterator it = m_behavior_maps.find(name);
  if (it == m_behavior_maps.end())
    return TimBehaviorMap_p();
  TimBehaviorMap_p behmap = (*it).second;
  m_behavior_maps.erase(it);
  return behmap;
}

TimBehaviorMap_p TimBehaviorMapCollection::getBehaviorMapByName(const QString &name) const {
  TimBehaviorMaps_t::const_iterator it = m_behavior_maps.find(name);
  if (it == m_behavior_maps.end())
    return m_default_map;
  return (*it).second;
}

QStringList TimBehaviorMapCollection::getSettingList() const {
  QStringList ret;
  for (TimBehaviorMaps_t::const_iterator it = m_behavior_maps.begin(); it != m_behavior_maps.end(); ++it) {
    ret.append((*it).first);
  }
  return ret;
}

void TimBehaviorMapCollection::SSGWrite(QXmlStreamWriter *writer) {
  writer->writeStartElement("behaviormapcollection");
  for (auto const& [name, behaviorMap] : m_behavior_maps) {
      behaviorMap->SSGWrite(writer);
    }
  writer->writeEndElement();
}


