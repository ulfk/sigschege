// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMBEHAVIORMAPCOLLECTION_H_
#define TIMBEHAVIORMAPCOLLECTION_H_

#include "TimBehaviorMap.h"
#include <QStringList>
#include <QXmlStreamWriter>

class TimBehaviorMapCollection : public QObject {
  Q_OBJECT

public:
  TimBehaviorMapCollection();
  ~TimBehaviorMapCollection();

  void clear();

  void registerBehaviorMap(const QString &name, TimBehaviorMap *behavior_map);
  void registerBehaviorMap(const QString &name, TimBehaviorMap_p behavior_map);
  TimBehaviorMap_p deregisterBehaviorMap(const QString &name);
  TimBehaviorMap_p getBehaviorMapByName(const QString &name) const;
  QStringList getSettingList() const;
  void SSGWrite(QXmlStreamWriter *writer);

private:
  TimBehaviorMaps_t m_behavior_maps;
  TimBehaviorMap_p m_default_map;
};

#endif /* TIMBEHAVIORMAPCOLLECTION_H_ */
