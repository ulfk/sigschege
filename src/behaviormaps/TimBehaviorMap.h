// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMBEHAVIORMAP_H_
#define TIMBEHAVIORMAP_H_

#include "TimVisualMap.h"
#include <QString>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QXmlStreamWriter>
#include <vector>

typedef std::vector<unsigned int > TimSignalLevels_t;

class TimBehaviorMap {
public:

  typedef struct {
    double setup;
    double event;
    double hold;
    double end;
  } Ev_t;

  typedef enum {
    toggle,
    select,
    input
  } ValueChoiceMethod;

  TimBehaviorMap();
  TimBehaviorMap(const QString &name);
  TimBehaviorMap(const TimBehaviorMap &rhs);
  TimBehaviorMap & operator=(const TimBehaviorMap &rhs);
  virtual ~TimBehaviorMap();

  void setName(const QString &name);
  const QString& getName() const;

  bool operator==(const QString &rhs) const;
  bool operator==(const TimBehaviorMap &rhs) const;
  bool operator<(const TimBehaviorMap &rhs) const;

  void insertVisualMap(TimVisualMap_p visual_map, int index = -1);
  void rmVisualMap(int index);
  void swapVisualMap(int dst, int src);
  TimVisualMap_p getVisualMap(const QString &value) const;
  TimVisualMap_p getVisualMap(int index) const;

  void insertSignalLevel(unsigned int level, int index = -1);
  void updateSignalLevel(unsigned int level, int index);
  unsigned int getSignalLevel(int index) const;
  void rmSignalLevel(int index);
  unsigned int getSignalLevelCount() const;

  inline void setValueChoiceMethod(ValueChoiceMethod v);
  inline ValueChoiceMethod getValueChoiceMethod() const;

  QStringList getVisualMapPatterns() const;

  virtual QString getNextValue(const QString &cur_value) const;

  void SSGWrite(QXmlStreamWriter *writer);

private:
  QString m_name;
  TimSignalLevels_t m_signalLevels;
  TimVisualMaps_t m_visualMaps;
  TimVisualMap_p  m_defaultMap;
  ValueChoiceMethod m_valueChoiceMethod;
};

#include <map>

typedef std::shared_ptr<TimBehaviorMap> TimBehaviorMap_p;
typedef std::map<QString, TimBehaviorMap_p> TimBehaviorMaps_t;

void TimBehaviorMap::setValueChoiceMethod(TimBehaviorMap::ValueChoiceMethod v) {
  m_valueChoiceMethod = v;
}

TimBehaviorMap::ValueChoiceMethod TimBehaviorMap::getValueChoiceMethod() const {
  return m_valueChoiceMethod;
}

#endif /* TIMBEHAVIORMAP_H_ */
