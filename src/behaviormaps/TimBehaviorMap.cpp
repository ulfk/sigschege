// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimBehaviorMap.h"
#include "TimVisualMap.h"
#include <QDebug>

TimBehaviorMap::TimBehaviorMap()
  : m_defaultMap(new TimVisualMap()), m_valueChoiceMethod(ValueChoiceMethod::input)
{
  m_defaultMap->setBehaviorMap(this);
}

TimBehaviorMap::TimBehaviorMap(const QString &name)
  : m_name(name), m_defaultMap(new TimVisualMap()), m_valueChoiceMethod(ValueChoiceMethod::input)
{
  m_defaultMap->setBehaviorMap(this);
}

TimBehaviorMap::TimBehaviorMap(const TimBehaviorMap &rhs) {
  *this = rhs;
}

TimBehaviorMap& TimBehaviorMap::operator=(const TimBehaviorMap &rhs) {
  m_name = rhs.m_name;
  // deep copy signal levels (default implementation works for now)
  m_signalLevels = rhs.m_signalLevels;
  // deep copy visual maps
  for (TimVisualMaps_t::const_iterator it = rhs.m_visualMaps.begin(); it != rhs.m_visualMaps.end(); ++it) {
    m_visualMaps.push_back(std::make_shared<TimVisualMap>(*(*it)));
  }
  // deep copy default visual map
  *m_defaultMap = *(rhs.m_defaultMap);
  m_valueChoiceMethod = rhs.m_valueChoiceMethod;
  return *this;
}

TimBehaviorMap::~TimBehaviorMap() {}

void TimBehaviorMap::setName(const QString &name) {
  m_name = name;
}

const QString & TimBehaviorMap::getName() const {
  return m_name;
}

bool TimBehaviorMap::operator==(const QString &rhs) const {
  return m_name == rhs;
}

bool TimBehaviorMap::operator==(const TimBehaviorMap &rhs) const {
  return m_name == rhs.m_name;
}

void TimBehaviorMap::insertVisualMap(TimVisualMap_p visual_map, int index) {
  visual_map->setBehaviorMap(this);
  if (index < 0)
    m_visualMaps.push_back(visual_map);
  else
    m_visualMaps.insert(m_visualMaps.begin()+index, visual_map);
}

void TimBehaviorMap::rmVisualMap(int index) {
  m_visualMaps.erase(m_visualMaps.begin()+index);
}

void TimBehaviorMap::swapVisualMap(int dst, int src) {
  TimVisualMap_p temp = m_visualMaps[dst];
  m_visualMaps[dst] = m_visualMaps.at(src);
  m_visualMaps[src] = temp;
}

TimVisualMap_p TimBehaviorMap::getVisualMap(const QString &value) const {
  // try to find a matching visual map and return a pointer to it.
  TimVisualMaps_t::const_iterator it;
  for (it = m_visualMaps.begin(); it != m_visualMaps.end(); ++it) {
    if ((*it)->match(value)) {
      return (*it);
    }
  }
  // return the default visual map if no matching map was found
  return m_defaultMap;
}

TimVisualMap_p TimBehaviorMap::getVisualMap(int index) const {
  return *(m_visualMaps.begin()+index);
}

void TimBehaviorMap::insertSignalLevel(unsigned int level, int index) {
  if (index < 0)
    m_signalLevels.push_back(level);
  else
    m_signalLevels.insert(m_signalLevels.begin()+index, level);

}

void TimBehaviorMap::updateSignalLevel(unsigned int level, int index) {
  m_signalLevels[index] = level;
}

unsigned int TimBehaviorMap::getSignalLevel(int index) const {
  if (index >= m_signalLevels.size()) {
    qDebug() << "Warning requested illegal signal level with index " << index << ". Max legal signal level index is " << m_signalLevels.size()-1 << ".";
    return 0;
  }
  return m_signalLevels.at(index);
}

void TimBehaviorMap::rmSignalLevel(int index) {
  m_signalLevels.erase(m_signalLevels.begin()+index);
}

unsigned int TimBehaviorMap::getSignalLevelCount() const {
  return m_signalLevels.size();
}

QString TimBehaviorMap::getNextValue(const QString &cur_value) const {
  if (m_visualMaps.empty())
    return QString();
  auto currentIdx = std::find_if(begin(m_visualMaps), end(m_visualMaps),
                                 [&cur_value](std::shared_ptr<TimVisualMap> vm) -> bool {return vm->getPattern() == cur_value;});
  if (currentIdx!=m_visualMaps.end())
    ++currentIdx;
  if (currentIdx==m_visualMaps.end())
    currentIdx = m_visualMaps.begin();
  return (*currentIdx)->getPattern();
}

void TimBehaviorMap::SSGWrite(QXmlStreamWriter *writer) {
  writer->writeStartElement("behaviormap");

  writer->writeStartElement("name");
  writer->writeCharacters(m_name);
  writer->writeEndElement();

  for (auto const& visualMap : m_visualMaps) {
      visualMap->SSGWrite(writer);
    }
  writer->writeEndElement();
}

QStringList TimBehaviorMap::getVisualMapPatterns() const {
  QStringList patterns;
  for (auto visualMapPtr : m_visualMaps) {
      patterns.append(visualMapPtr->getPattern());
    }
  return std::move(patterns);
}
