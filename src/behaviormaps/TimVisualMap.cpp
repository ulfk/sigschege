// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimVisualMap.h"
#include "TimBehaviorMap.h"
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QWidget>
#include <QDebug>
#include <algorithm>

TimVisualMap::TimVisualMap()
  : m_behaviorMap(0), m_valuePos(50), m_valueVisible(false) {}

TimVisualMap::TimVisualMap(const QString & pattern, ActiveSignalLevels_t activeSignalLevels, const TimBehaviorMap *map)
  : m_regexp(pattern), m_behaviorMap(map), m_activeSignalLevels(activeSignalLevels), m_valuePos(50), m_valueVisible(false) {}


TimVisualMap::TimVisualMap(const TimVisualMap &rhs) {
  *this = rhs;
}

TimVisualMap::~TimVisualMap() {}

void TimVisualMap::setBehaviorMap(const TimBehaviorMap *map) {
  m_behaviorMap = map;
}

TimVisualMap & TimVisualMap::operator=(const TimVisualMap &rhs) {
  m_regexp = rhs.m_regexp;
  m_behaviorMap = rhs.m_behaviorMap;
  m_activeSignalLevels = rhs.m_activeSignalLevels;
  m_edgeMapping  = rhs.m_edgeMapping;
  m_valuePos = rhs.m_valuePos;
  m_valueVisible = rhs.m_valueVisible;
  return *this;
}

bool TimVisualMap::match(const QString &value) const {
  return m_regexp.exactMatch(value);
}

void TimVisualMap::setPattern(const QString &pattern) {
  return m_regexp.setPattern(pattern);
}

void TimVisualMap::setPattern(const QRegExp &pattern) {
  m_regexp = pattern;
}

QString TimVisualMap::getPattern() const {
  return m_regexp.pattern();
}

ActiveSignalLevels_t TimVisualMap::getActiveSignalLevels() const {
  return m_activeSignalLevels;
}

void TimVisualMap::setActiveSignalLevels(const ActiveSignalLevels_t &activeSignalLevels) {
  m_activeSignalLevels = activeSignalLevels;
}

bool TimVisualMap::insertSignalLevel(int level, int idx) {
  if (idx==-1)
    m_activeSignalLevels.push_back(level);
  else if (idx<=m_activeSignalLevels.size())
      m_activeSignalLevels.insert(m_activeSignalLevels.begin()+level, idx);
  else
    return false;
  return true;
}

unsigned int TimVisualMap::getSignalLevelCount() const {
  if (!m_behaviorMap)
    return 0;
  return m_behaviorMap->getSignalLevelCount();
}

EdgeMapping_t TimVisualMap::getEdgeMapping() const {
  return m_edgeMapping;
}

void TimVisualMap::setEdgeMapping(const EdgeMapping_t &em) {
  m_edgeMapping = em;
}

unsigned int TimVisualMap::getValuePos() const {
  return m_valuePos;
}

bool TimVisualMap::isValueVisible() const {
  return m_valueVisible;
}

void TimVisualMap::setValuePos(unsigned int valuePos) {
    m_valuePos = valuePos;
}

void TimVisualMap::setValueVisible(bool valueVisible) {
  m_valueVisible = valueVisible;
}

void TimVisualMap::SSGWrite(QXmlStreamWriter *writer) {
  writer->writeStartElement("visualmap");

  writer->writeStartElement("pattern");
  writer->writeCharacters(m_regexp.pattern());
  writer->writeEndElement();

  for (auto lvl: m_activeSignalLevels) {
      writer->writeStartElement("level");
      writer->writeCharacters(QString::number(lvl));
      writer->writeEndElement();
    }

  for (auto em: m_edgeMapping) {
      writer->writeStartElement("edgemapping");
      for (auto lvl: em.first) {
          writer->writeStartElement("source");
          writer->writeCharacters(QString::number(lvl));
          writer->writeEndElement();
        }
      writer->writeStartElement("target");
      writer->writeCharacters(QString::number(em.second));
      writer->writeEndElement();
      writer->writeEndElement();
    }


  writer->writeStartElement("valuevisible");
  writer->writeCharacters(QVariant(m_valueVisible).toString());
  writer->writeEndElement();

  writer->writeStartElement("valuepos");
  writer->writeCharacters(QString::number(m_valuePos));
  writer->writeEndElement();

  writer->writeEndElement();
}


