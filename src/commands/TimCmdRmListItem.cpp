// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include "TimCmdRmListItem.h"
#include "TimScene.h"
#include "TimMemberBase.h"

TimCmdRmListItem::TimCmdRmListItem(TimScene *scene, TimMemberBase *item) {
  m_timingScene = scene;
  m_item = item;
  m_index = -1;
  m_owning = false;
}

TimCmdRmListItem::~TimCmdRmListItem() {
  if (m_owning) {
    delete m_item;
  }
}

void TimCmdRmListItem::undo() {
  // add items again
  m_timingScene->insertTimListItem(m_index, m_item);
  m_owning = false;
}

void TimCmdRmListItem::redo() {
  m_index = m_timingScene->removeTimListItem(m_item);
  m_owning = true;
}



