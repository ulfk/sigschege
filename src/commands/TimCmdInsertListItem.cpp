// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include "TimCmdInsertListItem.h"

#include "TimScene.h"
#include "TimMemberBase.h"

TimCmdInsertListItem::TimCmdInsertListItem(TimScene *tscene, TimMemberBase *item, int index)
  : m_timingScene(tscene), m_item(item), m_index(index), m_owning(true) {}

TimCmdInsertListItem::~TimCmdInsertListItem() {
  if (m_owning)
    delete m_item;
}

void TimCmdInsertListItem::undo() {
  m_index = m_timingScene->removeTimListItem(m_item);
  m_owning = true;
}

void TimCmdInsertListItem::redo() {
  m_timingScene->insertTimListItem(m_index, m_item);
  m_owning = false;
}
