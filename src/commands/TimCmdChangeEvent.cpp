// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimCmdChangeEvent.h"
#include "TimEvent.h"

TimCmdChangeEvent::TimCmdChangeEvent(std::shared_ptr<TimEvent> event, const QString &newState)
  : m_oldState(event->getValue()), m_newState(newState), m_event(event) {}

TimCmdChangeEvent::~TimCmdChangeEvent() {}

void TimCmdChangeEvent::undo() {
  m_event->setValue(m_oldState);
}

void TimCmdChangeEvent::redo() {
  m_event->setValue(m_newState);
}
