// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#include "TimCmdChangeDiagramSettings.h"
#include "TimScene.h"
#include "TimDiagramSettings.h"

TimCmdChangeDiagramSettings::TimCmdChangeDiagramSettings(TimScene *timingScene, const TimDiagramSettings& newDiagramSettings)
  : m_timingScene(timingScene), newDiagramSettings(newDiagramSettings), oldDiagramSettings(*(timingScene->getDiagramSettings()))
{}


TimCmdChangeDiagramSettings::~TimCmdChangeDiagramSettings() {
}

void TimCmdChangeDiagramSettings::undo() {
  m_timingScene->setDiagramSettings(oldDiagramSettings);
}

void TimCmdChangeDiagramSettings::redo() {
  m_timingScene->setDiagramSettings(newDiagramSettings);
}
