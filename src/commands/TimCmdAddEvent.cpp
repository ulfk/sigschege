// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimCmdAddEvent.h"
#include "TimManualSignal.h"

TimCmdAddEvent::TimCmdAddEvent(TimManualSignal *signal, double time, const QString &value, double slope, double uncertainty)
  : m_signal(signal), m_time(time), m_slope(slope), m_uncertainty(uncertainty), m_value(value) {}

TimCmdAddEvent::~TimCmdAddEvent() {}

void TimCmdAddEvent::undo() {
  m_signal->rmTimEvent(m_time);
}

void TimCmdAddEvent::redo() {
  m_signal->addTimEvent(false, m_time, m_value, m_slope, m_uncertainty);
}

