// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#include "TimCmdRmEvent.h"
#include "TimEvent.h"
#include "TimWave.h"
#include <QDebug>

TimCmdRmEvent::TimCmdRmEvent(TimWave *wave, std::shared_ptr<TimEvent> event)
  : m_wave(wave), m_event(event) {}

TimCmdRmEvent::~TimCmdRmEvent() {}

void TimCmdRmEvent::undo() {
  m_wave->addTimEvent(m_event);
}

void TimCmdRmEvent::redo() {
  m_wave->rmTimEvent(m_event);
}
