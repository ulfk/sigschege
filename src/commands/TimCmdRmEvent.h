// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================

#ifndef TIMCMDRMEVENT_H_
#define TIMCMDRMEVENT_H_

#include "TimDeletable.h"
#include <QUndoCommand>
#include <memory>
class TimWave;
class TimEvent;

class TimCmdRmEvent : public QUndoCommand {
public:
  TimCmdRmEvent(TimWave * wave, std::shared_ptr<TimEvent> event);
  virtual ~TimCmdRmEvent();
  virtual void undo();
  virtual void redo();

private:
  TimWave  * m_wave;
  std::shared_ptr<TimEvent> m_event;
};

#endif /* TIMCMDRMEVENT_H_ */
