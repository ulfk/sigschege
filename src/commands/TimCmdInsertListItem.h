// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCMDINSERTLISTITEM_H_
#define TIMCMDINSERTLISTITEM_H_

#include <QUndoCommand>

class TimScene;
class TimMemberBase;

class TimCmdInsertListItem: public QUndoCommand
{
  public:

  TimCmdInsertListItem(TimScene *tscene, TimMemberBase *item, int index = -1);
    virtual ~TimCmdInsertListItem();

    /** @brief Undo the command.
     *
     * Undo the addSignal command.
     *
     */
    virtual void undo();

    /** @brief Redo the command.
     *
     * Redo the addSignal command.
     */
    virtual void redo();

  private:
    TimScene   *m_timingScene;
    TimMemberBase *m_item;
    int            m_index;
    bool           m_owning;
};

#endif /* TIMCMDINSERTLISTITEM_H_ */
