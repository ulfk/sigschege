// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCMDCHANGEDIAGRAMSETTINGS_H
#define TIMCMDCHANGEDIAGRAMSETTINGS_H

#include <QUndoCommand>
#include "TimDiagramSettings.h"
class TimScene;

class TimCmdChangeDiagramSettings : public QUndoCommand {
public:
  TimCmdChangeDiagramSettings(TimScene *timingScene, const TimDiagramSettings& newDiagramSettings);
    virtual ~TimCmdChangeDiagramSettings();

    /** @brief Undo the command.
     *
     * Undo the addSignal command.
     *
     */
    virtual void undo();

    /** @brief Redo the command.
     *
     * Redo the addSignal command.
     */
    virtual void redo();

private:
  TimScene *m_timingScene;
  TimDiagramSettings newDiagramSettings;
  TimDiagramSettings oldDiagramSettings;
};

#endif // TIMCMDCHANGEDIAGRAMSETTINGS_H
