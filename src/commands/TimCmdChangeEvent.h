// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCMDCHANGEEVENT_H
#define TIMCMDCHANGEEVENT_H

#include <QUndoCommand>
#include <QString>
#include "TimEvent.h"

class TimCmdChangeEvent : public QUndoCommand
{
public:
  TimCmdChangeEvent(std::shared_ptr<TimEvent> event, const QString &newState);
  virtual ~TimCmdChangeEvent();
  virtual void undo();
  virtual void redo();

private:
  QString m_oldState;
  QString m_newState;
  std::shared_ptr<TimEvent> m_event;
};

#endif // TIMCMDCHANGEEVENT_H
