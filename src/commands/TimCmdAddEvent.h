// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCMDADDEVENT_H_
#define TIMCMDADDEVENT_H_

#include "TimEvent.h"
#include <QUndoCommand>
class TimManualSignal;

class TimCmdAddEvent : public QUndoCommand {
public:

  TimCmdAddEvent(TimManualSignal *signal, double time, const QString &value, double slope, double uncertainty);
  virtual ~TimCmdAddEvent();
  virtual void undo();
  virtual void redo();

private:
  TimManualSignal *m_signal;
  double           m_time;
  double           m_slope;
  double           m_uncertainty;
  QString          m_value;
};

#endif /* TIMCMDADDEVENT_H_ */
