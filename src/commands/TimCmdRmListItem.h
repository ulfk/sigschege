// -*- c++ -*-
// Copyright by Ingo Hinrichs, Ulf Klaperski
// This file is part of Sigschege - Signal Schedule Generator
// Distributed under the terms of of the GNU General Public
// License, version 3. See the file LICENSE for further information.
// =================================================================
#ifndef TIMCMDRMLISTITEM_H_
#define TIMCMDRMLISTITEM_H_

#include <QtGui>
#include <QUndoCommand>
class TimScene;
class TimMemberBase;

/** @brief The @c TimCmdRmItem removes one item from the timing list.
 *
 * The @c TimCmdRmItem removes one item from the timing list. It supports undo and redo operations.
 *
 */
class TimCmdRmListItem: public QUndoCommand {
public:
  /** @brief CTor
   *
   * Creates a new @c TimCmdRmListItem object.
   *
   * @param tscene Pointer to the scene where the signal should be added
   * @param item Pointer to the list item to delete
   */
  TimCmdRmListItem(TimScene *tscene, TimMemberBase *item);

  /** @brief DTor
   * Destroys the @c TimCmdRmListItem object and every owning item object.
   */
  virtual ~TimCmdRmListItem();

  /** @brief Undo the command.
   *
   * Undo the rmItem command.
   *
   */
  virtual void undo();

  /** @brief Redo the command.
   *
   * Redo the rmItem command.
   */
  virtual void redo();

private:
  TimScene   *m_timingScene;
  TimMemberBase *m_item;
  int            m_index;
  bool           m_owning;
};

#endif /* TIMCMDRMLISTITEM_H_ */
