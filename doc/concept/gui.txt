 emacs, try -*- outline -*-

* behaviour inside the diagram

** possible actions inside wave

- 


A: click to add event, a dialog prompting for the new value should be shown, in case of the "logic"
visualization map a menu with the 4 possible choices could be used

B: click+pull event to move it to another time

C: click state/event to change value

D: click event text to change value

E: click event to delete it

** modes

add-mode (A)
select-mode (B,C,D)
delete-mode (E)

