#include <QTest>
#include <memory>
#include "TimEvent.h"

class TestTimEvent : public QObject {
    Q_OBJECT
private slots:
    void testCreate();
    void testCreateInitial();
    void testTimeCompare();
};

void TestTimEvent::testCreate()
{
  std::unique_ptr<TimEvent> ev = TimEvent::create(nullptr, "val", 2.0);
  QCOMPARE(ev->getValue(), "val");
  QCOMPARE(ev->getEventTime(), 2.0);
  QCOMPARE(ev->getIsInitial(), false);
}

void TestTimEvent::testCreateInitial()
{
  std::unique_ptr<TimEvent> ev = TimEvent::createInitial(nullptr, "val");
  QCOMPARE(ev->getValue(), "val");
  QCOMPARE(ev->getIsInitial(), true);
}

void TestTimEvent::testTimeCompare()
{
  std::unique_ptr<TimEvent> evInitial = TimEvent::createInitial(nullptr, "val");
  std::unique_ptr<TimEvent> evInitial2 = TimEvent::createInitial(nullptr, "val");
  std::unique_ptr<TimEvent> ev1 = TimEvent::create(nullptr, "val", std::numeric_limits<double>::lowest());
  std::unique_ptr<TimEvent> ev2 = TimEvent::create(nullptr, "val", -2.0);
  std::unique_ptr<TimEvent> ev3 = TimEvent::create(nullptr, "val", -2.0);

  QVERIFY(!(*evInitial < *evInitial2));
  QVERIFY(*evInitial < *ev1);
  QVERIFY(*evInitial < *ev2);
  QVERIFY(*ev1 < *ev2);
  ev1->setEventTime(-3.0);
  QVERIFY(*ev1 < *ev2);

  QVERIFY(*evInitial == *evInitial2);
  QVERIFY(*evInitial != *ev1);
  QVERIFY(*ev2 != *ev1);
  QVERIFY(*ev2 == *ev3);

  QVERIFY(*ev2 > *ev1);
  QVERIFY(!(*ev2 > *ev3));

  QVERIFY(*ev2 >= *ev1);
  QVERIFY(*ev2 >= *ev3);
  QVERIFY(!(*evInitial >= *ev3));

  QVERIFY(*ev2 <= *ev3);
  QVERIFY(*ev1 <= *ev3);
  QVERIFY(!(*ev3 <= *ev1));

}



QTEST_MAIN(TestTimEvent)
#include "TestTimEvent.moc"
