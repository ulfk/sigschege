
#include <QTest>
#include "TimVisualMap.h"

class TestVisualMap : public QObject {
    Q_OBJECT
private slots:
    void testGetSet();
    void testPattern();
};


void TestVisualMap::testGetSet()
{
  TimVisualMap vm("pattern", {10, 90}, nullptr);
  QCOMPARE(vm.getPattern(), "pattern");
  QCOMPARE(vm.getValuePos(), 50);
  vm.setPattern("p2");
  vm.setValuePos(77);
  QCOMPARE(vm.getPattern(), "p2");
  QCOMPARE(vm.getValuePos(), 77);
  QCOMPARE(vm.getActiveSignalLevels()[0], 10);
  vm.setActiveSignalLevels({40, 60});
  QCOMPARE(vm.getActiveSignalLevels()[1], 60);
  QVERIFY(!vm.isValueVisible());
  vm.setValueVisible(true);
  QVERIFY(vm.isValueVisible());
}

void TestVisualMap::testPattern()
{
  TimVisualMap vm(".*", {10, 90}, nullptr);
  QVERIFY(vm.match(""));
  QVERIFY(vm.match("a"));
  QVERIFY(vm.match("Anything!"));
  vm.setPattern("[0-9]");
  QVERIFY(!vm.match(""));
  QVERIFY(vm.match("7"));
  QVERIFY(!vm.match("77"));
  QVERIFY(!vm.match("v"));
}

QTEST_MAIN(TestVisualMap)
#include "TestVisualMap.moc"
