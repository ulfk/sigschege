#include <QTest>
#include "TimVisualMap.h"
#include "TimBehaviorMap.h"

class TestBehaviorMap : public QObject {
    Q_OBJECT
private slots:
    void testGetSet();
    void testSignalLevels();
    void testVisualMaps();
};


void TestBehaviorMap::testGetSet()
{
  TimBehaviorMap bm("bus");
  QCOMPARE(bm.getName(), "bus");
  bm.setName("logic");
  QCOMPARE(bm.getName(), "logic");
}

void TestBehaviorMap::testSignalLevels()
{
  TimBehaviorMap bm("bus");
  QCOMPARE(bm.getSignalLevelCount(), 0);
  bm.insertSignalLevel(20);
  bm.insertSignalLevel(50);
  bm.insertSignalLevel(80);
  QCOMPARE(bm.getSignalLevelCount(), 3);
  QCOMPARE(bm.getSignalLevel(0), 20);
  QCOMPARE(bm.getSignalLevel(2), 80);
  bm.insertSignalLevel(33, 1);
  bm.insertSignalLevel(100, 4);
  QCOMPARE(bm.getSignalLevelCount(), 5);
  QCOMPARE(bm.getSignalLevel(3), 80);
  QCOMPARE(bm.getSignalLevel(4), 100);
  QCOMPARE(bm.getSignalLevel(1), 33);
  bm.rmSignalLevel(1);
  QCOMPARE(bm.getSignalLevelCount(), 4);
  QCOMPARE(bm.getSignalLevel(2), 80);
  QCOMPARE(bm.getSignalLevel(3), 100);
  bm.updateSignalLevel(70, 2);
  QCOMPARE(bm.getSignalLevelCount(), 4);
  QCOMPARE(bm.getSignalLevel(2), 70);

}

void TestBehaviorMap::testVisualMaps()
{
  TimBehaviorMap bm("bus");
  TimVisualMap_p vm1 = TimVisualMap_p(new TimVisualMap("vm1", {10, 90}, nullptr));
  TimVisualMap_p vm2 = TimVisualMap_p(new TimVisualMap("vm2", {10, 90}, nullptr));
  TimVisualMap_p vm3 = TimVisualMap_p(new TimVisualMap("vm3", {10, 90}, nullptr));
  bm.insertVisualMap(vm1);
  bm.insertVisualMap(vm3);
  QCOMPARE(bm.getVisualMap(0)->getPattern(), "vm1");
  QCOMPARE(bm.getVisualMap(1)->getPattern(), "vm3");
  bm.insertVisualMap(vm2, 1);
  QCOMPARE(bm.getVisualMap(1)->getPattern(), "vm2");
  QCOMPARE(bm.getVisualMap(2)->getPattern(), "vm3");
  bm.swapVisualMap(1, 0);
  bm.swapVisualMap(1, 2);
  QCOMPARE(bm.getVisualMap(2)->getPattern(), "vm1");
  QCOMPARE(bm.getVisualMap("vm3")->getPattern(), "vm3");
  bm.rmVisualMap(0);
  bm.rmVisualMap(1);
  QCOMPARE(bm.getVisualMap(0)->getPattern(), "vm3");
}

QTEST_MAIN(TestBehaviorMap)
#include "TestBehaviorMap.moc"
