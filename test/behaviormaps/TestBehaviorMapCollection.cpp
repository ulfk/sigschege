#include <QTest>
#include "TimBehaviorMap.h"
#include "TimBehaviorMapCollection.h"

class TestBehaviorMapCollection : public QObject {
    Q_OBJECT
private slots:
    void testGetSet();
};


void TestBehaviorMapCollection::testGetSet()
{
  TimBehaviorMap_p bm1 = TimBehaviorMap_p(new TimBehaviorMap("bm1"));
  TimBehaviorMap_p bm2 = TimBehaviorMap_p(new TimBehaviorMap("bm2"));
  TimBehaviorMap *bm3 = new TimBehaviorMap("bm3");
  TimBehaviorMapCollection bmc;
  bmc.registerBehaviorMap("bm1", bm1);
  bmc.registerBehaviorMap("bm2", bm2);
  bmc.registerBehaviorMap("bm3", bm3);
  QCOMPARE(bmc.getBehaviorMapByName("bm2")->getName(), "bm2");
  QCOMPARE(bmc.getBehaviorMapByName("bm3")->getName(), "bm3");
}


QTEST_MAIN(TestBehaviorMapCollection)
#include "TestBehaviorMapCollection.moc"
