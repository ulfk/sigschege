#include <QTest>
#include <QAction>
#include <memory>
#include "RecentFileList.h"

using namespace std;


class TestRecentFileList : public QObject {
    Q_OBJECT
private slots:
    void testGetSet();
    void testRangeBasedFor();
    void testStringListInOut();
    void testCleanUp();
    void testMaxFiles();
};


void TestRecentFileList::testGetSet()
{
  RecentFileList rfl;
  QAction action(QString("Hello World"));
  rfl.push_back("Hello World");
  QCOMPARE(rfl.size(), 1);
  QCOMPARE(action.text(), rfl[0]->text());
}

void TestRecentFileList::testRangeBasedFor()
{
  RecentFileList rfl;
  rfl.push_back("/path/0");
  rfl.push_back("1");
  int i = 0;
  for (auto a: rfl) {
      QCOMPARE(a->text(),  QString::number(i));
      QVERIFY(i<2);
      ++i;
    }
  QCOMPARE(rfl[0]->toolTip(),  QString("/path/0"));
}

void TestRecentFileList::testStringListInOut()
{
  RecentFileList rfl;
  QStringList files { "file1",  "file2", "file3" };
  rfl.setFiles(files);
  QCOMPARE(rfl.size(), 3);
  QCOMPARE(rfl[2]->toolTip(),  QString("file3"));
  QStringList files2 = rfl.getFiles();
  QCOMPARE(files, files2);
}

void TestRecentFileList::testCleanUp()
{
  QStringList files { "3",  "1", "2", "3", "3" };
  RecentFileList rfl;
  rfl.setFiles(files);
  QCOMPARE(rfl.size(), 3);
  int i = 0;
  for (auto a: rfl) {
      QCOMPARE(a->text(),  QString::number(++i));
    }
  rfl.push_back("2");
  QCOMPARE(rfl.size(), 3);
  QCOMPARE(rfl[0]->text(),  "1");
  QCOMPARE(rfl[1]->text(),  "3");
  QCOMPARE(rfl[2]->text(),  "2");
}

void TestRecentFileList::testMaxFiles()
{
  RecentFileList rfl;
  rfl.setMaxFiles(3);
  QStringList files { "1", "2", "3" };
  rfl.setFiles(files);
  rfl.setMaxFiles(2);
  QCOMPARE(rfl.size(), 2);
  QCOMPARE(rfl[0]->text(),  "2");
  QCOMPARE(rfl[1]->text(),  "3");
}

QTEST_MAIN(TestRecentFileList)
#include "TestRecentFileList.moc"
